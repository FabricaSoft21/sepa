﻿ProgramacionApp.factory('EmpresaService',
    ['$http', '$rootScope', '$routeParams',
    function ($http, $rootScope, $routeParams) {
        var service = {};

        service.SubirArchivo = function (data, callback) {
            waitingDialog.show();
            var ajaxRequest = $.ajax({
                type: "POST",
                url: URLServices + "File/UploadFileFicha",
                contentType: false,
                processData: false,
                data: data
            }).done(function (responseData, textStatus) {
                callback(responseData);
                waitingDialog.hide();
            }).fail(function () {
                waitingDialog.hide();
            });
        };

        //Metodo para guardar empresa
        service.GuardarEmpresa = function (Empresa, callback) {
            $http.post(URLServices + "Empresas/GuardarEmpresa", Empresa)
                .success(function (response) {
                    callback(response);
                });
        };

        //Metodo para listas las empresas
        service.ConsultarEmpresa = function (callback) {
            $http.get(URLServices + "Empresas/ConsultarEmpresa/")
                .success(function (response) {
                    callback(response);
                });
        };

        //metodo para modificar una empresa
        service.Modificar = function (Empresa, callback) {
            $http.post(URLServices + "Empresas/ModificarEmpresa/", Empresa[0])
              .success(function (response) {
                  callback(response);
              })
        };

        service.GuardarModificacionEmpresa = function (Empresa, callback) {
            $http.post(URLServices + "Empresas/GuardarModificacionEmpresa/", Empresa)
                    .success(function (response) {
                        callback(response);
                    })
        };



        service.BorrarEmpresa = function (Empresas, callback) {
            var Item = {
                Parametros: []
            };

            $.each(Empresas, function (index, value) {
                Item.Parametros.push(
                    {
                        Parametro2: value.Parametro2
                    })
            });

            $http.post(URLServices + "Empresas/EliminarEmpresa/", Item)
                .success(function (response) {
                    callback(response);
                });
        };

        return service;
    }])
