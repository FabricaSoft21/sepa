﻿/// <reference path="../Area/AreaService.js" />
ProgramacionApp.controller('EmpresasController',
    ['$scope', '$rootScope', '$location', 'EmpresaService', '$routeParams', '$sce',
        function ($scope, $rootScope, $location, EmpresaService, $routeParams, $sce) {

            $scope.AbrirModal = function () {
                $("#ModalEmpresas").modal("show");
                $scope.VaciarCampos();
            };
            var nav = $("#navbar").hasClass("gn-menu-wrapper gn-open-all");
            if (nav == true) {
                $(".Principal").css("width", "80%");

            } else {
                $(".Principal").css("width", "95%");
            }
            $("#atras").hide();


            $scope.empresa = {
                Id_Empresa:"",
                Nit: "",
                Nombre_Empresa: "",
                Nombre_Coformador: "",
                Correo: "",
                Telefono: "",
                Direccion: "",
                CargoCoformador: "",
                CorreoCoformador: "",
                TelefonoCoformador: ""
            }

            // pagination
            $scope.curPage = 0;
            $scope.pageSize = 8;
            
            //metodo para consultar una empresa
           EmpresaService.ConsultarEmpresa(function (response) {
                if (response.success == true) {
                   
                    $scope.datalists = response.datos;

                    $scope.numberOfPages = function () {
                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                    };

                    $scope.Datos = $scope.datalists;
                }
            });

            //metodo para registar una empresa
            $scope.Guardar = function () {
              
                if ($scope.empresa.Nit == "" || $scope.empresa.Nombre_Empresa == "" || $scope.empresa.Direccion == "" || $scope.empresa.CargoCoformador == "" || $scope.empresa.CorreoCoformador == "" || $scope.empresa.TelefonoCoformador == "" || $scope.empresa.Nombre_Coformador == "" || $scope.empresa.Correo == "" || $scope.empresa.Telefono == "") {
                    bootbox.dialog({
                        title: "Información",
                        message: "Debe diligenciar todos los campos",
                        buttons: {
                            success: {
                                label: "Cerrar",
                                className: "btn-default",
                            }
                        }
                    });
                } else {
                    EmpresaService.GuardarEmpresa($scope.empresa, function (response) {
                        if (response.success == true) {
                            EmpresaService.ConsultarEmpresa(function (response) {
                                if (response.success == true) {
                                    $scope.datalists = response.datos;
                                    $scope.ListaCompleta = response.datos;
                                    bootbox.dialog({
                                        title: "Información",
                                        message: "El registro se realizó con éxito",
                                        buttons: {
                                            success: {
                                                label: "Cerrar",
                                                className: "btn-default",
                                            }
                                        }
                                    });
                                }
                                $("#ModalEmpresas").modal("hide");
                                $scope.VaciarCampos();
                            });

                        } else {

                            bootbox.dialog({
                                title: "Información",
                                message: "La empresa ya se encuentra resgistrada",
                                buttons: {
                                    success: {
                                        label: "Cerrar",
                                        className: "btn-default",
                                    }
                                }
                            });
                        }
                    });
                }
            };


            //metodo para vaciar campos
            $scope.VaciarCampos = function () {
                $scope.empresa.Id_Empresa = "";
                $scope.empresa.Nit = "";
                $scope.empresa.Nombre_Empresa = "";
                $scope.empresa.Nombre_Coformador = "";
                $scope.empresa.Correo = "";
                $scope.empresa.Telefono = "";
                $scope.empresa.Direccion = "";
                $scope.empresa.CargoCoformador = "";
                $scope.empresa.CorreoCoformador = "";
                $scope.empresa.TelefonoCoformador = "";
            };

           

            //Función para filtrar la coordinacion 
            $scope.Filter = function (e) {
                var Busqueda = $("#Buscar").val();
                var exp = new RegExp(Busqueda);
                if (Busqueda == "") {
                    EmpresaService.ConsultarEmpresa(function (response) {
                        if (response.success == true) {
                            $scope.datalists = response.datos;
                            $scope.ListaCompleta = response.datos;
                        }
                    });
                }
                var Area = [];
                $scope.datalists = $scope.ListaCompleta;
                Area = $scope.datalists.filter(function (item) {


                    if (exp.test(item.Parametro2) || exp.test(item.Parametro2)) {

                        return item;
                    }

                    else if (exp.test(item.Parametro3.toLowerCase()) || exp.test(item.Parametro3.toUpperCase())) {
                        return item;
                    }

                    else if (exp.test(item.Parametro4.toLowerCase()) || exp.test(item.Parametro4.toUpperCase())) {
                        return item;
                    }
                });
                $scope.datalists = Area;
                //Variable para setear la paginación 
                $scope.curPage = 0;

            };



            //Funcion para editar una empresa
            
            $scope.Modificar = function () {

                var EmpresaModificar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;

                });


                if (EmpresaModificar.length == 1) {

                    EmpresaService.Modificar(EmpresaModificar, function (response) {

                        if (response.success == true) {

                            $scope.empresa.Nit = response.empres.Nit;
                            $scope.empresa.Nombre_Empresa = response.empres.Nombre_Empresa;
                            $scope.empresa.Nombre_Coformador = response.empres.Nombre_Coformador;
                            $scope.empresa.Correo = response.empres.Correo;
                            $scope.empresa.Telefono = response.empres.Telefono;
                            $("#ModalEditar").modal("show");
                        }
                    });
                } else {

                    swal("¡Ten cuidado!", "Primero debes elegir la empresa que deseas modificar.");
                }
            }

            //Función 2 para editar las areas
            $scope.GuardarEdicionEmpresa = function () {
                if ($scope.empresa.Nit == "" || $scope.empresa.Nombre_Empresa == "" || $scope.empresa.Nombre_Coformador == "" || $scope.empresa.Correo == "" || $scope.empresa.Telefono == "") {
                    bootbox.dialog({
                        title: "Información",
                        message: "Debe diligenciar todos los campos",
                        buttons: {
                            success: {
                                label: "Cerrar",
                                className: "btn-default",
                            }
                        }
                    });
                } else {
                    EmpresaService.GuardarModificacionEmpresa($scope.empresa, function (response) {
                        if (response.success == true) {
                            bootbox.dialog({
                                title: "Información",
                                message: "La modificación se realizó con éxito",
                                buttons: {
                                    success: {
                                        label: "Cerrar",
                                        className: "btn-default",
                                    }
                                }
                            });
                            $("#ModalEditar").modal("hide");
                            $scope.VaciarCampos();
                            EmpresaService.ConsultarEmpresa(function (response) {
                                if (response.success == true) {
                                    $scope.datalists = response.datos;
                                    $scope.ListaCompleta = response.datos;
                                }
                            });
                        }
                    });
                    
                }
            }

            //fincion para cambia el estado de una empresa
            $scope.CambiarEstadoSeleccionados = function () {
                var EmpresaBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (EmpresaBorrar.length == 0) {

                    swal("¡Ten cuidado!", "Primero debes elegir la empresa que deseas inhabilitar.");

                } else {

                    $("#modalInhabilitar").modal("show");
                }
            };



            //Función para inhabilitar la coordinacion 
            $scope.inhabilitar = function () {

                var EmpresaBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                EmpresaService.BorrarEmpresa(EmpresaBorrar, function (response) {

                    if (response.success == true) {
                        EmpresaService.ConsultarEmpresa(function (response) {
                            if (response.success == true) {
                                bootbox.dialog({
                                    title: "Información",
                                    message: "La eliminación se realizó con éxito",
                                    buttons: {
                                        success: {
                                            label: "Cerrar",
                                            className: "btn-default",
                                        }
                                    }
                                });
                                $scope.datalists = response.datos;
                                $scope.ListaCompleta = response.datos;
                            }
                        });
                    }

                });

            };

        }])








