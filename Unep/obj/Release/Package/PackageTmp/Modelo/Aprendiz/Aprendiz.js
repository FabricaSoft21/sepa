﻿ProgramacionApp.controller('AprendizController',
    ['$scope', '$rootScope', '$location', 'AprendizService', '$routeParams', '$sce',
        function ($scope, $rootScope, $location, AprendizService, $routeParams, $sce) {

            //Subir excel Aprendiz
            $scope.SubirArchivo = function () {
                AprendizService.alerta();
            }

            //Datos del Aprendiz
            $scope.Aprendiz = {
                IdAprendiz: "",
                Tipo_Documento: "",
                Nombre: "",
                Apellido: "",
                NumDocumento: "",
                Email: "",
                Estado: "",
                Telefono: "",
                PoblacionDps: true,
                IdFicha: "",
                IdUsuario: ""
            };

            //Datos de la ficha
            $scope.Ficha = {
                IdFicha: "",
                NumFicha: "",
                NumAprendices: "",
                Estado: "",
                TipoFormacion: "",
                FechaInicio: "",
                FechaFin: "",
                FechaLimite: "",
                FechaMomentoDos: "",
                FechaMomentoTres: "",
                Jornada: "",
                IdPrograma: ""
            };

            $scope.UploadFileWeb = function () {
                $("#fileUploadWeb").trigger('click');
            };
            $("#fileUploadWeb").change(function () {
                dataweb = new FormData();
                var files = $("#fileUploadWeb").get(0).files;
                var fileExtension = ['xlsx'];
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    bootbox.dialog({
                        title: "Importar Archivo",
                        message: "La extencion del archivo no es valida",
                        buttons: {
                            success: {
                                label: "Cerrar",
                                className: "btn-primary",
                            }
                        }
                    });
                    $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                    return false;
                }
                // Add the uploaded image content to the form data collection
                if (files.length > 0) {
                    readURL(this, "logoweb");
                    dataweb.append("UploadedImage", files[0]);
                    if (dataweb != null) {
                        AprendizService.SubirArchivo(dataweb, function (response) {
                            if (response.success) {
                                bootbox.dialog({
                                    title: "Importar Archivo",
                                    message: "La importación del archivo se realizó con éxito ",
                                    buttons: {
                                        success: {
                                            label: "Cerrar",
                                            className: "btn-primary",
                                        }
                                    }
                                });
                                $scope.path = response.path;
                                $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                                AprendizService.ConsultarAprendiz(function (response) {
                                    if (response.success == true) {
                                        $scope.datalists = response.datos;
                                        $scope.ListaCompleta = response.datos;
                                        $scope.numberOfPages = function () {
                                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                                        };
                                    }
                                });
                                $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                                return;
                            }
                        });
                    }
                }
            });

            function readURL(input, control) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#' + control + '').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            };


            //Abrir modal registro
            $scope.AbrirModal = function () {
                $("#ModalAprendizRegistrar").modal("show");
                $scope.VaciarCampos();
            };

            //Registrar Aprendiz
            $scope.Guardar = function () {
                $.each($scope.Ficha, function (Sindex, value) {
                    if (value.IdFicha == $scope.Ficha.IdFicha) {
                        $scope.Aprendiz.IdFicha = value.IdFicha;
                    }
                });

                if ($scope.Aprendiz.NumDocumento == "" || $scope.Aprendiz.Tipo_Documento == "" || $scope.Aprendiz.Nombre == "" || $scope.Aprendiz.Apellido == "" || $scope.Aprendiz.Email == "" || $scope.Aprendiz.Telefono == "" || $scope.Aprendiz.IdFicha == "") {
                    bootbox.dialog({
                        title: "Información",
                        message: "Debe diligenciar todos los campos",
                        buttons: {
                            success: {
                                label: "Cerrar",
                                className: "btn-primary",
                            }
                        }
                    });

                } else {
                    AprendizService.Email($scope.Aprendiz, function (response) {
                        if (response.success == true) {
                            AprendizService.GuardarAprendiz($scope.Aprendiz, function (response) {
                                if (response.success == true) {
                                    $scope.VaciarCampos();
                                    AprendizService.ConsultarAprendiz(function (response) {
                                        if (response.success == true) {
                                            $scope.datalists = response.datos;
                                            $scope.ListaCompleta = response.datos;
                                            bootbox.dialog({
                                                title: "Información",
                                                message: "El registro se realizó con éxito",
                                                buttons: {
                                                    success: {
                                                        label: "Cerrar",
                                                        className: "btn-primary",
                                                    }
                                                }
                                            });
                                            $("#ModalAprendizRegistrar").modal("hide");
                                        }
                                    });
                                } else {
                                    bootbox.dialog({
                                        title: "Información",
                                        message: "El aprendiz ya se encuentra registrado",
                                        buttons: {
                                            success: {
                                                label: "Cerrar",
                                                className: "btn-primary",
                                            }
                                        }
                                    });
                                }
                            }
                                );
                        } else {
                            bootbox.dialog({
                                title: "Información",
                                message: "El correo no tiene el formato correcto",
                                buttons: {
                                    success: {
                                        label: "Cerrar",
                                        className: "btn-primary",
                                    }
                                }
                            });
                        }
                    });
                }
            }

            //Consultar ficha select
            AprendizService.ConsultarFicha(function (response) {
                if (response.success == true) {
                    $scope.Ficha = response.datos;

                }
            });

            //Consultar Aprendiz
            AprendizService.ConsultarAprendiz(function (response) {
                if (response.success == true) {

                    $scope.curPage = 0;
                    $scope.pageSize = 8;
                    $scope.datalists = response.datos;
                    $scope.ListaCompleta = response.datos;
                    $scope.numberOfPages = function () {
                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                    };

                    $scope.Datos = $scope.datalists;
                }
            });

            //Limpiar campos
            $scope.VaciarCampos = function () {
                $scope.Aprendiz.IdAprendiz = "",
                $scope.Aprendiz.Nombre = "",
                $scope.Aprendiz.Apellido = "",
                $scope.Aprendiz.NumDocumento = "",
                $scope.Aprendiz.Email = "",
                $scope.Aprendiz.Estado = "",
                $scope.Aprendiz.Telefono = "",
                $scope.Aprendiz.PoblacionDps = "",
                $scope.Aprendiz.IdFicha = "",
                $scope.Aprendiz.IdUsuario = ""

            };

            //Seleccionar
            $scope.SeleccionarTodos = function () {

                contador = (($scope.curPage + 1) * 3) - 3;
                var item = (($scope.curPage + 1) * 3) - 3;
                var items1 = (($scope.curPage + 1) * 3);


                $.each($scope.datalists, function (index, value) {

                    if (contador < items1) {
                        value[contador];
                        value.Seleccionado = $scope.SeleccionTodos;
                        contador++;

                    }
                });


            };

            //Función 1 para editar los aprendices
            $scope.Modificar = function () {
                var AprendizM = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                if (AprendizM.length == 1) {

                    AprendizService.ModificarAprendiz(AprendizM, function (response) {
                        console.log(response)
                        if (response.success == true) {
                            $scope.Aprendiz = response.Aprendiz;
                            $("#ModalAprendizEditar").modal("show");
                            AprendizService.ConsultarFichaxAprendiz(response.Aprendiz.IdFicha, function (response) {
                                $("#listas > option[value='" + response.datos[0].IdFicha + "']").attr('selected', 'selected');
                            });
                        }
                    });
                } else {
                    swal("¡Ten cuidado!", "Primero debes elegir el aprendiz que deseas modificar.");
                }
            };

            //Función 2 para editar las areas
            $scope.GuardarModicarAprendiz = function () {
                if ($scope.Aprendiz.NumDocumento == null || $scope.Aprendiz.Nombre == null || $scope.Aprendiz.Apellido == null || $scope.Aprendiz.Email == null || $scope.Aprendiz.Telefono == null || $scope.Aprendiz.IdFicha == null) {
                    bootbox.dialog({
                        title: "Información",
                        message: "Debe diligenciar todos los campos",
                        buttons: {
                            success: {
                                label: "Cerrar",
                                className: "btn-primary",
                            }
                        }
                    })
                } else {
                    AprendizService.GuardarModificacionAprendiz($scope.Aprendiz, function (response) {
                        if (response.success == true) {
                            AprendizService.ConsultarAprendiz(function (response) {
                                if (response.success == true) {
                                    $scope.datalists = response.datos;
                                    $scope.ListaCompleta = response.datos;
                                    bootbox.dialog({
                                        title: "Información",
                                        message: "La modificación del registro se realizo con exito",
                                        buttons: {
                                            success: {
                                                label: "Cerrar",
                                                className: "btn-primary",
                                            }
                                        }
                                    });
                                    $("#ModalAprendizEditar").modal("hide");
                                    $scope.VaciarCampos();
                                }
                            });
                        }
                    });
                }
            }

            //Función para Borrar todos las registros de la tabla
            $scope.BorrarSeleccionados = function () {
                var AprendizesBorrar = $scope.Datos.filter(function (item) {
                    return item.Seleccionado === true;
                });
                if (AprendizesBorrar.length == 0) {
                    alert("Seleccione un aprendiz para borrar");
                }
                if (AprendizesBorrar.length > 0) {
                    r = confirm("¿Desea eliminar los aprendices?");
                    if (r == true) {
                        AprendizService.BorrarAprendiz(AprendizesBorrar, function (response) {
                            if (response.success == true) {
                                alert("Eliminado!!");
                            }
                        })
                    }
                }
            };

            //Función 1 para el borrado de las areas
            $scope.CambiarEstadoSeleccionados = function () {
                var UsariosBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (UsariosBorrar.length == 0) {
                    swal("¡Ten cuidado!", "Primero debes elegir el aprendiz que deseas inhabilitar.");
                } else {

                    $("#modalInhabilitar").modal("show");
                }
            }

            //Buscar en la tabla
            $scope.Filter = function (e) {
                var Busqueda = $("#Buscar").val();
                var exp = new RegExp(Busqueda);
                if (Busqueda == "") {
                    AprendizService.ConsultarAprendiz(function (response) {
                        if (response.success == true) {
                            $scope.datalists = response.datos;
                            $scope.ListaCompleta = response.datos;
                        }
                    });
                }
                var Aprendiz = [];
                $scope.datalists = $scope.ListaCompleta;
                Aprendiz = $scope.datalists.filter(function (item) {


                    if (exp.test(item.Parametro2) || exp.test(item.Parametro2)) {

                        return item;
                    }

                    else if (exp.test(item.Parametro3.toLowerCase()) || exp.test(item.Parametro3.toUpperCase())) {
                        return item;
                    }

                    else if (exp.test(item.Parametro4.toLowerCase()) || exp.test(item.Parametro4.toUpperCase())) {
                        return item;
                    }
                });
            
            $scope.datalists = Aprendiz;
            //Variable para setear la paginación 
            $scope.curPage = 0;
            }
            //Función 2 para el borrado de las areas
            $scope.inhabilitar = function () {

                var AprendizBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                AprendizService.BorrarAprendiz(AprendizBorrar, function (response) {

                    if (response.success == true && response.respuesta == true) {
                        AprendizService.ConsultarAprendiz(function (response) {
                            if (response.success == true) {
                                $scope.datalists = response.datos;
                                $scope.ListaCompleta = response.datos;
                            }
                        })

                        bootbox.dialog({
                            title: "Eliminar",
                            message: "Se elimaron las siguiente cantidad de registros " + response.contadorTrue,
                            buttons: {
                                success: {
                                    label: "Cerrar",
                                    className: "btn-primary",
                                }
                            }
                        });


                    } else {

                        bootbox.dialog({
                            title: "Inhabilitar",
                            message: "No se puedieron elimanar la siguiente cantidad de registros " + response.contadorFalse + " porque estan asociados a otras tablas.",
                            buttons: {
                                success: {
                                    label: "Cerrar",
                                    className: "btn-primary",
                                }
                            }
                        });
                    }

                });

            };
}]);