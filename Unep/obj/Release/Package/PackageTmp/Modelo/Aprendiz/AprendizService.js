﻿ProgramacionApp.factory('AprendizService',
    ['$http', '$rootScope', '$routeParams',
    function ($http, $rootScope, $routeParams) {
        var service = {};

        service.SubirArchivo = function (data, callback) {
            waitingDialog.show();
            console.log(data);
            var ajaxRequest = $.ajax({
                type: "POST",
                url: URLServices + "File/UploadFileAprendiz",
                contentType: false,
                processData: false,
                data: data,
                async: true,
            }).done(function (responseData, textStatus) {
                callback(responseData);
                waitingDialog.hide();
            }).fail(function () {
                waitingDialog.hide();
                location.reload();
            });
        };

        service.Email = function (Aprendiz, callback) {
            $http.post(URLServices + "Aprendiz/Email/", Aprendiz)
                .success(function (response) {
                        callback(response);
                });
        };

        service.GuardarAprendiz = function (Aprendiz, callback) {
            $http.post(URLServices + "Aprendiz/GuardarAprendiz/", Aprendiz)
           .success(function (response) {
               callback(response);
           });
        };

        service.ModificarAprendiz = function (Aprendiz, callback) {
            $http.post(URLServices + "Aprendiz/ModificarAprendiz/", Aprendiz[0])
              .success(function (response) {
                  callback(response);
              })
        };

        service.GuardarModificacionAprendiz = function (Aprendiz, callback) {
            $http.post(URLServices + "Aprendiz/GuardarModificacionAprendiz", Aprendiz)
                .success(function (response) {
                    callback(response);
                });
        };

        service.ConsultarFichaxAprendiz = function (IdAprendiz, callback) {

            item = {
                Parametro1: IdAprendiz
            };

            $http.post(URLServices + "Aprendiz/ConsultarFichaxAprendiz/", item)
                .success(function (response) {
                    callback(response);
                });
        };

        service.ConsultarFicha = function (callback) {

            $http.get(URLServices + "Aprendiz/ConsultarFichas/")
                .success(function (response) {
                    callback(response);
                });
        };

        service.ConsultarAprendiz = function (callback) {
            $http.get(URLServices + "Aprendiz/ConsultarAprendiz/")
                .success(function (response) {
                    callback(response);
                });
        };

        service.BorrarAprendiz = function (Aprendiz, callback) {
            var Item = {
                Parametros: []
            }
            $.each(Aprendiz, function (index, value) {
                Item.Parametros.push(
                    {
                        Parametro1: value.Parametro1
                    })
            });
            $http.post(URLServices + "Aprendiz/EliminarAprendiz/", Item)
                .success(function (response) {
                    callback(response);
                });
        };

        return service;
    }])