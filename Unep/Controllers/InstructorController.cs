﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using LogicaNegocio.Logica;
using Datos.Modelo;
using Datos.DTO;
using Unep.parametros;

namespace Unep.Controllers
{
  

    public class InstructorController : ApiController
    {
        [HttpPost]
        public IHttpActionResult GuardarInstructor(InstructorDTO oInstructor)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                var Instructor = oInstructorBl.GuardarInstructor(oInstructor);
                if (Instructor == true)
                {
                    return Ok(new { success = true });
                }
                else
                {
                    return Ok(new { success = false });
                }
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpPost]
        public IHttpActionResult ConsultarInstructores()
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                var Datos = oInstructorBl.ConsultarInstructores();
                return Ok(new { datos = Datos, success = true });

            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult EliminarProgramacion(ParametrosDTO oParametrosDTO)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                var Datos = oInstructorBl.EliminarProgramaciones(int.Parse(oParametrosDTO.Parametro2));
                return Ok(new { datos = Datos, success = true });

            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult ConsultarIdentificadorFicha(ParametrosDTO oParametrosDTO)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                var Datos = oInstructorBl.ConsultarIdentificadorFicha(int.Parse(oParametrosDTO.Parametro2));
                return Ok(new { datos = Datos, success = true });

            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult ConsultarInhabilitados()
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                var Datos = oInstructorBl.ConsultarInhabilitados();
                return Ok(new { datos = Datos, success = true });

            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult CambiarEstado(List<Instructor> oInstructor)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                foreach (var item in oInstructor)
                {
                    oInstructorBl.CambiarEstado(item);
                }
                return Ok(new { success = true });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        public IHttpActionResult HabilitarInstructor(List<Instructor> oInstructor)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                foreach (var item in oInstructor)
                {
                    oInstructorBl.CambiarEstado(item);
                }
                return Ok(new { success = true });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpPost]

        public IHttpActionResult consultarCedula(string cedula)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();

                oInstructorBl.ConsultarInstructorCedula(cedula);

                return Ok(new { success = true });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpGet]
        public IHttpActionResult ConsultarCentros()
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();

                var Datos = oInstructorBl.ConsultarCentros();

                return Ok(new { datos = Datos, success = true });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpPost]
        public IHttpActionResult Consultarcoordinacion(ParametrosDTO oParametrosDTO)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();

                var Datos = oInstructorBl.Consultarcoordinacion(int.Parse(oParametrosDTO.Parametro2));

                return Ok(new { datos = Datos, success = true });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpPost]
        public IHttpActionResult ConsultarAreas(ParametrosDTO oParametrosDTO)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                DetalleCentrosAreasCoordinacion detalle = new DetalleCentrosAreasCoordinacion();
                detalle.IdCentro = Convert.ToInt32(oParametrosDTO.Parametro2);
                detalle.IdCoordinacion = Convert.ToInt32(oParametrosDTO.Parametro3);

                var Datos = oInstructorBl.ConsultarAreas(detalle);

                return Ok(new { datos = Datos, success = true });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpPost]

        public IHttpActionResult ModificarInstructor(Instructor oInstructor)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();

                var Instructor = oInstructorBl.ConsultarInstructorCedula1(oInstructor.NumeroDocumento);

                return Ok(new { success = true, Instructor });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }


        [HttpPost]

        public IHttpActionResult GuardarModificacionInstructor(InstructorDTO oInstructor)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();

                oInstructorBl.ActualizarRegistro(oInstructor);

                return Ok(new { success = true });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpGet]
        public IHttpActionResult ConsultarProgramas()
        {
            try
            {
                ProgramaBl oProgramaBl = new ProgramaBl();
                var Programas = oProgramaBl.ConsultarProgramas();
                return Ok(new { success = true, datos = Programas });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult Picoyplaca(ParametrosDTO placa)
        {
            try
            {
                ParametrosDTO parame = new ParametrosDTO();
                parame.Parametro2 = (placa.Parametro1.Contains("Lunes")) ? "true" : "false";
                parame.Parametro3 = (placa.Parametro1.Contains("Martes")) ? "true" : "false";
                parame.Parametro4 = (placa.Parametro1.Contains("Miercoles")) ? "true" : "false";
                parame.Parametro5 = (placa.Parametro1.Contains("Jueves")) ? "true" : "false";
                parame.Parametro6 = (placa.Parametro1.Contains("Viernes")) ? "true" : "false";
                parame.Parametro7 = (placa.Parametro1.Contains("Sabado")) ? "true" : "false";
                return Ok(new { success = true, datos = parame });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

        [HttpGet]
        public IHttpActionResult fichas()
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                var ficha = oInstructorBl.ConsultarFichas();
                return Ok(new { success = true, datos = ficha });
            }
            catch(Exception ex)
            {
                return Ok(new { success = false, ex = ex.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult ConsultarProgramacion(ParametrosDTO parametros)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                var ficha = oInstructorBl.ConsultarProgramaciones(int.Parse(parametros.Parametro2));
                return Ok(new { success = true, datos = ficha });
            }
            catch (Exception ex)
            {
                return Ok(new { success = false, ex = ex.Message });
            }
        }


        [HttpPost]
        public IHttpActionResult EnviarCorreoInstructor()
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                oInstructorBl.EnviarCorreoInstructor();
                return Ok(new { success = true });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false });
            }
        }

        [HttpPost]
        public IHttpActionResult RegistrarProgramacion(List<ProgramacionInstructorFicha> oProgramacion)
        {
            try
            {
                InstructorBl oInstructorBl = new InstructorBl();
                var Instructor = oInstructorBl.RegistrarProgramacion(oProgramacion);
                if (Instructor == true)
                {
                    return Ok(new { success = true });
                }
                else
                {
                    return Ok(new { success = false });
                }
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }

        }
    }
}