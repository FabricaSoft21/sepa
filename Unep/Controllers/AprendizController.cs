﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using LogicaNegocio.Logica;
using Datos.Modelo;
using Unep.parametros;
using System.Threading;
using Datos.DTO;



namespace Unep.Controllers
{
    public class AprendizController : ApiController
    {
        [HttpPost]
        public IHttpActionResult GuardarAprendiz(Aprendiz oAprendiz)
        {
            try
            {
                AprendizBl oAprendizBl = new AprendizBl();
                var Aprendiz = oAprendizBl.GuardarAprendizIndividual(oAprendiz);
                if (Aprendiz == true)
                {
                    return Ok(new { success = true });

                }
                else
                {
                    return Ok(new { success = false });
                }
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpGet]
        public IHttpActionResult ConsultarFichas()
        {
            try
            {
                FichaBl oFichaBl = new FichaBl();
                bool Datos = oFichaBl.ConsultarFichas();
                return Ok(new { datos = Datos, success = true });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

        [HttpGet]
        public IHttpActionResult ConsultarAprendiz()
        {
            List<AprendizDTO> ListaParametros = new List<AprendizDTO>();
            try
            {
                AprendizBl oAprendizBl = new AprendizBl();

                ListaParametros = oAprendizBl.ConsultarAprendiz();

                return Ok(new { datos = ListaParametros, success = true });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult EliminarAprendiz(ParametrosDTO oParametrosDTO)
        {
            try
            {
                var vali = false;
                bool Respuesta = false;
                int contadorTrue = 0;
                int contadorFalse = 0;
                var Id = "";
                AprendizBl oAprendizBl = new AprendizBl();
                foreach (var item in oParametrosDTO.Parametros)
                {

                    Respuesta = oAprendizBl.EliminarAprendiz(int.Parse(item.Parametro1));

                    if (Respuesta)
                    {
                        contadorTrue++;
                    }
                    else
                    {
                        contadorFalse++;
                    }

                }
                if(contadorTrue == 1)
                {
                    vali = true;
                }
                return Ok(new { respuesta = Respuesta, contadorTrue = contadorTrue, contadorFalse = contadorFalse, success = true, prueba = vali });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        //[HttpPost]
        //public IHttpActionResult ggdfEliminarAprendiz(ParametrosDTO oParametrosDTO)
        //{
        //    try
        //    {
        //        var vali = false;
        //        bool Respuesta = false;
        //        int contadorTrue = 0;
        //        int contadorFalse = 0;
        //        var Id = "";
        //        AprendizBl oAprendizBl = new AprendizBl();
        //        foreach (var item in oParametrosDTO.Parametros)
        //        {

        //            Respuesta = oAprendizBl.EliminarAprendiz(int.Parse(item.Parametro1));

        //            if (Respuesta)
        //            {
        //                contadorTrue++;
        //            }
        //            else
        //            {
        //                contadorFalse++;
        //            }

        //        }
        //        if (contadorTrue == 1)
        //        {
        //            vali = true;
        //        }
        //        return Ok(new { respuesta = Respuesta, contadorTrue = contadorTrue, contadorFalse = contadorFalse, success = true, prueba = vali });
        //    }
        //    catch (Exception exc)
        //    {

        //        return Ok(new { success = false, exc = exc.Message });
        //    }

        //}


        [HttpPost]
        public IHttpActionResult GuardarModificacionAprendiz(Aprendiz oAprendiz)
        {
            try
            {
                AprendizBl oAprendizBl = new AprendizBl();
                oAprendizBl.ActualizarRegistro(oAprendiz);

                return Ok(new { success = true });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpPost]
        public IHttpActionResult CambiarFicha(Aprendiz oAprendiz)
        {
            try
            {
                AprendizBl oAprendizBl = new AprendizBl();
                oAprendizBl.ActualizarFicha(oAprendiz);

                return Ok(new { success = true });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpPost]
        public IHttpActionResult ModificarAprendiz(ParametrosDTO Parametro)
        {
            try
            {
                AprendizBl oAprendizBl = new AprendizBl();
                var Aprendiz = oAprendizBl.ConsultarAprendizId(int.Parse(Parametro.Parametro1));

                return Ok(new { success = true, Aprendiz});
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpPost]
        public IHttpActionResult ConsultarFichaxAprendiz(ParametrosDTO oParametrosDTO)
        {
            try
            {
                AprendizBl oAprendizBl = new AprendizBl();
                var datos = oAprendizBl.ConsultarFichaxAprendiz(int.Parse(oParametrosDTO.Parametro1));
                return Ok(new { success = true, datos });

            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc.Message });
            }


        }

        [HttpPost]
        public IHttpActionResult Email(Aprendiz oAprendiz)
        {
            try
            {
                if (oAprendiz.Correo.Contains("@") && oAprendiz.Correo.Contains("."))
                {
                    return Ok(new { success = true });

                }
                else
                {
                    return Ok(new { success = false });
                }
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

    }
}