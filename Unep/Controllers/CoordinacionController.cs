﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using LogicaNegocio.Logica;
using Datos.Modelo;
using Unep.parametros;
using Datos.DTO;

namespace Unep.Controllers
{
    public class CoordinacionController : ApiController
    {
        [HttpPost]
        public IHttpActionResult GuardarCoordinacion(CoordinacionDTO oCoordinacion)
        {
            try
            {
                CoordinacionBl oCoordinacionBl = new CoordinacionBl();
                var coordinador = oCoordinacionBl.GuardarCoordinacion(oCoordinacion);
                return Ok(new { success = true, coordinador });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }


        [HttpGet]
        public IHttpActionResult ConsultarCoordinacion()
        {
            try
            {
                CoordinacionBl oCoordinBl = new CoordinacionBl();
                var Datos = oCoordinBl.ConsultarCoordinacion();
                AreaBl AreaBl = new AreaBl();
                CentroBl CentroBl = new CentroBl();

                List<ParametrosDTO> ListaParametro = new List<ParametrosDTO>();

                foreach (var item in Datos)
                {
                    ParametrosDTO oParametro = new ParametrosDTO();
                    oParametro.Parametro1 = item.IdCoordinacion.ToString();
                    oParametro.Parametro3 = item.NumeroDocumento.ToString();
                    oParametro.Parametro4 = item.Nombres.ToString();
                    oParametro.Parametro5 = item.Apellidos.ToString();
                    oParametro.Parametro6 = item.Correo.ToString();
                    oParametro.Parametro7 = item.Telefono.ToString();
                    oParametro.Parametro8 = item.TipoDocumento.ToString();
                    ListaParametro.Add(oParametro);
                }
                return Ok(new { datos = ListaParametro, success = true });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult ModificarCoordinacion(ParametrosDTO oParametro)
        {
            try
            {
                CoordinacionBl oCoordinacionBl = new CoordinacionBl();
                var Coordinacion = oCoordinacionBl.ConsultarCoordinacionId(int.Parse(oParametro.Parametro1));

                return Ok(new { success = true, Coordinacion = Coordinacion });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult ConsultarIds(ParametrosDTO oParametro)
        {
            try
            {
                CoordinacionBl oCoordinacionBl = new CoordinacionBl();
                var Coordinacion = oCoordinacionBl.ConsultarIds(int.Parse(oParametro.Parametro1));
                return Ok(new { success = true, Coordinacion = Coordinacion });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }


        [HttpPost]
        public IHttpActionResult GuardarModificacionCoordinacion(CoordinacionDTO oCoordinacion)
        {
            try
            {
                CoordinacionBl oCoordinacionBl = new CoordinacionBl();
                oCoordinacionBl.ActualizarRegistro(oCoordinacion);
                return Ok(new { success = true });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult EliminarCoordinacion(ParametrosDTO oParametrosDTO)
        {
            try
            {
                CoordinacionBl oCoordinacionBl = new CoordinacionBl();
                foreach (var item in oParametrosDTO.Parametros)
                {
                    oCoordinacionBl.EliminarCoordinacion(int.Parse(item.Parametro1));
                }
                return Ok(new { success = true });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }
        [HttpGet]
        public IHttpActionResult ConsultarCentros()
        {
            try
            {
                CentroBl oCentroBl = new CentroBl();
                var Datos = oCentroBl.ConsultarCentros();
                return Ok(new { datos = Datos, success = true });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }
    }
}