﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LogicaNegocio.Logica;
using Unep.parametros;
using Datos.Modelo;

namespace Unep.Controllers
{
    public class GestionController : ApiController
    {

        Model1 entry = new Model1();
        [HttpGet]
        public IHttpActionResult  ConsultarAlternativa()
        {

            try
            {

                GestionBl gestion = new GestionBl();

                var datos = gestion.ConsultarAlternativa().ToList();

                return Ok(new { datos =datos,success = true });
            }
            catch
            {

                return Ok(new { success = false });
            }


        }

        [HttpGet]
        public IHttpActionResult ConsultarEmpresaSede()
        {
            try
            {

                GestionBl gestion = new GestionBl();

                var datos = gestion.ConsultarEmpresaSede().ToList();

                return Ok(new { datos = datos, success = true });
            }
            catch
            {

                return Ok(new { success = false });
            }


        }


        [HttpPost]
        public IHttpActionResult ModificarAprendiz(ParametrosDTO Parametro)
        {
            try
            {
                GestionBl oAprendizBl = new GestionBl();
                var Aprendiz = oAprendizBl.ConsultarAprendizId(int.Parse(Parametro.Parametro1));

                return Ok(new { success = true, Aprendiz });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }


        [HttpPost]
        public IHttpActionResult selectfecha(ParametrosDTO Parametro)
        {
            try
            {
                GestionBl oAprendizBl = new GestionBl();
                var Aprendiz = oAprendizBl.selectfecah(int.Parse(Parametro.Parametro1));

                return Ok(new { success = true, Aprendiz });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }




        [HttpPost]
        public IHttpActionResult GuardarModificacionAprendiz(Aprendiz oAprendiz)
        {
            try
            {
                GestionBl oAprendizBl = new GestionBl();
                oAprendizBl.ActualizarRegistro(oAprendiz);

                return Ok(new { success = true });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

        [HttpPost]
        public IHttpActionResult GuardarGestion(GestionIndividual oAprendiz)
        {
            try
            {

                    GestionBl oAprendizBl = new GestionBl();
                  var boola =  oAprendizBl.GuardarGestion(oAprendiz);
                if (boola)
                {
                    return Ok(new { success = true });

                }else
                {
                    return Ok(new { success = false });
                }
                    

            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });
            }

        }

    }
}
