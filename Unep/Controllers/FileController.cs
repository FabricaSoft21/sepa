﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Globalization;
using System.Text;
using System.Net.Mail;
using LogicaNegocio.Logica;
using Datos.Modelo;
using Unep.Controllers;
using LinqToExcel;
using System.Data.OleDb;
using System.Data;
using Datos.DTO;

namespace CORONA.ValidacionRef.Servicios.Controllers
{
    public class FileController : ApiController
    {

        public IHttpActionResult UploadFileArea()
        {
            try
            {
                //                List<LogResponseDTO> lstErrores = new List<LogResponseDTO>();
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    var fileSavePath = string.Empty;

                    var docfiles = new List<string>();

                    var URLArchivo = "";

                    foreach (string file in httpRequest.Files)
                    {

                        var postedFile = httpRequest.Files[file];
                        //  var filePath = HttpContext.Current.Server.MapPath("C:/UploadedFiles/");
                        var filePath = "C:/UploadedFiles/";

                        var GUID = Guid.NewGuid().ToString();

                        if (!Directory.Exists(filePath))
                        {
                            Directory.CreateDirectory(filePath);
                        }

                        fileSavePath = Path.Combine(filePath, GUID + "." + postedFile.FileName.Split('.')[1]);



                        postedFile.SaveAs(fileSavePath);

                        docfiles.Add(filePath);

                        URLArchivo = "C:/UploadedFiles/" + GUID + "." + postedFile.FileName.Split('.')[1];


                        string e = Path.GetExtension(URLArchivo);
                        if (e != ".xlsx")
                        {
                            return Ok(new { success = false, message = "La extencion del archivo no es valida" });
                        }

                    }

                    string FilePath = URLArchivo.Split('/')[2];


                    Area oArea = new Area();
                    AreaBl oAreaBl = new AreaBl();

                    var book = new ExcelQueryFactory(URLArchivo);

                    var hoja = book.GetWorksheetNames();
                    var resultado = (from i in book.Worksheet(hoja.FirstOrDefault())
                                     select i).ToList();

                    foreach (var values in resultado)
                    {
                        var codigo = oAreaBl.ConsultarAreaCodigo(values[0]);
                        if (codigo == null)
                        {
                            oArea.CodigoArea = (values[0]);
                            oArea.Nombre = values[1];
                            oArea.Descripcion = values[2];

                            oAreaBl.GuardarArea(oArea);

                        }
                    }
                    return Ok(new { success = true, path = URLArchivo });
                }
                else
                {
                    return Ok(new { success = false, message = "No File" });
                }

            }
            catch (Exception exc)
            {

                return Ok(new { success = false, message = exc.Message });
            }
        }

        public IHttpActionResult UploadFilePrograma()
        {
            try
            {
                //                List<LogResponseDTO> lstErrores = new List<LogResponseDTO>();
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    var fileSavePath = string.Empty;

                    var docfiles = new List<string>();

                    var URLArchivo = "";

                    foreach (string file in httpRequest.Files)
                    {

                        var postedFile = httpRequest.Files[file];

                        var filePath = "C:/UploadedFiles/";

                        var GUID = Guid.NewGuid().ToString();

                        if (!Directory.Exists(filePath))
                        {
                            Directory.CreateDirectory(filePath);
                        }

                        fileSavePath = Path.Combine(filePath, GUID + "." + postedFile.FileName.Split('.')[1]);



                        postedFile.SaveAs(fileSavePath);

                        docfiles.Add(filePath);


                        URLArchivo = "C:/UploadedFiles/" + GUID + "." + postedFile.FileName.Split('.')[1];


                        string e = Path.GetExtension(URLArchivo);
                        if (e != ".xlsx")
                        {
                            return Ok(new { success = false, message = "La extencion del archivo no es valida" });
                        }

                    }

                    string FilePath = URLArchivo.Split('/')[2];


                    Programa oPrograma = new Programa();
                    ProgramaBl oProgramaBl = new ProgramaBl();
                    List<string> ProgramaNoRegistro = new List<string>();

                    Area oArea = new Area();
                    AreaBl oAreaBl = new AreaBl();

                    var book = new ExcelQueryFactory(URLArchivo);

                    var hoja = book.GetWorksheetNames();
                    var resultado = (from i in book.Worksheet(hoja.FirstOrDefault())
                                     select i).ToList();

                    var contador = 0;


                    foreach (var values in resultado)
                    {
                        if (values[0] == "")
                        {
                            break;
                        }
                        var codigo = oProgramaBl.ConsultarProgramaCodigo(values[0]);
                        if (codigo != null)
                        {
                            ProgramaNoRegistro.Add(values[1]);
                            continue;
                        }
                        if (codigo == null)
                        {
                            oPrograma.CodigoPrograma = (values[0]);
                            oPrograma.NombrePrograma = values[1];
                            oPrograma.Nivel = values[2];


                            oPrograma.IdArea = oAreaBl.ConsultarAreaCodigo(values[3]).IdArea;

                            oProgramaBl.GuardarPrograma(oPrograma);
                            contador++;

                        }


                    }

                    var no = ProgramaNoRegistro;
                    return Ok(new { success = true, path = URLArchivo });


                }
                else
                {
                    return Ok(new { success = false, message = "No File" });
                }

            }
            catch (Exception exc)
            {

                return Ok(new { success = false, message = exc.Message });
            }
        }

        public IHttpActionResult UploadFileFicha()
        {
            List<RegistroError> error = new List<RegistroError>();


            List<Ficha> Registro = new List<Ficha>();
            Model1 entity = new Model1();

            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    var fileSavePath = string.Empty;
                    var docfiles = new List<string>();
                    var URLArchivo = "";

                    foreach (var file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file.ToString()];
                        var filePath = "C:/UploadedFiles/";
                        var GUID = Guid.NewGuid().ToString();

                        if (!Directory.Exists(filePath))
                        {
                            Directory.CreateDirectory(filePath);
                        }
                        fileSavePath = Path.Combine(filePath, GUID + "." + postedFile.FileName.Split('.')[1]);

                        postedFile.SaveAs(fileSavePath);

                        docfiles.Add(filePath);

                        URLArchivo = "C:/UploadedFiles/" + GUID + "." + postedFile.FileName.Split('.')[1];

                        string e = Path.GetExtension(URLArchivo);
                        if (e != ".xlsx")
                        {
                            return Ok(new { success = false, message = "La extencion del archivo no es valida" });
                        }
                    }
                    FichaBl Ficha = new FichaBl();
                    var book = new ExcelQueryFactory(URLArchivo);
                    var resultado = (from row in book.Worksheet("Hoja1")
                                     let item = new FichaDTO
                                     {
                                         IdentificadorFicha = row[0].Cast<string>(),
                                         Codigo = row[1].Cast<string>(),
                                         Estado = row[3].Cast<string>(),
                                         Jornada = row[4].Cast<string>(),
                                         TipoRespuesta = (row[6].Cast<string>() != null) ? row[6].Cast<string>() : (row[5].Cast<string>() == "") ? "ABIERTA" : "CERRADA",
                                         CodigoArea = row[7].Cast<string>(),
                                         CodigoPrograma = row[9].Cast<string>(),
                                         FechaInicioFicha = row[13].Cast<DateTime>(),
                                         FechaFinFicha = row[14].Cast<DateTime>(),
                                         ModalidadFormacion = row[15].Cast<string>(),
                                         CapacitacionSGVA = (row[16].Cast<string>().ToLower() == "si") ? true : false,
                                         TotalAprendices = row[17].Cast<int>(),
                                         NumeroMesesEp = row[18].Cast<string>()
                                     }
                                     select item
                                     ).ToList();
                    Ficha.GuardarFichaExcel(resultado);

                    return Ok(new { success = true, datos = error });
                }
                else
                {
                    return Ok(new { success = false, message = "No File" });
                }
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, message = exc.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult UploadFileAprendiz()
        {
            List<RegistroError> error = new List<RegistroError>();
            List<Aprendiz> Registro = new List<Aprendiz>();
            Model1 entity = new Model1();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    var fileSavePath = string.Empty;
                    var docfiles = new List<string>();
                    var URLArchivo = "";

                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        var filePath = "C:/UploadedFiles/";
                        var GUID = Guid.NewGuid().ToString();

                        if (!Directory.Exists(filePath))
                        {
                            Directory.CreateDirectory(filePath);
                        }

                        fileSavePath = Path.Combine(filePath, GUID + "." + postedFile.FileName.Split('.')[1]);

                        postedFile.SaveAs(fileSavePath);

                        docfiles.Add(filePath);

                        URLArchivo = "C:/UploadedFiles/" + GUID + "." + postedFile.FileName.Split('.')[1];


                        string e = Path.GetExtension(URLArchivo);
                        if (e != ".xlsx")
                        {
                            return Ok(new { success = false, message = "La extencion del archivo no es valida" });
                        }
                    }

                    AprendizBl Aprendiz = new AprendizBl();

                    var book = new ExcelQueryFactory(URLArchivo);
                    var resultado = (from row in book.Worksheet("Hoja1")
                                     let item = new Aprendiz
                                     {
                                         NumeroDocumento = row[2].Cast<string>(),
                                         TipoDocumento = row[1].Cast<string>(),
                                         Nombres = row[3].Cast<string>(),
                                         PrimerApellido = row[4].Cast<string>(),
                                         SegundoApellido = row[5].Cast<string>(),
                                         Correo = row[7].Cast<string>(),
                                         Telefono = row[8].Cast<string>(),
                                         PoblacionDPS = (row[9].Cast<string>() == "SI") ? true : false,
                                         IdFicha = row[0].Cast<int>()
                                     }
                                     select item).ToList();
                    error = Aprendiz.GuardarAprendiz(resultado);
                    return Ok(new { success = true, datos = error });
                }
                else
                {
                    return Ok(new { success = false, message = "No File" });
                }

            }
            catch (Exception exc)
            {

                return Ok(new { success = false, message = exc.Message });
            }
        }


        [HttpPost]
        public IHttpActionResult UploadFileEmpresa()
        {
            List<RegistroError> error = new List<RegistroError>();
            List<EmpresasDT> Registro = new List<EmpresasDT>();
            List<string> errores;
            Model1 entity = new Model1();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    var fileSavePath = string.Empty;
                    var docfiles = new List<string>();
                    var URLArchivo = "";

                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        var filePath = "C:/UploadedFiles/";
                        var GUID = Guid.NewGuid().ToString();

                        if (!Directory.Exists(filePath))
                        {
                            Directory.CreateDirectory(filePath);
                        }

                        fileSavePath = Path.Combine(filePath, GUID + "." + postedFile.FileName.Split('.')[1]);

                        postedFile.SaveAs(fileSavePath);

                        docfiles.Add(filePath);

                        URLArchivo = "C:/UploadedFiles/" + GUID + "." + postedFile.FileName.Split('.')[1];


                        string e = Path.GetExtension(URLArchivo);
                        if (e != ".xlsx")
                        {
                            return Ok(new { success = false, message = "La extencion del archivo no es valida" });
                        }
                    }

                    EmpresaBl empresa = new EmpresaBl();

                    var book = new ExcelQueryFactory(URLArchivo);
                    var resultado = (from row in book.Worksheet("Hoja1")
                                     let item = new EmpresasDT
                                     {
                                         NitEmpresa = row[0].Cast<string>(),
                                         NombreEmpresa = row[1].Cast<string>(),
                                         Direccion = row[2].Cast<string>(),
                                         Telefono = row[3].Cast<string>(),
                                         Correo = row[4].Cast<string>(),
                                         NombreCoformador = row[5].Cast<string>(),
                                         CargoCoformador = row[6].Cast<string>(),
                                         TelefonoCoformador = (row[7].Cast<string>()),
                                         CorreoCoformador = row[8].Cast<string>()
                                     }
                                     select item).ToList();

                    empresa.GuardarEmpresaArchivo(resultado);
                    return Ok(new { success = true, datos = error });
                }
                else
                {
                    return Ok(new { success = false, message = "No File" });
                }

            }
            catch (Exception exc)
            {
                throw (exc);

                return Ok(new { success = false, message = exc.Message });
            }
        }
    }
}
