﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using LogicaNegocio.Logica;
using Datos.Modelo;
using Unep.parametros;
using Datos.DTO;
using System.Web.Http.Description;
using System.Net;

namespace Unep.Controllers
{
    public class EmpresasController : ApiController
    {

        [HttpPost]
        public IHttpActionResult GuardarEmpresa(EmpresasDT empresa)
        {

            EmpresaBl emprsaBlC = new EmpresaBl();

            try
            {
                bool vali = emprsaBlC.GuardarEmpresa(empresa);

                if (vali)
                {

                    return Ok(new { success = true });
                }
                else
                {

                    return Ok(new { success = false });
                }
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }


        }


        [HttpGet]
        public IHttpActionResult ConsultarEmpresa()
        {

            try
            {
                EmpresaBl oemprebl = new EmpresaBl();


                List<Empresa> datos = oemprebl.ConsultarEmpresa().ToList();


                return Ok(new { datos = datos, success = true });

            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });

            }

        }

        [HttpGet]
        public IHttpActionResult ConsultarEmpresainactiva()
        {

            try
            {
                EmpresaBl oemprebl = new EmpresaBl();


                List<Empresa> datos = oemprebl.ConsultarEmpresainactiva().ToList();


                return Ok(new { datos = datos, success = true });

            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });

            }

        }


        [HttpPost]
        public IHttpActionResult ModificarEmpresa(Empresa empresa)
        {
            try
            {
                EmpresaBl oEmpresa = new EmpresaBl();
                var empres = oEmpresa.EmpresaId(empresa.IdEmpresa);

                return Ok(new { success = true, empres = empres });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }


        [HttpPost]
        public IHttpActionResult GuardarModificacionEmpresa(Empresa oEmpresa)
        {
            try
            {
                EmpresaBl oEmpres = new EmpresaBl();
                oEmpres.ActualizarRegistro(oEmpresa);
                return Ok(new { success = true });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }


        [HttpPost]
        public IHttpActionResult EliminarEmpresa(ParametrosDTO oParametrosDTO)
        {
            try
            {
                EmpresaBl oEmpresa = new EmpresaBl();
                foreach (var item in oParametrosDTO.Parametros)
                {
                    oEmpresa.EliminarEmpresa(Convert.ToInt32(item.Parametro2));
                }
                return Ok(new { success = true });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }
        }

    }
}