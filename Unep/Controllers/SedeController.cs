﻿using LogicaNegocio.Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Datos.Modelo;
using Datos.DTO;
using Unep.parametros;


namespace Unep.Controllers
{
    public class SedeController : ApiController
    {

        [HttpGet]
        public IHttpActionResult ConsultarEmpresa()
        {

            try
            {
                EmpresaBl oSedebl = new EmpresaBl();
                List<Empresa> Empresa = oSedebl.ConsultarEmpresa();
                return Ok(new { success = true, datos = Empresa });
            }
            catch (Exception ex)
            {
                return Ok(new { success = false, ex = ex.Message });
            }
        }


        [HttpPost]
        public IHttpActionResult ConsultarSede(ParametrosDTO oPara)
        {
            try
            {
                Sedebl oSedebl = new Sedebl();
                List<Sede> Empresa = oSedebl.ConsultarSede(Convert.ToInt32(oPara.Parametro1));
                return Ok(new { success = true, datos = Empresa });
            }
            catch (Exception ex)
            {
                return Ok(new { success = false, ex = ex.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult RegistrarSede(ParametrosDTO Sedes)
        {
            try
            {
                Sede oSede = new Sede();
                oSede.CargoCoformador = Sedes.Parametro1;
                oSede.Codigo = Sedes.Parametro2;
                oSede.CorreoCoformador = Sedes.Parametro3;
                oSede.Direccion = Sedes.Parametro4;
                oSede.Estado = Sedes.Parametro5;
                oSede.IdEmpresa = int.Parse(Sedes.Parametro6);
               
                oSede.Nombre = Sedes.Parametro8;
                oSede.NombreCoformador = Sedes.Parametro9;
                oSede.Telefono = Sedes.Parametro10;
                oSede.TelefonoCoformador = Sedes.Parametro11;
                Sedebl oemprebl = new Sedebl();
                var datos = oemprebl.RegistrarPersona(oSede);

                return Ok(new { datos = datos, success = true });
            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });

            }



        }


        [HttpPost]
        public IHttpActionResult SedeId(ParametrosDTO oPara)
        {

            try
            {
                Sedebl Osede = new Sedebl();

                var sed = Osede.SedeId(Convert.ToInt32(oPara.Parametro9));


                return Ok(new { succes = true, sed });
            }
            catch (Exception ex)
            {

                return Ok(new { succes = false, ex.Message });
            }


        }

        [HttpPost]
        public IHttpActionResult GuardarModificacionSede(ParametrosDTO parametros)
        {

            try
            {
                Sedebl OsedeBl = new Sedebl();


                Sede SedeO = new Sede();

                SedeO.Codigo = parametros.Parametro2;
                SedeO.Nombre = parametros.Parametro3;
                SedeO.Telefono = parametros.Parametro4;
                SedeO.Direccion = parametros.Parametro5;
                SedeO.NombreCoformador = parametros.Parametro6;
                SedeO.CargoCoformador = parametros.Parametro7;
                SedeO.TelefonoCoformador = parametros.Parametro8;
                SedeO.CorreoCoformador = parametros.Parametro9;
                SedeO.IdEmpresa = Convert.ToInt32(parametros.Parametro10);
                SedeO.Estado = parametros.Parametro11;
         
                OsedeBl.GuardarModificacionSede(SedeO);



                return Ok(new { success = true });

            }
            catch (Exception ex)
            {

                return Ok(new { success = false, ex.Message });

            }
        }



        public IHttpActionResult inhabilitar(ParametrosDTO oParametrosDTO)
        {

            try
            {
                var esta = false; ;
                Sedebl osede = new Sedebl();
                foreach (var item in oParametrosDTO.Parametros)
                {
                    esta =osede.inhabilitar(Convert.ToInt32(item.Parametro9));
                }
                return Ok(new { success = true,esta });
            }
            catch (Exception exc)
            {
                return Ok(new { success = false, exc = exc.Message });
            }



        }


        [HttpPost]
        public IHttpActionResult ConsultarSedeinactiva(ParametrosDTO opara)
        {

            try
            {
                Sedebl oemprebl = new Sedebl();
                List<Sede> datos = oemprebl.ConsultarSedeinactiva(Convert.ToInt32(opara.Parametro1)).ToList();
                return Ok(new { datos = datos, success = true });

            }
            catch (Exception exc)
            {

                return Ok(new { success = false, exc = exc.Message });

            }

        }

    } 
}
