﻿ProgramacionApp.controller('FichaController',
    ['$scope', '$rootScope', '$location', 'FichaService', '$routeParams', '$sce',
        function ($scope, $rootScope, $location, FichaService, $routeParams, $sce) {

            $scope.datalistare = [];

            $scope.datalists = [];

            $scope.condi = {
                id: "",
                con: ""
            }


            var nav = $("#navbar").hasClass("gn-menu-wrapper gn-open-all");
            if (nav == true) {
                $(".Principal").css("width", "80%");

            } else {
                $(".Principal").css("width", "95%");
            }

            $scope.AbrirModalGestion = function () {
                $scope.VaciarCampos();

                $("#modificar").show();
                $("#panel1").css({ "display": "block" });
                $("#btnAtras").hide();
                $("#siguiente").show();
                $("#guardartodo").hide();
                $("#mostrarDos").css({ "background": "white", "border": "1px solid #fc7323", "color": "black" });
                $("#mostrarUno").css({ "background": "#fc7323", "color": "white" });
                $("#btnAtras").hide();
                $("#panel2").hide();
                $scope.deshabilitar();
                $("#selectemp").hide();
                $("#motivoid").hide();
                $("#guardartodo").hide();
                $("#guardar").hide();
                $("#ModalGestion").modal("show");
            };

            $("#atras").hide();

            $scope.alternativa = {
                IdAlternativa: "",
                NombreAlternativa: ""
            }


            $scope.gestion = {
                IdGestion: "",
                Arl: "",
                EmpresaSedido: "",
                DescripcionSedido: "",
                IdAprendiz: "",
                IdAlternativa: "",
                Idsede: "",
                FechaInicioProductiva: ""
            };


            FichaService.ConsultarAlternativa().then(function (response) {
                if (response.data.success == true) {
                    $scope.alternativa = response.data.datos;
                }
            });

            FichaService.ConsultarAprendiz1().then(function (response) {
                $("#panel2").hide();
                $("#btnAtras").hide();
                if (response.data.success == true) {
                    $scope.curPage = 0;
                    $scope.pageSize = 10;
                    $scope.datalists = response.data.datos;
                    $scope.ListaCompleta = response.data.datos;
                    $scope.numberOfPages = function () {
                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                    };

                    $scope.Datos = $scope.datalists;
                }
            });

            $scope.Gestionar = function (doc) {
                FichaService.ModificarAprendiz1(doc).then(function (response) {
                    if (response.data.success == true) {
                        $scope.Aprendiz.IdAprendiz = parseInt(response.data.Aprendiz[0].IdAprendiz);
                        $scope.Aprendiz.TipoDocumento = response.data.Aprendiz[0].TipoDocumento;
                        $scope.Aprendiz.NumeroDocumento = parseInt(response.data.Aprendiz[0].NumeroDocumento);
                        $scope.Aprendiz.Nombres = response.data.Aprendiz[0].Nombres;
                        $scope.Aprendiz.PrimerApellido = response.data.Aprendiz[0].PrimerApellido;
                        $scope.Aprendiz.SegundoApellido = response.data.Aprendiz[0].SegundoApellido;
                        $scope.Aprendiz.Correo = response.data.Aprendiz[0].Correo;
                        $scope.Aprendiz.Telefono = parseInt(response.data.Aprendiz[0].Telefono);
                        $scope.Aprendiz.IdFicha = response.data.Aprendiz[0].IdFicha;
                        $scope.Aprendiz.IdUsuario = response.data.Aprendiz[0].IdUsuario;

                        $scope.gestion.IdAlternativa = response.data.Aprendiz[0].IdAlternativa;
                        setTimeout(function () {
                            $("#listase").val(response.data.Aprendiz[0].Idsede).trigger("change");
                            $("#listas3").val(response.data.Aprendiz[0].IdAlternativa).trigger("change");
                            
                            
                        });
                        $scope.gestion.Idsede = response.data.Aprendiz[0].Idsede;
                        $scope.gestion.Arl = response.data.Aprendiz[0].Arl;
                        $scope.gestion.FechaInicioProductiva = response.data.Aprendiz[0].FechaInicioProductiva;
                        $("#fechainip").val(response.data.Aprendiz[0].FechaInicioProductiva);
                        $scope.gestion.DescripcionSedido = response.data.Aprendiz[0].DescripcionSedido;
                        $scope.gestion.EmpresaSedido = response.data.Aprendiz[0].EmpresaSedido;
                        $scope.AbrirModalGestion();
                    }
                });
                FichaService.selectfecha(doc).then(function (response) {
                    if (response.data.success == true) {
                        $("#fichaini").val(response.data.Aprendiz);
                    }
                });

            }


            $scope.ModificarApg = function () {

                $("#NombreAprendizga").removeAttr("disabled");
                $("#ApellidoAprendiz1").removeAttr("disabled");
                $("#ApellidoAprendiz2").removeAttr("disabled");
                $("#correo2ga").removeAttr("disabled");
                $("#telefonoga").removeAttr("disabled");
                $("#modificarapg").hide();
                $("#guardar").show();
            }

            $scope.GuardarModicarAprendiz1 = function () {
                if ($scope.Aprendiz.NumeroDocumento == null || $scope.Aprendiz.Nombres == null || $scope.Aprendiz.PrimerApellido == null || $scope.Aprendiz.SegundoApellido == null || $scope.Aprendiz.Correo == null || $scope.Aprendiz.Telefono == null) {
                    swal("Información", "Debe diligenciar todos los campos", "error");
                } else {
                    FichaService.GuardarModificacionAprendiz1($scope.Aprendiz).then(function (response) {
                        if (response.data.success == true) {
                            FichaService.ConsultarAprendiz1().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                    swal("Información", "La modificación del registro se realizo con exito", "success");
                                    $scope.deshabilitar();
                                }
                            });
                        }
                    });
                }
            }


            $scope.habilitar = function () {

                $("#NombreAprendizg").removeAttr("disabled");
                $("#ApellidoAprendiz1").removeAttr("disabled");
                $("#ApellidoAprendiz2").removeAttr("disabled");
                $("#correo2ga").removeAttr("disabled");
                $("#telefonoga").removeAttr("disabled");
                $("#modificarapg").hide();
                $("#guardar").show();


            }

            $scope.deshabilitar = function () {
                $("#NombreAprendizg").attr("disabled", "true");
                $("#ApellidoAprendiz1").attr("disabled", "true");
                $("#ApellidoAprendiz2").attr("disabled", "true");
                $("#correo2ga").attr("disabled", "true");
                $("#telefonoga").attr("disabled", "true");
                $("#modificarapg").show();
                $("#guardar").hide();


            }


            FichaService.ConsultarEmpresaSede().then(function (response) {
                if (response.data.success == true) {
                    $scope.Empresa = response.data.datos;
                }
            });


            $scope.GuardarGestion = function (response) {

                $scope.gestion.IdAprendiz = $scope.Aprendiz.IdAprendiz;
                $scope.gestion.IdAlternativa = $scope.alternativa.IdAlternativa;
                FichaService.GuardarGestion($scope.gestion).then(function (response) {

                    if (response.data.success == true) {
                        FichaService.ConsultarAprendiz1().then(function (response) {
                            if (response.data.success == true) {

                                swal("Información", "el registro se realizo con exito", "success");
                                $("#ModalGestion").modal("hide");

                            }
                        });
                    } else {
                        swal("Error", "Este aprendiz ya tiene una gestion individual", "error");

                    }


                })


            }

            $scope.VaciarCampos = function () {

                setTimeout(function () {

                    $("#listasem").val("").trigger("change");
                    $("#listase").val("").trigger("change");
                    $("#listas").val("").trigger("change");
                });

                $scope.gestion.Arl = "";
                $scope.gestion.FechaInicioProductiva = "";
                $scope.gestion.DescripcionSedido = "";
                $scope.EmpresaSedido = "";


            }


            $scope.Aprendiz = {
                IdAprendiz: "",
                TipoDocumento: "",
                Nombres: "",
                PrimerApellido: "",
                SegundoApellido: "",
                NumeroDocumento: "",
                Correo: "",
                Estado: "",
                Telefono: "",
                PoblacionDPS: true,
                IdFicha: "",
                IdUsuario: ""
            };

            //Iniciar tabla
            $scope.tabla = function (aprendices) {
                $scope.datalistare = aprendices;
                console($scope.datalistare, aprendices)
            }

            //Condicionados
            $scope.condicion = function (id) {
                if ($('#h' + id).is(':checked')) {
                    swal({
                        title: "Condicionado",
                        text: "Ingrese el motivo por el cual se encuentra condicionado",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        inputPlaceholder: "Ingrese..."
                    }, function (inputValue) {
                        if (inputValue === false) $('#h' + id).prop('checked', false);
                        if (inputValue === "") {
                            swal.showInputError("Debe ingresar algo.");
                            return false
                        }
                        $scope.condi.id = id;
                        $scope.condi.con = inputValue;
                        if ($('#h' + id).is(':checked')) {
                            FichaService.condicion($scope.condi).then(function (response) {
                                if (response.data.success) {
                                    $('#ModalFichaAprendiz').modal('show');
                                    swal("Correcto", "Se registro la condición satisfactoriamente", "success")


                                }
                            })
                        }
                    });
                }
            }


            //Guardar estado Aprendices
            $scope.GuardarAprendices = function () {
                var Momentos = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === false;
                });

                $.each($scope.Ficha, function (index, value) {
                    if (Momentos.length == 1) {
                        var IdFicha = $scope.Ficha.IdFicha;
                        $scope.MomentosFicha.IdFicha = IdFicha;
                    }
                });

                if ($scope.MomentosFicha.FechaUno != "" || $scope.MomentoUno != "" || $scope.MomentosFicha.FechaDos != "" || $scope.MomentoDos != "") {

                    FichaService.GuardarMomentosFicha($scope.MomentosFicha).then(function (response) {
                        if (response.data.success == true) {



                        }
                    })
                }
                var items = $scope.datalistar.filter(function (item) {
                    return item.Seleccionado === true;
                
                });
                if (items.length < 0) {
                    swal("Cuidado!", "Debe seleccionar almenos un aprendiz", "error");
                } else {

                    FichaService.GuardarLlamadoLista(items).then(function (response) {
                        if (response.data.success == true) {
                            FichaService.ConsultarAprendiz(Momentos[0].Parametro1).then(function (response) {
                                if (response.data.success == true) {
                                        $scope.datalistar = response.data.datos;
                                        $scope.ListaCompleta = response.data.datos;
                                        FichaService.ConsultarAprendizCondicionado(Momentos[0].Parametro1).then(function (responses) {
                                            for (var i = 0; i < responses.data.datos.length; i++) {
                                                $('#h' + responses.data.datos[i].IdAprendiz).prop('checked', true);
                                            }
                                        })
                                }
                            });



                        }

                        swal("Cuidado!", "Bien", "success");

                    });
                }
            }

            //Función 1 para editar los aprendices
            $scope.ModificarAprendiz = function (id) {

                FichaService.ModificarAprendiz(id).then(function (response) {
                    if (response.data.success == true) {
                        $scope.Aprendiz = response.data.Aprendiz;
                        var sf = $('#listaredit');
                        setTimeout(function () {
                            sf.val($scope.Aprendiz.IdFicha).trigger("change");
                        }, 100);
                        $("#ModalAprendizEditar").modal("show");
                    }
                });
            };

            //Función 2 para editar las Aprendices
            $scope.GuardarModicarAprendiz = function () {
                if ($scope.Aprendiz.NumeroDocumento == null || $scope.Aprendiz.Nombres == null || $scope.Aprendiz.PrimerApellido == null || $scope.Aprendiz.SegundoApellido == null || $scope.Aprendiz.Correo == null || $scope.Aprendiz.Telefono == null || $scope.Aprendiz.IdFicha == null) {
                    swal("Información", "Debe diligenciar todos los campos", "error");
                } else {
                    FichaService.GuardarModificacionAprendiz($scope.Aprendiz).then(function (response) {
                        if (response.data.success == true) {
                            FichaService.ConsultarAprendiz().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalistar = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                    swal("Información", "La modificación del registro se realizo con exito", "success");
                                    $("#ModalAprendizEditar").modal("hide");
                                    $scope.VaciarCampos();
                                }
                            });
                        }
                    });
                }
            }

            //Consultar Aprendices Ficha
            $scope.AbrirAprendices = function () {
                var Momentos = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (Momentos.length == 1) {
                    $('#fichaaprendiz').val(Momentos[0].Parametro2);

                    FichaService.MomentosFicha(Momentos).then(function (response) {
                        if (response.data.success == true) {
                            $scope.MomentosFicha.MomentoUno = (response.data.dato.Parametro2 == "true") ? true : false;
                            $scope.MomentosFicha.MomentoDos = (response.data.dato.Parametro3 == "true") ? true : false;
                            $scope.MomentosFicha.FechaUno = response.data.dato.Parametro4.toString().substring(0, 10);
                            $scope.MomentosFicha.FechaDos = response.data.dato.Parametro5.toString().substring(0, 10);


                        }
                    });


                    FichaService.ModificarFicha(Momentos).then(function (response) {
                        if (response.data.success == true) {
                            $scope.Ficha.IdFicha = response.data.Ficha.IdFicha
                            $scope.Ficha.FechaMomentoDos = response.data.Ficha.FechaMomentoDos.toString().substring(0, 10);
                            $scope.Ficha.FechaMomentoTres = response.data.Ficha.FechaMomentoTres.toString().substring(0, 10);
                        }
                    });
                    FichaService.ConsultarAprendiz(Momentos[0].Parametro1).then(function (response) {
                        if (response.data.success == true) {
                            if (response.data.datos.length > 0) {
                                $scope.datalistar = response.data.datos;
                                $scope.ListaCompleta = response.data.datos;
                                FichaService.ConsultarAprendizCondicionado(Momentos[0].Parametro1).then(function (responses) {
                                    for (var i = 0; i < responses.data.datos.length; i++) {
                                        $('#h' + responses.data.datos[i].IdAprendiz).prop('checked', true);
                                    }
                                })
                                $("#ModalFichaAprendiz").modal("show");
                            } else {
                                swal("¡Ten cuidado!", "Primero debes asociar aprendices a la ficha", "warning");
                            }

                        }
                    });
                } else {
                    swal("¡Ten cuidado!", "Primero debes elegir la ficha a la que deseas hacer inducción de lectiva.");
                }
            }



            $scope.AbrirModalFichaAbierta = function () {
                $("#ModalFicha").modal("show");
                $scope.VaciarCampos();
            };

            $scope.UploadFileWebAbierta = function () {
                $("#UploadFileWebAbierta").trigger('click');
            };

            $("#UploadFileWebAbierta").change(function () {
                dataweb = new FormData();

                var files = $("#UploadFileWebAbierta").get(0).files;

                //
                var fileExtension = ['xlsx'];
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    bootbox.dialog({
                        title: "Importar Archivo",
                        message: "La extencion del archivo no es valida",
                        buttons: {
                            success: {
                                label: "Cerrar",
                                className: "btn-primary",
                            }
                        }
                    });
                    $("#UploadFileWebAbierta").replaceWith($("#UploadFileWebAbierta").val('').clone(true));

                    return false;

                }


                // Add the uploaded image content to the form data collection
                if (files.length > 0) {

                    readURL(this, "logoweb");

                    dataweb.append("UploadedImage", files[0]);
                    if (dataweb != null) {
                        FichaService.SubirArchivo(dataweb, function (response) {
                            if (response.success) {

                                bootbox.dialog({
                                    title: "Importar Archivo",
                                    message: "La importación del archivo se realizó con éxito ",
                                    buttons: {
                                        success: {
                                            label: "Cerrar",
                                            className: "btn-primary",
                                        }
                                    }
                                });



                                $scope.path = response.path;

                                $("#UploadFileWebAbierta").replaceWith($("#UploadFileWebAbierta").val('').clone(true));
                                FichaService.ConsultarFichasActivas().then(function (response) {
                                    if (response.data.success == true) {
                                        $scope.datalists = response.data.datos;
                                        $scope.ListaCompleta = response.data.datos;
                                    }
                                });

                                $("#UploadFileWebAbierta").replaceWith($("#UploadFileWebAbierta").val('').clone(true));

                                return;
                            }

                        });
                    }

                }

            });

            function readURL(input, control) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#' + control + '').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            };




            $scope.UploadFileWebCerrada = function () {
                $("#UploadFileWebCerrada").trigger('click');
            };

            $("#UploadFileWebCerrada").change(function () {
                dataweb = new FormData();

                var files = $("#UploadFileWebCerrada").get(0).files;

                //
                var fileExtension = ['xlsx'];
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    bootbox.dialog({
                        title: "Importar Archivo",
                        message: "La extencion del archivo no es valida",
                        buttons: {
                            success: {
                                label: "Cerrar",
                                className: "btn-primary",
                            }
                        }
                    });
                    $("#UploadFileWebCerrada").replaceWith($("#UploadFileWebCerrada").val('').clone(true));

                    return false;

                }


                // Add the uploaded image content to the form data collection
                if (files.length > 0) {

                    readURL(this, "logoweb");

                    dataweb.append("UploadedImage", files[0]);
                    if (dataweb != null) {
                        FichaService.SubirArchivo(dataweb, function (response) {
                            if (response.success) {

                                bootbox.dialog({
                                    title: "Importar Archivo",
                                    message: "La importación del archivo se realizó con éxito ",
                                    buttons: {
                                        success: {
                                            label: "Cerrar",
                                            className: "btn-primary",
                                        }
                                    }
                                });

                                $scope.path = response.path;
                                $("#UploadFileWebCerrada").replaceWith($("#UploadFileWebCerrada").val('').clone(true));
                                FichaService.ConsultarFichasActivas().then(function (response) {
                                    if (response.data.success == true) {
                                        $scope.datalists = response.data.datos;
                                        $scope.ListaCompleta = response.data.datos;
                                    }
                                });

                                $("#UploadFileWebCerrada").replaceWith($("#UploadFileWebCerrada").val('').clone(true));

                                return;
                            }
                        });
                    }
                }
            });

            function readURL(input, control) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#' + control + '').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            };



            $scope.AbrirModalArchivo = function () {
                $("#ModalArchivo").modal("show");
            };

            $("#atras").hide();

            //Datos de la ficha
            $scope.Ficha = {
                IdFicha: "",
                IdentificadorFicha: "",
                Jornada: "",
                Estado: "",
                TipoRespuesta: "",
                FechaInicioFicha: "",
                FechaFinFicha: "",
                EtapaFicha: "",
                ModalidadFormacion: "",
                AprendicesActivos: "",
                FechaLimite: "",
                FechaMomentoDos: "",
                FechaMomentoTres: "",
                FechaInicioProductiva: "",
                CapacitacionSGVA: true,
                TotalAprendices: "",
                IdDetalle: "",
                NumeroMesesEl: "",
                NumeroMesesEp: ""
            };


            //Datos de coordinación
            $scope.Coordinacion = {
                IdCoordinacion: "",
                NumeroDocumento: "",
                TipoDocumento: "",
                Nombres: "",
                Apellidos: "",
                Telefono: "",
                Correo: ""
            };

            //Datos del centro de formación
            $scope.Centro = {
                IdCentro: "",
                NombreCentro: "",
                IdRegional: "",
                Direccion: ""
            };


            //Datos del programa de formación
            $scope.Programa = {
                IdPrograma: "",
                CodigoPrograma: "",
                Nivel: "",
                IdArea: "",
                NombrePrograma: ""
            };


            //Datos momentos de la ficha
            $scope.MomentosFicha = {
                IdMomento: "",
                MomentoUno: "",
                MomentoDos: "",
                FechaUno: "",
                FechaDos: "",
                IdFicha: ""
            };

            $scope.curPage = 0;
            $scope.pageSize = 10;

            //Función para vaciar los campos
            $scope.VaciarCampos = function () {
                $scope.Ficha.IdFicha = "",
                    $scope.Ficha.IdentificadorFicha = "",
                    $scope.Ficha.Jornada = "",
                    $scope.Ficha.Estado = "",
                    $scope.Ficha.TipoRespuesta = "",
                    $scope.Ficha.FechaInicioFicha = "",
                    $scope.Ficha.FechaFinFicha = "",
                    $scope.Ficha.ModalidadFormacion = "",
                    $scope.Ficha.AprendicesActivos = "",
                    $scope.Ficha.FechaLimite = "",
                    $scope.Ficha.FechaMomentoDos = "",
                    $scope.Ficha.FechaMomentoTres = "",
                    $scope.Ficha.FechaInicioProductiva = "",
                    $scope.Ficha.CapacitacionSGVA = "",
                    $scope.Ficha.TotalAprendices = "",
                    $scope.Ficha.NumeroMesesEl = "",
                  $scope.Ficha.NumeroMesesEp = ""
            };

            //Datos fecha guardar
            $('#FechaInicio').datepicker({
                language: 'es',
                autoclose: true,
            });

            //Datos fecha guardar
            $('#FechaFin').datepicker({
                language: 'es',
                autoclose: true,
            });

            //Datos fecha modificar
            $('#FechaMomentoDos').datepicker({
                language: 'es',
                autoclose: true,
            });

            //Datos fecha modificar
            $('#FechaMomentoTres').datepicker({
                language: 'es',
                autoclose: true,
            });



            //Datos fecha modificar momento
            $('#FechaMomentoDEditar').datepicker({
                language: 'es',
                autoclose: true,
            });

            //Datos fecha modificar momento
            $('#FechaMomentoTEditar').datepicker({
                language: 'es',
                autoclose: true,
            });

            //Datos fecha modificar
            $('#FechaFin1').datepicker({
                language: 'es',
                autoclose: true,
            });

            //Datos fecha modificar
            $('#FechaInicio1').datepicker({
                language: 'es',
                autoclose: true,
            });


            //Función para selecionar el nivel academico
            $scope.SeleccionarNivelAcademico = function (programa) {

                $.each($scope.Programa, function (index, value) {

                    if (value.IdPrograma == programa) {

                        $("#Nivel > option[value='" + value.Nivel.toUpperCase() + "']").attr('selected', 'selected');
                        $scope.Ficha.TipoFormacion = value.Nivel.toUpperCase();
                    }
                });

                FichaService.ConsultarcoordinadorXPrograma(programa).then(function (response) {

                    if (response.data.success == true) {
                        $scope.Coordinacion = response.data.dato;
                    }
                });
            };

            //Función para consultar los Centros 
            FichaService.ConsultarCentrosss().then(function (response) {
                if (response.data.success == true) {
                    $scope.Centro = response.data.datos;
                }
            });

            //Función para seleccionar todos los datos de la tabla
            $scope.SeleccionarTodos = function () {
                $.each($scope.Datos, function (index, value) {
                    value.Seleccionado = $scope.SeleccionTodos;
                });
            };

            //Función para consultar las areas de formacion
            FichaService.ConsultarAreas().then(function (response) {

                if (response.data.success == true) {
                    $scope.Area = response.data.datos;
                }
            });

            //metodo para consultar una empresa
            FichaService.ConsultarEmpresa().then(function (response) {
                if (response.data.success == true) {
                    $scope.empresa = response.data.datos;
                }
            });

            //metodo para consultar programa x área
            $scope.ConsultarProgramaxArea = function (IdArea) {
                FichaService.ConsultarProgramaxArea(IdArea).then(function (response) {

                    if (response.data.success == true) {
                        $scope.Programa = response.data.programa;
                    }
                });
            }

            //Función para duardar los datos de la ficha
            $scope.Guardar = function () {
                $.each($scope.Programa, function (index, value) {
                    if (value.IdPrograma == $scope.Programa.IdPrograma) {
                        $scope.Ficha.IdPrograma = value.IdPrograma
                    }
                });
                $.each($scope.Centro, function (index, value) {
                    if (value.IdCentro == $scope.Centro.IdCentro) {
                        $scope.Ficha.IdCentro = value.IdCentro
                    }
                });
                $.each($scope.Coordinacion, function (index, value) {
                    if (value.Parametro1 == $scope.Coordinacion[0].Parametro1) {
                        $scope.Ficha.IdCoordinacion = value.Parametro1
                    }
                });

                var x = $("#FechaInicio").val().split('/');
                var y = $("#FechaFin").val().split('/');

                var StartDate = x[1] + "-" + x[2] + "-" + x[0];
                var EndDate = y[1] + "-" + y[2] + "-" + y[0];
                if (Date.parse(StartDate) >= Date.parse(EndDate)) {
                    swal("¡Ten cuidado!", "La fecha final debe ser mayor a la fecha inicial.");
                    return;
                }

                if ($scope.Ficha.IdentificadorFicha == "" || $scope.Ficha.TotalAprendices == null || $scope.Ficha.TipoRespuesta == "" ||
                    $scope.Ficha.FechaInicioFicha == "" || $scope.Ficha.FechaFinFicha == "" || $scope.Ficha.Jornada == "" || $scope.Ficha.ModalidadFormacion == "") {

                    swal("¡Ten cuidado!", "Debe diligenciar todos los campos.");
                } else {
                    FichaService.GuardarFicha($scope.Ficha).then(function (response) {

                        if (response.data.success == true) {
                            FichaService.ConsultarFichasActivas().then(function (response) {
                                if (response.data.success == true) {

                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                    $scope.numberOfPages = function () {
                                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                                    };
                                    $.each($scope.datalists, function (index, value) {
                                        var fechaInicio = value.Parametro6.toString().substring(0, 10);
                                        var fechaFin = value.Parametro7.toString().substring(0, 10);
                                        var fechaMomDos = value.Parametro11.toString().substring(0, 10);
                                        var FechaMomTres = value.Parametro12.toString().substring(0, 10);
                                        $scope.datalists[index].Parametro11 = fechaMomDos;
                                        $scope.datalists[index].Parametro12 = FechaMomTres;
                                        $scope.datalists[index].Parametro6 = fechaInicio;
                                        $scope.datalists[index].Parametro7 = fechaFin;
                                    });
                                }
                            });
                            swal("Registro exitoso!", "La ficha fue registrada correctamente", "success");
                            $("#ModalFicha").modal("hide");
                            $scope.VaciarCampos();
                        }
                    });

                }
            };

            //Función para consultar las fichas
            FichaService.ConsultarFichasActivas().then(function (response) {
                if (response.data.success == true) {

                    $scope.datalists = response.data.datos;
                    $scope.ListaCompleta = response.data.datos;

                    $scope.numberOfPages = function () {
                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                    };
                    $.each($scope.datalists, function (index, value) {
                        var fechaInicio = value.Parametro6.toString().substring(0, 10);
                        var fechaFin = value.Parametro7.toString().substring(0, 10);
                        var fechaMomDos = value.Parametro11.toString().substring(0, 10);
                        var FechaMomTres = value.Parametro12.toString().substring(0, 10);

                        $scope.datalists[index].Parametro11 = fechaMomDos;
                        $scope.datalists[index].Parametro12 = FechaMomTres;
                        $scope.datalists[index].Parametro6 = fechaInicio;
                        $scope.datalists[index].Parametro7 = fechaFin;
                    });
                }
            });

            //Funcion para realizar los momentos
            $scope.ModalMomentos = function () {
                var Momentos = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (Momentos.length == 1) {


                    FichaService.MomentosFicha(Momentos).then(function (response) {
                        if (response.data.success == true) {


                            $scope.MomentosFicha.MomentoUno = (response.data.dato.Parametro2 == "true") ? true : false;
                            $scope.MomentosFicha.MomentoDos = (response.data.dato.Parametro3 == "true") ? true : false;
                            $scope.MomentosFicha.FechaUno = response.data.dato.Parametro4.toString().substring(0, 10);
                            $scope.MomentosFicha.FechaDos = response.data.dato.Parametro5.toString().substring(0, 10);


                        }
                    });


                    FichaService.ModificarFicha(Momentos).then(function (response) {
                        if (response.data.success == true) {
                            $scope.Ficha.IdFicha = response.data.Ficha.IdFicha
                            $scope.Ficha.FechaMomentoDos = response.data.Ficha.FechaMomentoDos.toString().substring(0, 10);
                            $scope.Ficha.FechaMomentoTres = response.data.Ficha.FechaMomentoTres.toString().substring(0, 10);
                            $("#ModalMomento").modal("show");
                        }
                    });
                } else {
                    swal("¡Ten cuidado!", "Primero debes elegir la ficha a la que deseas hacer inducción de lectiva.");
                }

            };

            //Función para guardar Momentos de la ficha
            //$scope.GuardarMomento = function () {
            //    var Momentos = $scope.datalists.filter(function (item) {
            //        return item.Seleccionado === true;
            //    });

            //    $.each($scope.Ficha, function (index, value) {
            //        if (Momentos.length == 1) {
            //            var IdFicha = $scope.Ficha.IdFicha;
            //            $scope.MomentosFicha.IdFicha = IdFicha;
            //        }
            //    });

            //    if ($scope.MomentosFicha.FechaUno != "" | $scope.MomentoUno != "" | $scope.MomentosFicha.FechaDos != "" | $scope.MomentoDos != "") {
            //        FichaService.GuardarMomentosFicha($scope.MomentosFicha).then(function (response) {
            //            if (response.data.success == true) {
            //                $("#ModalMomento").modal("hide");
            //                swal("YEAAHHHH");
            //            }
            //        })
            //    } else {
            //        $("#ModalMomento").modal("hide");

            //    }
            //};

            //Función para modificar una ficha
            $scope.ModificarFicha = function () {
                var ModificarF = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                if (ModificarF.length == 1) {

                    FichaService.ModificarFicha(ModificarF).then(function (response) {

                        if (response.data.success == true) {
                            $scope.Ficha.IdFicha = response.data.Ficha.IdFicha
                            $scope.Ficha.IdentificadorFicha = response.data.Ficha.IdentificadorFicha;
                            $scope.Ficha.Jornada = response.data.Ficha.Jornada;
                            $scope.Ficha.Estado = response.data.Ficha.Estado;
                            $scope.Ficha.TipoRespuesta = response.data.Ficha.TipoRespuesta;
                            $scope.Ficha.FechaInicioFicha = response.data.Ficha.FechaInicioFicha.toString().substring(0, 10);
                            $scope.Ficha.FechaFinFicha = response.data.Ficha.FechaFinFicha.toString().substring(0, 10);
                            $scope.Ficha.ModalidadFormacion = response.data.Ficha.ModalidadFormacion;
                            $scope.Ficha.FechaMomentoDos = response.data.Ficha.FechaMomentoDos.toString().substring(0, 10);
                            $scope.Ficha.FechaMomentoTres = response.data.Ficha.FechaMomentoTres.toString().substring(0, 10);
                            $scope.Ficha.TotalAprendices = response.data.Ficha.TotalAprendices;
                            $scope.Ficha.NumeroMesesEp = response.data.Ficha.NumeroMesesEp;
                            $scope.Ficha.NumeroMesesEl = response.data.Ficha.NumeroMesesEl;


                            var sd = $('#Centross');
                            setTimeout(function () {
                                sd.val(response.data.Ficha.IdCentro).trigger("change");
                            }, 100);

                            $scope.Ficha.IdPrograma = response.data.Ficha.IdPrograma;

                            FichaService.ConsultarAreaxPrograma($scope.Ficha.IdPrograma).then(function (response) {

                                if (response.data.success == true) {


                                    var sd = $('#listas');
                                    setTimeout(function () {
                                        sd.val(response.data.dato.Parametro1).trigger("change");
                                    }, 100);

                                    FichaService.ConsultarProgramaxArea(response.data.dato.Parametro1).then(function (response) {

                                        if (response.data.success == true) {
                                            $scope.Programa = response.data.programa;

                                            var sp = $('#ListaProgramasModificar');
                                            setTimeout(function () {
                                                sp.val($scope.Ficha.IdPrograma).trigger("change");
                                            }, 100);

                                            $("#Oferta > option[value='" + $scope.Ficha.TipoRespuesta + "']").attr('selected', 'selected');

                                            $("#TipoFormacion > option[value='" + $scope.Ficha.TipoFormacion + "']").attr('selected', 'selected');
                                            $("#Jornada > option[value='" + $scope.Ficha.Jornada + "']").attr('selected', 'selected');
                                            $("#ModalEditar").modal("show");
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    swal("¡Ten cuidado!", "Primero debes elegir la ficha que deseas modificar.");
                }
            };

            //Función para modificar una ficha
            $scope.DetallesFicha = function (id) {
    
                FichaService.ModificarFic(id).then(function (response) {
                    if (response.data.success == true) {
                        $scope.Ficha.IdFicha = response.data.Ficha.IdFicha
                        $scope.Ficha.IdentificadorFicha = response.data.Ficha.IdentificadorFicha;
                        $scope.Ficha.Jornada = response.data.Ficha.Jornada;
                        $scope.Ficha.Estado = response.data.Ficha.Estado;
                        $scope.Ficha.TipoRespuesta = response.data.Ficha.TipoRespuesta;
                        $scope.Ficha.FechaInicioFicha = response.data.Ficha.FechaInicioFicha.toString().substring(0, 10);
                        $scope.Ficha.FechaFinFicha = response.data.Ficha.FechaFinFicha.toString().substring(0, 10);
                        $scope.Ficha.ModalidadFormacion = response.data.Ficha.ModalidadFormacion;
                        $scope.Ficha.FechaMomentoDos = response.data.Ficha.FechaMomentoDos.toString().substring(0, 10);
                        $scope.Ficha.FechaMomentoTres = response.data.Ficha.FechaMomentoTres.toString().substring(0, 10);
                        $scope.Ficha.TotalAprendices = response.data.Ficha.TotalAprendices;
                        $scope.Ficha.NumeroMesesEl = response.data.Ficha.NumeroMesesEl;
                        $scope.Ficha.NumeroMesesEp = response.data.Ficha.NumeroMesesEp;


                        var sd = $('#Centross');
                        setTimeout(function () {
                            sd.val(response.data.Ficha.IdCentro).trigger("change");
                        }, 100);

                        $scope.Ficha.IdPrograma = response.data.Ficha.IdPrograma;

                        FichaService.ConsultarAreaxPrograma($scope.Ficha.IdPrograma).then(function (response) {

                            if (response.data.success == true) {


                                var sd = $('#listas');
                                setTimeout(function () {
                                    sd.val(response.data.dato.Parametro1).trigger("change");
                                }, 100);

                                FichaService.ConsultarProgramaxArea(response.data.dato.Parametro1).then(function (response) {

                                    if (response.data.success == true) {
                                        $scope.Programa = response.data.programa;

                                        var sp = $('#ListaProgramasModificar');
                                        setTimeout(function () {
                                            sp.val($scope.Ficha.IdPrograma).trigger("change");
                                        }, 100);

                                        $("#Oferta > option[value='" + $scope.Ficha.TipoRespuesta + "']").attr('selected', 'selected');

                                        $("#TipoFormacion > option[value='" + $scope.Ficha.ModalidadFormacion + "']").attr('selected', 'selected');
                                        $("#Jornada > option[value='" + $scope.Ficha.Jornada + "']").attr('selected', 'selected');
                                        $("#ModalDetalles").modal("show");
                                    }
                                });
                            }
                        });
                    }
                });
            };


            //Función para guardar la edición de unas ficha
            $scope.GuardarEdicionFicha = function () {
                $.each($scope.Programa, function (index, value) {
                    if (value.NombrePrograma == $scope.Programa.NombrePrograma) {
                        $scope.Ficha.IdPrograma = value.IdPrograma
                    }
                });

                if ($scope.Ficha.IdentificadorFicha == "" || $scope.Ficha.TotalAprendices == "" || $scope.Ficha.IdPrograma == "" || $scope.Ficha.TipoDocumento == "" ||
                    $scope.Ficha.FechaInicioFicha == "" || $scope.Ficha.FechaFinFicha == "" || $scope.Ficha.Jornada == "" ||
                    $scope.Ficha.ModalidadFormacion == "" || $scope.Ficha.TipoRespuesta == "" || $scope.Ficha.NumeroMesesEp == "") {
                    swal("¡Ten cuidado!", "Primero debes diligenciar todos los campos.");
                } else {
                    FichaService.GuardarModificacionFicha($scope.Ficha).then(function (response) {
                        if (response.data.success == true) {
                            $("#ModalEditar").modal("hide");

                            FichaService.ConsultarFichasActivas().then(function (response) {
                                if (response.data.success == true) {

                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                    $scope.numberOfPages = function () {
                                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                                    };
                                    $.each($scope.datalists, function (index, value) {
                                        var fechaInicio = value.Parametro6.toString().substring(0, 10);
                                        var fechaFin = value.Parametro7.toString().substring(0, 10);
                                        var fechaMomDos = value.Parametro11.toString().substring(0, 10);
                                        var FechaMomTres = value.Parametro12.toString().substring(0, 10);
                                        $scope.datalists[index].Parametro11 = fechaMomDos;
                                        $scope.datalists[index].Parametro12 = FechaMomTres;
                                        $scope.datalists[index].Parametro6 = fechaInicio;
                                        $scope.datalists[index].Parametro7 = fechaFin;
                                    });
                                }
                            });
                        }
                    });
                }

            };

            //Consultar fichas terminadas por unificación 
            $scope.FichasInactivasPorUnificacion = function () {
                FichaService.ConsultarFichasInactivas().then(function (response) {

                    if (response.data.success == true) {

                        $scope.datalists = response.data.datos;
                        $scope.ListaCompleta = response.data.datos;
                        $scope.numberOfPages = function () {
                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                        };
                        $.each($scope.datalists, function (index, value) {
                            var fechaInicio = value.Parametro6.toString().substring(0, 10);
                            var fechaFin = value.Parametro7.toString().substring(0, 10);
                            var fechaMomDos = value.Parametro11.toString().substring(0, 10);
                            var FechaMomTres = value.Parametro12.toString().substring(0, 10);

                            $scope.datalists[index].Parametro11 = fechaMomDos;
                            $scope.datalists[index].Parametro12 = FechaMomTres;
                            $scope.datalists[index].Parametro6 = fechaInicio;
                            $scope.datalists[index].Parametro7 = fechaFin;
                        });
                    }
                });

                $("#Unificar").hide();
                $("#excel").hide();
                $("#modificar").hide();
                $("#inhabilitadasUnificacion").hide();
                $("#inhabilitadasTiempo").hide();
                $("#atras").show();

            };

            $scope.atras = function () {
                $("#Unificar").show();
                $("#excel").show();
                $("#modificar").show();
                $("#inhabilitadasUnificacion").show();
                $("#inhabilitadasTiempo").show();
                $("#atras").hide();

                FichaService.ConsultarFichasActivas().then(function (response) {
                    if (response.data.success == true) {
                        $scope.datalists = response.data.datos;
                        $scope.ListaCompleta = response.data.datos;
                        $scope.numberOfPages = function () {
                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                        };
                        $.each($scope.datalists, function (index, value) {
                            var fechaInicio = value.Parametro6.toString().substring(0, 10);
                            var fechaFin = value.Parametro7.toString().substring(0, 10);
                            var fechaMomDos = value.Parametro11.toString().substring(0, 10);
                            var FechaMomTres = value.Parametro12.toString().substring(0, 10);

                            $scope.datalists[index].Parametro11 = fechaMomDos;
                            $scope.datalists[index].Parametro12 = FechaMomTres;
                            $scope.datalists[index].Parametro6 = fechaInicio;
                            $scope.datalists[index].Parametro7 = fechaFin;
                        });
                    }
                });
            };


            //Consultar fichas terminadas por Fecha 
            $scope.FichasInactivasPorTiempo = function () {
                FichaService.ConsultarFichasInactivasFecha().then(function (response) {

                    if (response.data.success == true) {

                        $scope.datalists = response.data.datos;
                        $scope.ListaCompleta = response.data.datos;
                        $scope.numberOfPages = function () {
                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                        };
                        $.each($scope.datalists, function (index, value) {
                            var fechaInicio = value.Parametro6.toString().substring(0, 10);
                            var fechaFin = value.Parametro7.toString().substring(0, 10);
                            var fechaMomDos = value.Parametro11.toString().substring(0, 10);
                            var FechaMomTres = value.Parametro12.toString().substring(0, 10);

                            $scope.datalists[index].Parametro11 = fechaMomDos;
                            $scope.datalists[index].Parametro12 = FechaMomTres;
                            $scope.datalists[index].Parametro6 = fechaInicio;
                            $scope.datalists[index].Parametro7 = fechaFin;
                        });
                    }
                });

                $("#Unificar").hide();
                $("#excel").hide();
                $("#modificar").hide();
                $("#inhabilitadasUnificacion").hide();
                $("#inhabilitadasTiempo").hide();
                $("#atras").show();

            };

            $scope.atras = function () {
                $("#Unificar").show();
                $("#excel").show();
                $("#modificar").show();
                $("#inhabilitadasUnificacion").show();
                $("#inhabilitadasTiempo").show();
                $("#atras").hide();

                FichaService.ConsultarFichasActivas().then(function (response) {
                    if (response.data.success == true) {
                        $scope.datalists = response.data.datos;
                        $scope.ListaCompleta = response.data.datos;
                        $scope.numberOfPages = function () {
                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                        };
                        $.each($scope.datalists, function (index, value) {
                            var fechaInicio = value.Parametro6.toString().substring(0, 10);
                            var fechaFin = value.Parametro7.toString().substring(0, 10);
                            var fechaMomDos = value.Parametro11.toString().substring(0, 10);
                            var FechaMomTres = value.Parametro12.toString().substring(0, 10);

                            $scope.datalists[index].Parametro11 = fechaMomDos;
                            $scope.datalists[index].Parametro12 = FechaMomTres;
                            $scope.datalists[index].Parametro6 = fechaInicio;
                            $scope.datalists[index].Parametro7 = fechaFin;
                        });
                    }
                });


            };



            //Función para Unificar fichas
            $scope.UnificarFicha = function () {

                var ModificarF = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });


                if (ModificarF.length == 1) {



                    FichaService.ModificarFicha(ModificarF).then(function (response) {

                        if (response.data.success == true) {

                            $scope.Ficha.IdFicha = response.data.Ficha.IdFicha

                            $scope.Ficha.NumFicha = response.data.Ficha.NumFicha;
                            $scope.Ficha.Estado = response.data.Ficha.Estado;
                            $scope.Ficha.IdPrograma = response.data.Ficha.IdPrograma;


                            FichaService.ConsultarAreaxPrograma(response.Ficha.IdPrograma).then(function (response) {

                                if (response.data.success == true) {

                                    $("#Areass > option[value='" + response.data.dato.Parametro1 + "']").attr('selected', 'selected');

                                    FichaService.ConsultarProgramaxArea(response.data.dato.Parametro1).then(function (response) {

                                        if (response.data.success == true) {
                                            $scope.Programa = response.data.programa;
                                            setTimeout(function () {
                                                $("#programasss > option[value='" + response.data.programa[0].IdPrograma + "']").attr('selected', 'selected');
                                            }, 500);


                                            FichaService.ConsultarFichasxPrograma($scope.Ficha.IdPrograma).then(function (response) {
                                                if (response.data.success == true) {
                                                    $scope.Ficha = response.data.datos;
                                                    setTimeout(function () {
                                                        $("#fichasss > option[value='" + response.data.datos.IdFicha + "']").attr('selected', 'selected');
                                                    }, 500);

                                                    //$scope.Ficha1.IdFicha = response.datos.IdFicha
                                                    //$scope.Ficha1.NumFicha = response.datos.NumFicha;
                                                    //$scope.Ficha1.Estado = response.datos.Estado;
                                                    //$scope.Ficha1.IdPrograma = response.datos .IdPrograma;


                                                }

                                            });
                                            $("#ModalUnificar").modal("show");
                                        }
                                    });

                                }
                            });
                        }
                    });
                } else {
                    bootbox.dialog({
                        title: "Unificar",
                        message: "Debe seleccionar una ficha",
                        buttons: {
                            success: {
                                label: "Cerrar",
                                className: "btn-primary",
                            }
                        }
                    });
                }
            };



            //Función para guardar unificación de fichas
            $scope.GuardarUnificarFichas = function () {

                $.each($scope.Ficha, function (index, value) {
                    if (value.NumFicha == $scope.Ficha[0].NumFicha) {
                        $scope.Ficha[0].IdFicha = value.IdFicha
                    }
                });


                FichaService.UnificarFicha($scope.Ficha).then(function (response) {
                    if (response.data.success == true) {
                        $("#ModalUnificar").modal("hide");
                        FichaService.ConsultarFichasActivas().then(function (response) {
                            if (response.data.success == true) {

                                $scope.datalists = response.data.datos;
                                $scope.ListaCompleta = response.data.datos;
                                $scope.numberOfPages = function () {
                                    return Math.ceil($scope.datalists.length / $scope.pageSize);
                                };
                                $.each($scope.datalists, function (index, value) {
                                    var fechaInicio = value.Parametro6.toString().substring(0, 10);
                                    var fechaFin = value.Parametro7.toString().substring(0, 10);
                                    var fechaMomDos = value.Parametro11.toString().substring(0, 10);
                                    var FechaMomTres = value.Parametro12.toString().substring(0, 10);
                                    $scope.datalists[index].Parametro11 = fechaMomDos;
                                    $scope.datalists[index].Parametro12 = FechaMomTres;
                                    $scope.datalists[index].Parametro6 = fechaInicio;
                                    $scope.datalists[index].Parametro7 = fechaFin;
                                });
                            }
                        });
                    }
                });

            };


            $scope.Filter = function (e) {
                var Busqueda = $("#Buscar").val();
                var exp = new RegExp(Busqueda);
                if (Busqueda == "") {

                    FichaService.ConsultarFichasActivas().then(function (response) {
                        if (response.data.success == true) {
                            $scope.datalists = response.data.datos;
                            $scope.ListaCompleta = response.data.datos;
                            $.each($scope.datalists, function (index, value) {
                                var fechaInicio = value.Parametro6.toString().substring(0, 10);
                                var fechaFin = value.Parametro7.toString().substring(0, 10);
                                var fechaMomDos = value.Parametro11.toString().substring(0, 10);
                                var FechaMomTres = value.Parametro12.toString().substring(0, 10);


                                $scope.datalists[index].Parametro11 = fechaMomDos;
                                $scope.datalists[index].Parametro12 = FechaMomTres;
                                $scope.datalists[index].Parametro6 = fechaInicio;
                                $scope.datalists[index].Parametro7 = fechaFin;
                            });
                            $scope.numberOfPages = function () {
                                return Math.ceil($scope.datalists.length / $scope.pageSize);
                            };
                        }
                    });

                }
                var Ficha = [];
                $scope.datalists = $scope.ListaCompleta;
                Ficha = $scope.datalists.filter(function (item) {

                    if (exp.test(item.Parametro1.toLowerCase()) || exp.test(item.Parametro1.toUpperCase())) {

                        return item;
                    }

                    else if (exp.test(item.Parametro2.toLowerCase()) || exp.test(item.Parametro2.toUpperCase())) {
                        return item;
                    }

                    else if (exp.test(item.Parametro3.toLowerCase()) || exp.test(item.Parametro3.toUpperCase())) {
                        return item;
                    }

                    else if (exp.test(item.Parametro4.toLowerCase()) || exp.test(item.Parametro4.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Parametro6.toLowerCase()) || exp.test(item.Parametro6.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Parametro7.toLowerCase()) || exp.test(item.Parametro7.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Parametro8.toLowerCase()) || exp.test(item.Parametro8.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Parametro9.toLowerCase()) || exp.test(item.Parametro9.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Parametro10.toLowerCase()) || exp.test(item.Parametro10.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Parametro11.toLowerCase()) || exp.test(item.Parametro11.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Parametro12.toLowerCase()) || exp.test(item.Parametro12.toUpperCase())) {
                        return item;
                    }


                });




                //Variable para setear la paginación 
                $scope.curPage = 0;
            };

        }]);