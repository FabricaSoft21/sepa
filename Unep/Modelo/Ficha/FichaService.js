﻿ProgramacionApp.factory('FichaService',
    ['$http', '$rootScope', '$routeParams',
    function ($http, $rootScope, $routeParams) {
        var service = {};

        service.SubirArchivo = function (data, callback) {
            waitingDialog.show();
            var ajaxRequest = $.ajax({
                type: "POST",
                url: URLServices + "File/UploadFileFicha",
                contentType: false,
                processData: false,
                data: data
            }).done(function (responseData, textStatus) {
                callback(responseData);
                waitingDialog.hide();
            }).fail(function () {
                waitingDialog.hide();
            });
        };

        service.ModificarAprendiz1 = function (doc) {
            item = {
                Parametro1: doc
            };
            return $http({
                method: 'POST',
                url: URLServices + "Gestion/ModificarAprendiz/",
                data: item
            })
        };

        service.ConsultarAlternativa = function () {
            return $http({
                method: 'GET',
                url: URLServices + "Gestion/ConsultarAlternativa/"
            })
        };

        service.selectfecha = function (doc) {
            var Item = {
                Parametro1: doc
            }
            return $http({
                method: 'POST',
                url: URLServices + "Gestion/selectfecha/",
                data: Item
            })
        };

        service.ConsultarEmpresaSede = function () {
            return $http({
                method: 'GET',
                url: URLServices + "Gestion/ConsultarEmpresaSede/"
            })
        };

       

        service.GuardarGestion = function (gestion) {
            return $http({
                method: 'POST',
                url: URLServices + "Gestion/GuardarGestion/",
                data: gestion
            })
        };



        service.ConsultarAprendiz1 = function () {
            return $http({
                method: 'GET',
                url: URLServices + "Aprendiz/ConsultarAprendiz/"
            })
        };




        service.GuardarModificacionAprendiz = function (Aprendiz) {
            return $http({
                method: 'POST',
                url: URLServices + "Aprendiz/GuardarModificacionAprendiz/",
                data: Aprendiz
            })
        };

        service.GuardarLlamadoLista = function (aprendices) {
            return $http({
                method: 'POST',
                url: URLServices + "Ficha/GuardarLlamadoLista",
                data: aprendices

            })
        };

        service.GuardarModificacionAprendiz1 = function (Aprendiz) {
            return $http({
                method: 'POST',
                url: URLServices + "Gestion/GuardarModificacionAprendiz/",
                data: Aprendiz
            })
        };


        service.ModificarAprendiz = function (Aprendiz) {
            item = {
                Parametro1: Aprendiz
            };
            return $http({
                method: 'POST',
                url: URLServices + "Aprendiz/ModificarAprendiz/",
                data: item
            })
        };

        service.ModificarFic = function (ficha) {
            item = {
                Parametro1: ficha
            };
            return $http({
                method: 'POST',
                url: URLServices + "Ficha/ModificarFicha/",
                data: item
            })
        };

        service.ConsultarAprendiz = function (id) {
            item = {
                Parametro1: id
            }
            return $http({
                method: 'POST',
                url: URLServices + "Ficha/ConsultarAprendiz/",
                data: item
            })
        };



        service.condicion = function (condiciones) {
            item = {
                Parametro1: condiciones.id,
                Parametro2: condiciones.con
            }
            return $http({
                method: 'POST',
                url: URLServices + "Ficha/Condiciones/",
                data: item
            })
        };

        service.ConsultarAprendizCondicionado = function (id) {
            item = {
                Parametro1: id
            }
            return $http({
                method: 'POST',
                url: URLServices + "Ficha/ConsultarAprendizCondicionado/",
                data: item
            })
        };

        service.GuardarFicha = function (Ficha, callback) {
            return $http({
                method: 'POST',
                url: URLServices + "Ficha/GuardarFicha/",
                data: Ficha
            })
        };

        service.ConsultarCentrosss = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Coordinacion/ConsultarCentros/"
            })
        };

        service.ConsultarEmpresa = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Empresas/ConsultarEmpresa/"
            })
        };

        service.ConsultarFichas = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Ficha/ConsultarFichas/"
            })
        };

        service.ConsultarFichasActivas = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Ficha/ConsultarFichasActivas/"
            })
        };


        service.ConsultarAreas = function (callback) {

            return $http({
                method: 'GET',
                url: URLServices + "Area/ConsultarAreas/"
            })
        };

        service.ConsultarProgramaxArea = function (IdArea, callback) {

            item = {
                Parametro1: IdArea
            };
            return $http({
                method: 'POST',
                url: URLServices + "Programa/ConsultarProgramaxArea/",
                data: item
            })
        };

        service.ConsultarcoordinadorXPrograma = function (IdPrograma, callback) {

            item = {
                Parametro1: IdPrograma
            };

            return $http({
                method: 'POST',
                url: URLServices + "Ficha/ConsultarCoordiXprograma/",
                data: item
            })
        };

        service.ConsultarFichasInactivas = function (callback) {

            return $http({
                method: 'GET',
                url: URLServices + "Ficha/ConsultarFichasInactivas/"
            })
        };

        service.ConsultarFichasInactivasFecha = function (callback) {

            return $http({
                method: 'GET',
                url: URLServices + "Ficha/ConsultarFichasInactivasTiempo/"
            })

        };

        service.BorrarFicha = function (Ficha, callback) {
            var Item = {
                Parametros: []
            };

            $.each(Ficha, function (index, value) {
                Item.Parametros.push(
                    {
                        Parametro1: value.Parametro1
                    })
            });

            return $http({
                method: 'POST',
                url: URLServices + "Ficha/EliminarFicha/",
                data: Item
            })
        };

        service.ModificarFicha = function (Ficha, callback) {
            return $http({
                method: 'POST',
                url: URLServices + "Ficha/ModificarFicha/",
                data: Ficha[0]
            })
        };

        service.MomentosFicha = function (Ficha, callback) {
            return $http({
                method: 'POST',
                url: URLServices + "Ficha/ConsultarMomento/",
                data: Ficha[0]
            })
        };

        service.GuardarMomentosFicha = function (Ficha, callback) {

            return $http({
                method: 'POST',
                url: URLServices + "Ficha/GuardarMomentoFicha/",
                data: Ficha
            })
        };

        service.GuardarModificacionFicha = function (Ficha, callback) {

            return $http({
                method: 'POST',
                url: URLServices + "Ficha/GuardarModificacionFicha/",
                data: Ficha
            })
        };

        service.ConsultarProgramas = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Ficha/ConsultarProgramas/"
            })
        };

        service.ConsultarAreaxPrograma = function (IdPrograma,callback) {
            var Item = {
                Parametro1: IdPrograma
            };

            return $http({
                method: 'POST',
                url: URLServices + "Ficha/ConsultarAreaxPrograma/",
                data: Item
            })
        };

        service.ConsultarFichasxPrograma = function (IdFIcha, callback) {
            item = {
                Parametro1: IdFIcha
            };
            return $http({
                method: 'POST',
                url: URLServices + "Ficha/ConsultarFichasxPrograma/",
                data: item
            })
        };

        service.UnificarFicha = function (Ficha, callback) {
            return $http({
                method: 'POST',
                url: URLServices + "Ficha/UnificarFicha/",
                data: Ficha
            })
        };
        return service;
    }]);