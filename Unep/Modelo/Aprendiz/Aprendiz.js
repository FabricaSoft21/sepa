﻿ProgramacionApp.controller('AprendizController',
    ['$scope', '$rootScope', '$location', 'AprendizService', '$routeParams', '$sce',
        function ($scope, $rootScope, $location, AprendizService, $routeParams, $sce) {

            //Subir excel Aprendiz
            $scope.SubirArchivo = function () {
                AprendizService.alerta();
            }

            //Datos del Aprendiz
            $scope.Aprendiz = {
                IdAprendiz: "",
                TipoDocumento: "",
                Nombres: "",
                PrimerApellido: "",
                SegundoApellido: "",
                NumeroDocumento: "",
                Correo: "",
                Estado: "",
                Telefono: "",
                PoblacionDPS: true,
                IdFicha: "",
                IdUsuario: ""
            };

            //Datos del Registro de error
            $scope.Error = {
                Nombres: "",
                NumeroDocumento: "",
                Descripcion: ""
            };

            //Datos de la ficha
            $scope.Ficha = {
                IdFicha: "",
                NumFicha: "",
                NumAprendices: "",
                Estado: "",
                TipoFormacion: "",
                FechaInicio: "",
                FechaFin: "",
                FechaLimite: "",
                FechaMomentoDos: "",
                FechaMomentoTres: "",
                Jornada: "",
                IdPrograma: ""
            };

            $scope.UploadFileWeb = function () {
                $("#fileUploadWeb").trigger('click');
            };
            $("#fileUploadWeb").change(function () {
                dataweb = new FormData();
                var files = $("#fileUploadWeb").get(0).files;
                var fileExtension = ['xlsx'];
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    swal("Importar Archivo", "La extencion del archivo no es valida", "error");
                    $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                    return false;
                }
                // Add the uploaded image content to the form data collection
                if (files.length > 0) {
                    readURL(this, "logoweb");
                    dataweb.append("UploadedImage", files[0]);
                    if (dataweb != null) {
                        AprendizService.SubirArchivo(dataweb, function (response) {
                            if (response.success) {
                                console.log(response.datos)
                                if (response.datos.length == 0) {
                                    swal("Importar Archivo", "La importación del archivo se realizó con éxito", "success");
                                } else {
                                    $scope.Error = response.datos;
                                    $('#ModalErrores').modal("show");
                                }
                                $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                                AprendizService.ConsultarAprendiz().then(function (response) {
                                    if (response.data.success == true) {
                                        $scope.
                                            s = response.data.datos;
                                        $scope.ListaCompleta = response.data.datos;
                                        $scope.curPage = 0;
                                        $scope.pageSize = 10;
                                        $scope.numberOfPages = function () {
                                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                                        };
                                    }
                                });
                                $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                                return;
                            }
                        });
                    }
                }
            });

            function readURL(input, control) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#' + control + '').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            };


            //Abrir modal registro
            $scope.AbrirModal = function () {
                AprendizService.ConsultarFicha().then(function (response) {
                    if (response.data.success == true) {
                        $scope.Ficha = response.data.datos;

                    }
                });
                $("#ModalAprendizRegistrar").modal("show");
                $scope.VaciarCampos();
            };


            //Abrir modal Ficha
            $scope.CambiarFicha = function () {
                var fichas = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                if (fichas.length == 1) {
                    var sf = $('#listarFichaC');
                    setTimeout(function () {
                        sf.val("").trigger("change");
                    }, 100);
                    $("#ficha").val(fichas[0].IdentificadorFicha)
                    for (var i = 0; i < $scope.Ficha.length; i++) {
                        if ($scope.Ficha[i].IdentificadorFicha == fichas[0].IdentificadorFicha) {
                            $scope.Ficha.splice(i, 1);
                            break;
                        }
                    }
                    $("#ModalFicha").modal("show");
                } else {
                    swal("¡Ten cuidado!", "Debe seleccionar un aprendiz");
                }
                
            };

            //Guardar modificacion de ficha
            $scope.CambiarlaFicha = function () {
                var fichas = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                var fic = $("#listarFichaC").val();
                $scope.Aprendiz.NumeroDocumento = fichas[0].NumeroDocumento;
                $scope.Aprendiz.IdFicha = fic;
                AprendizService.ActualizarFicha($scope.Aprendiz).then(function (response) {
                    if (response.data.success == true) {
                        AprendizService.ConsultarAprendiz().then(function (response) {
                            if (response.data.success == true) {
                                $scope.datalists = response.data.datos;
                                $scope.ListaCompleta = response.data.datos;
                                swal("Información","La modificación del registro se realizo con exito","success")
                                $("#ModalFicha").modal("hide");
                            }
                        });
                    }
                });
                AprendizService.ConsultarFicha().then(function (response) {
                    if (response.data.success == true) {
                        $scope.Ficha = response.data.datos;

                    }
                });

            };


            //Registrar Aprendiz
            $scope.Guardar = function () {
                $.each($scope.Ficha, function (Sindex, value) {
                    if (value.IdFicha == $scope.Ficha.IdFicha) {
                        $scope.Aprendiz.IdFicha = value.IdFicha;
                    }
                });

                if ($scope.Aprendiz.NumeroDocumento == "" || $scope.Aprendiz.TipoDocumento == "" || $scope.Aprendiz.Nombres == "" || $scope.Aprendiz.PrimerApellido == "" || $scope.Aprendiz.SegundoApellido == "" || $scope.Aprendiz.Correo == "" || $scope.Aprendiz.Telefono == "" || $scope.Aprendiz.IdFicha == "") {
                    swal("Información", "Debe diligenciar todos los campos", "error");
                } else {
                    AprendizService.Email($scope.Aprendiz).then( function (response) {
                        if (response.data.success == true) {
                            AprendizService.GuardarAprendiz($scope.Aprendiz).then(function (response) {
                                if (response.data.success == true) {
                                    AprendizService.ConsultarAprendiz().then(function (response) {
                                        if (response.data.success == true) {
                                            $scope.datalists = response.data.datos;
                                            $scope.ListaCompleta = response.data.datos;
                                            $scope.VaciarCampos();
                                            $("#ModalAprendizRegistrar").modal("hide");
                                            swal("Información", "El registro se realizó con éxito", "success");
                                            $('#labelaprendi').hide();
                                        }
                                    });
                                } else {
                                    swal("Información", "El aprendiz ya se encuentra registrado", "error");
                                }
                            }
                                );
                        } else {
                            swal("Información", "El correo no tiene el formato correcto", "error")
                        }
                    });
                }
            }

            //Consultar ficha select
            AprendizService.ConsultarFicha().then(function (response) {
                if (response.data.success == true) {
                    $scope.Ficha = response.data.datos;

                }
            });

            //Consultar Aprendiz
            AprendizService.ConsultarAprendiz().then(function (response) {
                if (response.data.success == true) {
                    if (response.data.datos.length > 0) {
                        $('#labelaprendi').hide();
                        $scope.curPage = 0;
                        $scope.pageSize = 10;
                        $scope.datalists = response.data.datos;
                        $scope.ListaCompleta = response.data.datos;
                        $scope.numberOfPages = function () {
                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                        };

                        $scope.Datos = $scope.datalists;
                    } else {
                        $('#labelaprendi').show();
                    }
                }
            });
                
            //Limpiar campos
            $scope.VaciarCampos = function () {
                $scope.Aprendiz.IdAprendiz = "",
                $scope.Aprendiz.Nombres = "",
                $scope.Aprendiz.PrimerApellido = "",
                $scope.Aprendiz.SegundoApellido = "",
                $scope.Aprendiz.NumeroDocumento = "",
                $scope.Aprendiz.Correo = "",
                $scope.Aprendiz.Estado = "",
                $scope.Aprendiz.Telefono = "",
                $scope.Aprendiz.PoblacionDPS = "",
                $scope.Aprendiz.IdFicha = "",
                $scope.Aprendiz.IdUsuario = ""

            };

            //Seleccionar
            $scope.SeleccionarTodos = function () {

                contador = (($scope.curPage + 1) * 3) - 3;
                var item = (($scope.curPage + 1) * 3) - 3;
                var items1 = (($scope.curPage + 1) * 3);


                $.each($scope.datalists, function (index, value) {

                    if (contador < items1) {
                        value[contador];
                        value.Seleccionado = $scope.SeleccionTodos;
                        contador++;

                    }
                });


            };

            //Función 1 para editar los aprendices
            $scope.Modificar = function () {
                AprendizService.ConsultarFicha().then(function (response) {
                    if (response.data.success == true) {
                        $scope.Ficha = response.data.datos;

                    }
                });
                var AprendizM = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                if (AprendizM.length == 1) {
                    AprendizService.ModificarAprendiz(AprendizM).then(function (response) {
                        if (response.data.success == true) {
                            $scope.Aprendiz = response.data.Aprendiz;
                            var sf = $('#listaredit');
                            setTimeout(function () {
                                sf.val($scope.Aprendiz.IdFicha).trigger("change");
                            }, 100);
                            $("#ModalAprendizEditar").modal("show");
                        }
                    });
                } else {
                    swal("¡Ten cuidado!", "Primero debes elegir el aprendiz que deseas modificar.");
                }
            };

            //Función 2 para editar las areas
            $scope.GuardarModicarAprendiz = function () {
                if ($scope.Aprendiz.NumeroDocumento == null || $scope.Aprendiz.Nombres == null || $scope.Aprendiz.PrimerApellido == null || $scope.Aprendiz.SegundoApellido == null || $scope.Aprendiz.Correo == null || $scope.Aprendiz.Telefono == null || $scope.Aprendiz.IdFicha == null) {
                    swal("Información", "Debe diligenciar todos los campos", "error");
                } else {
                    AprendizService.GuardarModificacionAprendiz($scope.Aprendiz).then( function (response) {
                        if (response.data.success == true) {
                            AprendizService.ConsultarAprendiz().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                    swal("Información", "La modificación del registro se realizo con exito", "success");
                                    $("#ModalAprendizEditar").modal("hide");
                                    $scope.VaciarCampos();
                                }
                            });
                        }
                    });
                }
            }

            //Función 1 para el borrado de las areas
            $scope.CambiarEstadoSeleccionados = function () {
                var UsariosBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (UsariosBorrar.length == 0) {
                    swal("¡Ten cuidado!", "Primero debes elegir el aprendiz que deseas inhabilitar.", "error");
                } else {

                    $("#modalInhabilitar").modal("show");
                }
            }

            //Buscar en la tabla
            $scope.Filter = function (e) {
                var Busqueda = $("#Buscar").val().toLowerCase();
                var exp = new RegExp(Busqueda);
                if (Busqueda == "") {
                    AprendizService.ConsultarAprendiz().then(function (response) {
                        if (response.data.success == true) {
                            $scope.datalists = response.data.datos;
                            $scope.ListaCompleta = response.data.datos;
                        }
                    });
                }
                var Aprendiz = [];
                $scope.datalists = $scope.ListaCompleta;
                Aprendiz = $scope.datalists.filter(function (item) {


                    if (exp.test(item.TipoDocumento) || exp.test(item.TipoDocumento)) {

                        return item;
                    }

                    else if (exp.test(item.NumeroDocumento)) {
                        return item;
                    }

                    else if (exp.test(item.Nombres.toLowerCase()) || exp.test(item.Nombres.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Apellidos.toLowerCase()) || exp.test(item.Apellidos.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Correo.toLowerCase()) || exp.test(item.Correo.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Telefono)) {
                        return item;
                    }
                    else if (exp.test(item.IdentificadorFicha)) {
                        return item;
                    }
                });
            
            $scope.datalists = Aprendiz;
            //Variable para setear la paginación 
            $scope.curPage = 0;
            }
            //Función 2 para el borrado de las areas
            $scope.inhabilitar = function () {

                var AprendizBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                AprendizService.BorrarAprendiz(AprendizBorrar).then(function (response) {

                    if (response.data.success == true && response.data.respuesta == true) {
                        AprendizService.ConsultarAprendiz().then(function (response) {
                            if (response.data.success == true) {
                                $scope.datalists = response.data.datos;
                                $scope.ListaCompleta = response.data.datos;
                            }
                        })
                        if (response.data.prueba) {
                            swal("Inhabilitar", "Se inhabilito el registro correctamente", "warning")
                        } else {
                            swal("Inhabilitar", "Se inhabilitaron los " + response.data.contadorTrue + " registros correctamente", "warning")
                        }
                    } else {
                        swal("Inhabilitar", "No se puedieron inhabilitar la siguiente cantidad de registros " + response.data.contadorFalse + " porque ya cuentan con un proceso", "warning")
                    }

                });

            };
}]);