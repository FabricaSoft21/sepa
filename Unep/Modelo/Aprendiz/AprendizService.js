﻿ProgramacionApp.factory('AprendizService',
    ['$http', '$rootScope', '$routeParams',
    function ($http, $rootScope, $q, $routeParams) {
        var service = {};

        service.SubirArchivo = function (data, callback) {
            waitingDialog.show();
            console.log(data);
            var ajaxRequest = $.ajax({
                type: "POST",
                url: URLServices + "File/UploadFileAprendiz",
                contentType: false,
                processData: false,
                data: data,
                async: true,
            }).done(function (responseData, textStatus) {
                callback(responseData);
                waitingDialog.hide();
            }).fail(function () {
                waitingDialog.hide();
                location.reload();
            });
        };

        service.Email = function (Aprendiz) {
            return $http({
                method: 'POST',
                url: URLServices + "Aprendiz/Email/",
                data: Aprendiz
            })
        };

        service.GuardarAprendiz = function (Aprendiz) {
            return $http({
                method: 'POST',
                url: URLServices + "Aprendiz/GuardarAprendiz/",
                data: Aprendiz
            })
        };

        service.ModificarAprendiz = function (Aprendiz) {
            item = {
                Parametro1: Aprendiz[0].IdAprendiz
            };
            return $http({
                method: 'POST',
                url: URLServices + "Aprendiz/ModificarAprendiz/",
                data: item
            })
        };

        service.ActualizarFicha = function (Aprendiz) {
            return $http({
                method: 'POST',
                url: URLServices + "Aprendiz/CambiarFicha/",
                data: Aprendiz
            })
        };


        service.GuardarModificacionAprendiz = function (Aprendiz) {
            return $http({
                method: 'POST',
                url: URLServices + "Aprendiz/GuardarModificacionAprendiz/",
                data: Aprendiz
            })
        };

        service.ConsultarFichaxAprendiz = function (IdAprendiz, callback) {

            item = {
                Parametro1: IdAprendiz
            };
            return $http({
                method: 'POST',
                url: URLServices + "Aprendiz/ConsultarFichaxAprendiz/",
                data: item
            })
        };

        service.ConsultarFicha = function () {
          return $http({
                method: 'GET',
                url: URLServices + "Aprendiz/ConsultarFichas/"
            })
        };

        service.ConsultarAprendiz = function () {
            return $http({
                method: 'GET',
                url: URLServices + "Aprendiz/ConsultarAprendiz/"
            })
        };

        service.BorrarAprendiz = function (Aprendiz) {
            var Item = {
                Parametros: []
            }
            $.each(Aprendiz, function (index, value) {
                Item.Parametros.push(
                    {
                        Parametro1: value.IdAprendiz
                    })
            });
            return $http({
                method: 'POST',
                url: URLServices + "Aprendiz/EliminarAprendiz/",
                data: Item
            })
        };

        return service;
    }])