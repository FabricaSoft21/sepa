﻿ProgramacionApp.factory('EmpresaService',
    ['$http', '$rootScope', '$routeParams',
    function ($http, $rootScope, $routeParams) {
        var service = {};

        service.SubirArchivo = function (data, callback) {
            waitingDialog.show();
            var ajaxRequest = $.ajax({
                type: "POST",
                url: URLServices + "File/UploadFileEmpresa",
                contentType: false,
                processData: false,
                data: data
            }).done(function (responseData, textStatus) {
                callback(responseData);
                waitingDialog.hide();
            }).fail(function () {
                waitingDialog.hide();
            });
        };


        //Metodo para guardar empresa



        service.GuardarEmpresa = function (empresa) {
         
            return $http({
                method: 'POST',
                url: URLServices + "Empresas/GuardarEmpresa/",
                data: empresa

            })
        };

        //Metodo para listas las empresas

        service.ConsultarEmpresa = function () {
            waitingDialog.show();
            return $http({
                method: 'GET',
                url: URLServices + "Empresas/ConsultarEmpresa/"
            });
         
            
        };

        service.ConsultarEmpresafiltro = function () {
            return $http({
                method: 'GET',
                url: URLServices + "Empresas/ConsultarEmpresa/"
            });

        };

        service.ConsultarSedefiltro = function () {
            return $http({
                method: 'GET',
                url: URLServices + "Sede/ConsultarSede/"
            });

        };
        

        //metodo para modificar una empresa
        service.Modificar = function (Empresa) {
            item = {
                IdEmpresa: Empresa[0].IdEmpresa
            };
            return $http({
                method: 'POST',
                url: URLServices + "Empresas/ModificarEmpresa/", 
                data :item
            })
        };

        //Metodo para guardar la modificacion de las empresas
        service.GuardarModificacionEmpresa = function (Empresa) {
           
            return $http({
                method: 'POST',
                url: URLServices + "Empresas/GuardarModificacionEmpresa/",
                data: Empresa
            })
        };

        //Metodo para cabiar el estado a una empresa
        service.BorrarEmpresa = function (Empresas) {
            var Item = {
                Parametros: []
            };

            $.each(Empresas, function (index, value) {
                Item.Parametros.push(
                    {
                        parametro2: value.IdEmpresa
                    })
            });

            return $http({
                method: 'POST',
                url: URLServices + "Empresas/EliminarEmpresa/",
                data: Item
            })
        };

        service.ConsultarEmpresaSede = function () {
            return $http({
                method: 'GET',
                url: URLServices + "Empresas/ConsultarEmpresa/"
            })
        };
     

        service.ConsultarSede = function (sede) {
            var Item = {
                Parametro1: sede[0].IdEmpresa
            };
            
            waitingDialog.show();
            return $http({
                method: 'POST',
                url: URLServices + "Sede/ConsultarSede/",
                data:Item
            })
        };
      
        service.ConsultarSedeinactiva = function (sede) {
            var Item = {
                Parametro1: sede[0].IdEmpresa
            };
            waitingDialog.show();
            return $http({
                method: 'POST',
                url: URLServices + "Sede/ConsultarSedeinactiva/",
                data: Item
            })
        }

        
        

        service.ConsultarEmpresainactiva = function () {
            waitingDialog.show();
            return $http({
                method: 'GET',
                url: URLServices + "Empresas/ConsultarEmpresainactiva/"
            })
        };

        service.GuardarModificacionSede = function (sede) {
            var Item = {
                Parametro1: sede.IdSede,
                Parametro2: sede.Codigo,
                Parametro3: sede.Nombre,
                Parametro4: sede.Telefono,
                Parametro5: sede.Direccion,
                Parametro6: sede.NombreCoformador,
                Parametro7: sede.CargoCoformador,
                Parametro8: sede.TelefonoCoformador,
                Parametro9: sede.CorreoCoformador,
                Parametro10: sede.IdEmpresa,
                Parametro11: sede.Estado
            };

            return $http({
                method: 'POST',
                url: URLServices + "Sede/GuardarModificacionSede/",
                data: Item
            })
        };
       
        service.inhabilitarSede = function (sedes) {
            var Item = {
                Parametros: []
            };

            $.each(sedes, function (index, value) {
                Item.Parametros.push(
                    {
                        Parametro9: value.IdSede
                    })
            });

            return $http({
                method: 'POST',
                url: URLServices + "Sede/inhabilitar/",
                data: Item
            })
        };

        service.ModificarSede = function (sede) {
            item = {
                Parametro9: sede[0].IdSede
            };
            return $http({
                method: 'POST',
                url: URLServices + "Sede/SedeId/",
                data: item
            })
        };

        service.RegistrarSede = function (sede) {
            item = {
                Parametro1: sede.CargoCoformador,
                parametro2: sede.Codigo,
                Parametro3: sede.CorreoCoformador,
                parametro4: sede.Direccion,
                Parametro5: sede.Estado,
                parametro6: sede.IdEmpresa,
                Parametro7: sede.IdSede,
                parametro8: sede.Nombre,
                Parametro9: sede.NombreCoformador,
                parametro10: sede.Telefono,
                Parametro11: sede.TelefonoCoformador
            }
            return $http({
                method: 'POST',
                url: URLServices + "Sede/RegistrarSede/",
                data: item
            })
        };
       
        return service;
    }])
