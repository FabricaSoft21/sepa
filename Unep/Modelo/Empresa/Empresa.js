﻿/// <reference path="../Area/AreaService.js" />
ProgramacionApp.controller('EmpresasController',
    ['$scope', '$rootScope', '$location', 'EmpresaService', '$routeParams', '$sce',
        function ($scope, $rootScope, $location, EmpresaService, $routeParams, $sce) {

            $scope.SubirArchivo = function () {
                EmpresaService.alerta();
            }

            $scope.AbrirModal = function () {
                $("#ModalEmpresas").modal("show");
                $scope.VaciarCampos();
            };


            $scope.VaciarCampoSede = function () {

                $scope.sede.IdSede = "";
                $scope.sede.Codigo = "";
                $scope.sede.Nombre = "";
                $scope.sede.Telefono = "";
                $scope.sede.Direccion = "";
                $scope.sede.NombreCoformador = "";
                $scope.sede.CargoCoformador = "";
                $scope.sede.CorreoCoformador = "";
                $scope.sede.TelefonoCoformador = "";
                var sd = $('#listas');
                setTimeout(function () {
                    sd.val("").trigger("change");

                }, 100);
            };


            $scope.AbrirModalSede = function () {
                $('#codigo').val($scope.global[0].NitEmpresa);
                $('#empresasede').val($scope.global[0].NombreEmpresa + " / " + $scope.global[0].NitEmpresa)
              
                $("#ModalSede").modal("show");
         
            };
            var nav = $("#navbar").hasClass("gn-menu-wrapper gn-open-all");
            if (nav == true) {
                $(".Principal").css("width", "80%");

            } else {
                $(".Principal").css("width", "95%");
            }
            $("#atras").hide();

            $scope.empresa = {
                Id_Empresa: "",
                NitEmpresa: "",
                NombreEmpresa: "",
                Correo: "",
                Telefono: "",
                Direccion: "",
                NombreCoformador: "",
                CargoCoformador: "",
                CorreoCoformador: "",
                TelefonoCoformador: "",
            }




                //Funcion para cargar los datos al modal de editar
                $scope.ModificarSede = function () {
                    var SedeModificar = $scope.datalists.filter(function (item) {
                        return item.Seleccionado === true;
                    });
                    if (SedeModificar.length == 1) {
                        EmpresaService.ModificarSede(SedeModificar).then(function (response) {
                            if (response.data.succes == true) {
                                $scope.sede.Codigo = response.data.sed.Codigo;

                                var sf = $('#listasE');
                                setTimeout(function () {
                                    sf.val(response.data.sed.IdEmpresa).trigger("change");
                                }, 100);

                                $scope.sede.Nombre = response.data.sed.Nombre;
                                $scope.sede.Telefono = parseInt(response.data.sed.Telefono);
                                $scope.sede.Direccion = response.data.sed.Direccion;
                                $scope.sede.NombreCoformador = response.data.sed.NombreCoformador;
                                $scope.sede.CargoCoformador = response.data.sed.CargoCoformador;
                                $scope.sede.CorreoCoformador = response.data.sed.CorreoCoformador;
                                $scope.sede.CorreoCoformador = response.data.sed.CorreoCoformador;
                                $scope.sede.TelefonoCoformador = parseInt(response.data.sed.TelefonoCoformador);
                                $scope.sede.Estado = response.data.sed.Estado;
                                $("#estado").attr("disabled", true);

                                $("#ModalEditarSede").modal("show");
                            }
                        });
                    } else {
                        swal("¡Ten cuidado!", "Primero debes elegir la sede que deseas modificar.");
                    }
                }

                //fincion para cambia el estado de una empresa
                $scope.CambiarEstadoSeleccionadoSede = function () {
                    var SedeBorrar = $scope.datalists.filter(function (item) {
                        return item.Seleccionado === true;
                    });

                    if (SedeBorrar.length == 0) {
                        swal("¡Ten cuidado!", "Primero debes elegir la Sede a la que deseas Cambiar el estado.");
                    } else {
                        $("#modalInhabilitarSede").modal("show");
                    }
                };




            

                //Función 2 para editar las areas
                $scope.GuardarEdicionSede = function () {
                    if ($scope.sede.IdEmpresa == "" || $scope.sede.Codigo == "" || $scope.sede.Telefono == "" || $scope.sede.Direccion == "" || $scope.sede.NombreCoformador == "" || $scope.sede.TelefonoCoformador == "" || $scope.sede.CorreoCoformador == "" || $scope.sede.CargoCoformador == "" || $scope.sede.Nombre == "") {
                        swal({
                            title: "Error!",
                            text: "Debe diligenciar todos los campos!",
                            type: "error",
                            showCancelButton: false,
                            showConfirmButton: false,
                            timer: 2000
                        });
                    } else {
                        EmpresaService.GuardarModificacionSede($scope.sede).then(function (response) {
                            if (response.data.success == true) {
                                swal({
                                    title: "Bien Hecho!",
                                    text: "Actualizacion realizada con exito!",
                                    type: "success",

                                });
                                $("#ModalEditarSede").modal("hide");
                           
                                EmpresaService.ConsultarSede($scope.global).then(function (response) {
                                    if (response.data.success == true) {
                                        $scope.datalists = response.data.datos;
                                        $scope.ListaCompleta = response.data.datos;

                                    }
                                    waitingDialog.hide();
                                });
                            }
                      
                        });
                    }
                }



                $scope.UploadFileWeb = function () {
                    $("#fileUploadWeb").trigger('click');
                };
                $("#fileUploadWeb").change(function () {
                    dataweb = new FormData();
                    var files = $("#fileUploadWeb").get(0).files;
                    var fileExtension = ['xlsx'];
                    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                        swal("Importar Archivo", "La extencion del archivo no es valida", "error");
                        $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                        return false;
                    }
                    // Add the uploaded image content to the form data collection
                    if (files.length > 0) {
                        readURL(this, "logoweb");
                        dataweb.append("UploadedImage", files[0]);
                        if (dataweb != null) {
                            EmpresaService.SubirArchivo(dataweb, function (response) {
                                if (response.success) {
                                    console.log(response.datos)
                                    if (response.datos.length == 0) {
                                        swal("Importar Archivo", "La importación del archivo se realizó con éxito", "success");
                                    } else {
                                        $scope.Error = response.datos;
                                        $('#ModalErrores').modal("show");
                                    }
                                    $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                                    EmpresaService.ConsultarEmpresa().then(function (response) {
                                        if (response.data.success == true) {
                                            $scope.datalists = response.data.datos;
                                            $scope.ListaCompleta = response.data.datos;
                                            $scope.numberOfPages = function () {
                                                return Math.ceil($scope.datalists.length / $scope.pageSize);
                                            };
                                        }
                                    });
                                    $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                                    return;
                                }
                            });
                        }
                    }
                });

                function readURL(input, control) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#' + control + '').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                };


                // pagination

                $scope.curPage = 0;
                $scope.pageSize = 10;

                //metodo para consultar una empresa
                EmpresaService.ConsultarEmpresa().then(function (response) {
                    if (response.data.success == true) {
                        $scope.datalists = response.data.datos;
                        $scope.numberOfPages = function () {
                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                        };
                        $scope.Datos = $scope.datalists;
                    }
                    waitingDialog.hide();
                });


                //metodo para registar una empresa
                $scope.Guardar = function () {
                    if ($scope.empresa.NitEmpresa == "" || $scope.empresa.NombreEmpresa == "" || $scope.empresa.Direccion == "" || $scope.empresa.Correo == "" || $scope.empresa.Telefono == "") {
                        swal({
                            title: "Error!",
                            text: "Debe diligenciar todos los campos!",
                            type: "error",

                        });
                    } else {
                        EmpresaService.GuardarEmpresa($scope.empresa).then(function (response) {
                            if (response.data.success == true) {
                                EmpresaService.ConsultarEmpresa().then(function (response) {
                                    if (response.data.success == true) {
                                        $scope.datalists = response.data.datos;
                                        $scope.ListaCompleta = response.data.datos;
                                        swal({
                                            title: "Bien Hecho!",
                                            text: "El registro se realizo con exito!",
                                            type: "success",

                                        }, function () { location.reload() });
                                    }
                                    $("#ModalEmpresas").modal("hide");
                                    $scope.VaciarCampos();
                                    waitingDialog.hide();
                                });
                            } else {
                                swal({
                                    title: "Error!",
                                    text: "La empresa ya se encuentra registrada!",
                                    type: "Error",

                                });
                            }
                        });
                    }
                };

                //metodo para vaciar campos
                $scope.VaciarCampos = function () {
                    $scope.empresa.Id_Empresa = "";
                    $scope.empresa.NitEmpresa = "";
                    $scope.empresa.NombreEmpresa = "";
                    $scope.empresa.Correo = "";
                    $scope.empresa.Telefono = "";
                    $scope.empresa.Direccion = "";
                };


                //Función para filtrar la Empresa 
                $scope.Filter = function (e) {
                    var Busqueda = $("#Buscar").val().toLowerCase();
                    var exp = new RegExp(Busqueda);
                    var ver = $("#sedes1").hasClass("glyphicon glyphicon-eye-open");
                    if (ver) {
                        if (Busqueda == "") {
                            EmpresaService.ConsultarEmpresafiltro().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                }

                            });
                        }
                        var Area = [];
                        $scope.datalists = $scope.ListaCompleta;
                        Area = $scope.datalists.filter(function (item) {
                            if (exp.test(item.NitEmpresa) || exp.test(item.NitEmpresa)) {

                                return item;
                            }
                            else if (exp.test(item.NombreEmpresa.toLowerCase()) || exp.test(item.NombreEmpresa.toUpperCase())) {
                                return item;
                            }
                        });


                    } else {
                        if (Busqueda == "") {
                            EmpresaService.ConsultarSedefiltro().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                }

                            });
                        }

                        var Area = [];
                        $scope.datalists = $scope.ListaCompleta;
                        Area = $scope.datalists.filter(function (item) {
                            if (exp.test(item.Codigo) || exp.test(item.Codigo)) {

                                return item;
                            }
                            else if (exp.test(item.Nombre.toLowerCase()) || exp.test(item.Nombre.toUpperCase())) {
                                return item;
                            }
                        });
                    }
                    $scope.datalists = Area;
                    //Variable para setear la paginación 
                    $scope.curPage = 0;
                };

                //Funcion para editar una empresa
                $scope.Modificar = function () {
                    var EmpresaModificar = $scope.datalists.filter(function (item) {
                        return item.Seleccionado === true;
                    });
                    if (EmpresaModificar.length == 1) {
                        EmpresaService.Modificar(EmpresaModificar).then(function (response) {
                            if (response.data.success == true) {
                                $scope.empresa.NitEmpresa = response.data.empres.NitEmpresa;
                                $scope.empresa.NombreEmpresa = response.data.empres.NombreEmpresa;
                                $scope.empresa.Direccion = response.data.empres.Direccion;
                                $scope.empresa.Correo = response.data.empres.Correo;
                                $scope.empresa.Telefono = parseInt(response.data.empres.Telefono);
                                $("#ModalEditar").modal("show");
                            }
                            waitingDialog.hide();
                        });
                    } else {
                        swal("¡Ten cuidado!", "Primero debes elegir la empresa que deseas modificar.");
                    }
                }



            //fincion para cambia el estado de una empresa
                $scope.CambiarEstadoSeleccionados = function () {
                    var EmpresaBorrar = $scope.datalists.filter(function (item) {
                        return item.Seleccionado === true;
                    });

                    if (EmpresaBorrar.length == 0) {
                        swal("¡Ten cuidado!", "Primero debes elegir la empresa que deseas inhabilitar.");
                    } else {
                        $("#modalInhabilitar").modal("show");
                    }
                };

                //Función para inhabilitar la coordinacion 
                $scope.inhabilitar = function () {
                    var EmpresaBorrar = $scope.datalists.filter(function (item) {
                        return item.Seleccionado === true;
                    });
                    EmpresaService.BorrarEmpresa(EmpresaBorrar).then(function (response) {
                        if (response.data.success == true) {

                            swal({
                                title: "Bien Hecho!",
                                text: "Se ha modificado el estado!",
                                type: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                timer: 1000
                            }, function () { location.reload() });
                        }
                    });
                };




                $scope.global = "";


                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //SEDE

                $scope.sede = {
                    IdSede: "",
                    Codigo: "",
                    Nombre: "",
                    Telefono: "",
                    Direccion: "",
                    NombreCoformador: "",
                    CargoCoformador: "",
                    CorreoCoformador: "",
                    TelefonoCoformador: "",
                    IdEmpresa: "",
                    Estado: ""
                }


               
                // Funcion Para guardar Sedes
                $scope.GuardarSede = function () {

                    if ($scope.sede.Estado == null ||  $scope.sede.Telefono == null || $scope.sede.Direccion == null || $scope.sede.NombreCoformador == null || $scope.sede.TelefonoCoformador == null || $scope.sede.CorreoCoformador == null || $scope.sede.CargoCoformador == null || $scope.sede.Nombre == null) {
                        swal({
                            title: "Error!",
                            text: "Debe diligenciar todos los campos",
                            type: "error",

                        });
                    } else {
                        $scope.sede.Codigo = $scope.global[0].NitEmpresa;
                        $scope.sede.IdEmpresa = $scope.global[0].IdEmpresa;
                        EmpresaService.RegistrarSede($scope.sede).then(function (response) {
                            if (response.data.success == true) {
                                EmpresaService.ConsultarSede($scope.global).then(function (response) {
                                    if (response.data.success == true) {
                                        $scope.datalists = response.data.datos;
                                        $scope.ListaCompleta = response.data.datos;
                                        swal({
                                            title: "Bien Hecho!",
                                            text: "Registro realizado exitosamente!",
                                            type: "success"

                                        });
                                    }
                                    $("#ModalSede").modal("hide");
                              
                                    waitingDialog.hide();
                                });
                            } else {
                                swal({
                                    title: "Error!",
                                    text: "La empresa ya se encuentra registrada!",
                                    type: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    timer: 2000
                                });
                            }
                        });
                    }
                };


                //Función para Consultar las Empresa 
                EmpresaService.ConsultarEmpresaSede().then(function (response) {
                    if (response.data.success == true) {
                        $scope.Empresa = response.data.datos;
                    }
                });


                //Clic para visualizar las sedes
                $("#sedes").click(function () {

                    var SedeModificar = $scope.datalists.filter(function (item) {
                        return item.Seleccionado === true;

                    });
                    $scope.global = SedeModificar;
                   
                    var clase = $("#sedes1").hasClass("glyphicon glyphicon-eye-open");
                    if (clase) {
                        if (SedeModificar.length == 1) {
                            $("#reset").hide();
                            $("#excel").hide();
                            $("#temp").hide();
                            $("#tsede").show();
                            EmpresaService.ConsultarSede(SedeModificar).then(function (response) {
                                if (response.data.success == true) {
                                    $scope.curPage = 0;
                                    $scope.pageSize = 8;
                                    $scope.datalists = response.data.datos;

                                    $scope.numberOfPages = function () {
                                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                                    };

                                    $scope.Datos = $scope.datalists;
                                }
                                waitingDialog.hide();
                            });
                    
                            $("#title1").show();
                            $("#title").hide();
                            $("#boton").hide();
                            $("#eliminar1").hide();
                            $("#eliminarsede").show();
                            $("#modificarsede").show();
                            $("#modificarem").hide();
                            $("#inactivo2").show();
                            $("#inactivo").show();
                            $("#boton1").show();
                            $("#sedes1").removeClass("glyphicon glyphicon-eye-open");
                            $("#sedes1").addClass("glyphicon glyphicon-arrow-left");

                            $("#reset").hide();
                            $("#excel").hide();
                        } else {
                            swal("¡Ten cuidado!", "Primero debes elegir la sede que deseas Consultar.");
                        }



                    } else {
                        //Aqui se devuelve a las empresas
                        $('#empresasede').val('')
                        $("#title1").hide();
                        $("#title").show();
                        $("#modificarem").show();
                        $("#modificarsede").hide();
                        $("#eliminarsede").hide();
                        $("#eliminar1").show();
                        $("#tsede").hide();
                        $("#temp").show();
                        $("#sedes1").removeClass("glyphicon glyphicon-arrow-left");
                        $("#sedes1").addClass("glyphicon glyphicon-eye-open");
                        $("#inactivo2").hide();
                        $("#boton").show();
                        $("#boton1").hide();
                        EmpresaService.ConsultarEmpresa().then(function (response) {
                            if (response.data.success == true) {
                                $scope.curPage = 0;
                                $scope.pageSize = 8;
                                $scope.datalists = response.data.datos;
                                $scope.numberOfPages = function () {
                                    return Math.ceil($scope.datalists.length / $scope.pageSize);
                                };

                                $scope.Datos = $scope.datalists;
                            }
                            waitingDialog.hide();
                        });
                        $("#excel").show();
                    }

                });



                $(document).ready(function () {
                    $("#inactivo2").hide();
                    $("#boton1").hide();
                    $("#tsede").hide();
                    $("#modificarsede").hide();
                    $("#eliminarsede").hide();
                    $("#title1").hide();
                    $("#reset").hide();

                    $("#eliminar").css("color", "red");

                });



                $("#reset").click(function () {

                    var clas1 = $("#sedes1").hasClass("glyphicon glyphicon-eye-open");
                    $("#eliminar2").removeClass("glyphicon glyphicon-ok-circle");
                    $("#eliminar2").addClass("glyphicon glyphicon-trash");
                    $("#eliminar2").css("color", "red");
                    if (clas1) {
                        $("#reset").hide();
                        $('#inactivo').show();
                        EmpresaService.ConsultarEmpresa().then(function (response) {
                            if (response.data.success == true) {
                                $scope.curPage = 0;
                                $scope.pageSize = 8;
                                $scope.datalists = response.data.datos;
                                $scope.numberOfPages = function () {
                                    return Math.ceil($scope.datalists.length / $scope.pageSize);
                                };

                                $scope.Datos = $scope.datalists;
                            }
                            waitingDialog.hide();
                        });


                    } else {
                        $("#eliminarsede1").removeClass("glyphicon glyphicon-ok-circle");
                        $("#eliminarsede1").addClass("glyphicon glyphicon-trash");
                        $("#eliminarsede1").css("color", "red");
                        $("#inactivo").hide();
                        $("#reset").hide();
                        $("#sedes").show();
                        $('#inactivo').show();
                        EmpresaService.ConsultarSede($scope.global).then(function (response) {
                            if (response.data.success == true) {
                                $scope.curPage = 0;
                                $scope.pageSize = 8;
                                $scope.datalists = response.data.datos;
                                $scope.numberOfPages = function () {
                                    return Math.ceil($scope.datalists.length / $scope.pageSize);
                                };

                                $scope.Datos = $scope.datalists;
                            }
                            waitingDialog.hide();
                        });
                    }
                });

               

                //Clic para consultar las empresas o las sedes inactivas
                $("#inactivo").click(function () {
                    $("#reset").show();
                    var clas = $("#sedes1").hasClass("glyphicon glyphicon-eye-open");
                    if (clas) {
                        $("#eliminar2").removeClass("glyphicon glyphicon-trash");
                        $("#eliminar2").addClass("glyphicon glyphicon-ok-circle");
                        $("#eliminar2").css("color", "green");
                        $("#inactivo").hide();
                        EmpresaService.ConsultarEmpresainactiva().then(function (response) {
                            if (response.data.success == true) {
                                $scope.curPage = 0;
                                $scope.pageSize = 8;
                                $scope.datalists = response.data.datos;
                                $scope.numberOfPages = function () {
                                    return Math.ceil($scope.datalists.length / $scope.pageSize);
                                };

                                $scope.Datos = $scope.datalists;
                            }
                            waitingDialog.hide();
                        });
                    } else {

                        EmpresaService.ConsultarSedeinactiva($scope.global).then(function (response) {


                            if (response.data.success == true) {
                                $scope.curPage = 0;
                                $scope.pageSize = 8;
                                $scope.datalists = response.data.datos;
                                $scope.numberOfPages = function () {
                                    return Math.ceil($scope.datalists.length / $scope.pageSize);
                                };

                                $scope.Datos = $scope.datalists;
                            }
                            waitingDialog.hide();
                        });

                        $("#sedes").hide();
                        $("#inactivo").hide();
                        $("#eliminarsede1").removeClass("glyphicon glyphicon-trash");
                        $("#eliminarsede1").addClass("glyphicon glyphicon-ok-circle");
                        $("#eliminarsede1").css("color", "green");
                    }
               
                });
           

                $scope.inhabilitarSede = function () {
                    var SedeBorrar = $scope.datalists.filter(function (item) {
                        return item.Seleccionado === true;
                    });
                    EmpresaService.inhabilitarSede(SedeBorrar).then(function (response) {
                        if (response.data.success == true) {
                            if (response.data.esta == false) {
                                EmpresaService.ConsultarSede($scope.global).then(function (response) {
                                    if (response.data.success == true) {
                                        swal({
                                            title: "Bien Hecho!",
                                            text: "Se ha modificado el estado!",
                                            type: "success",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            timer: 1000
                                        });
                                        $scope.datalists = response.data.datos;
                                        $scope.ListaCompleta = response.data.datos;
                                    }
                                    waitingDialog.hide();
                                });

                            } else {
                                EmpresaService.ConsultarSedeinactiva($scope.global).then(function (response) {
                                    if (response.data.success == true) {
                                        swal({
                                            title: "Bien Hecho!",
                                            text: "Se ha modificado el estado!",
                                            type: "success",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            timer: 1000
                                        });
                                        $scope.datalists = response.data.datos;
                                        $scope.ListaCompleta = response.data.datos;
                                    }
                                    waitingDialog.hide();
                                });
                            }

                        }

                    });

                }

                $scope.curPage = 0;
                $scope.pageSize = 10;

            }]);


