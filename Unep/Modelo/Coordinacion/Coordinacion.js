﻿ProgramacionApp.controller('CoordinacionController',
    ['$scope', '$rootScope', '$location', 'CoordinacionService', '$routeParams', '$sce',
        function ($scope, $rootScope, $location, CoordinacionService, $routeParams, $sce) {

            var nav = $("#navbar").hasClass("gn-menu-wrapper gn-open-all");
            if (nav == true) {
                $(".Principal").css("width", "80%");

            } else {
                $(".Principal").css("width", "95%");
            }

            $scope.Coordinacion = {
                IdCoordinacion: "",
                NumeroDocumento: "",
                TipoDocumento: "",
                Nombres: "",
                Apellidos: "",
                Telefono: "",
                Correo: ""
            };

            $scope.Programa = {
                IdPrograma: "",
                CodigoPrograma: "",
                Nivel: "",
                IdArea: "",
                NombrePrograma: ""
            };

            $scope.Centro = {
                IdCentro: "",
                NombreCentro: "",
                IdRegional: "",
                Direccion: ""
            };

            $scope.Area = {
                IdArea: "",
                Nombre: "",
                Descripcion: "",
                Codigo: ""
            };

            //Función para abrir modal de la coordinacion 
            $scope.AbrirModal = function () {
                $("#ModalCoordinacion").modal("show");
                $scope.VaciarCampos();
            };

            //Función para vaciar los campos 
            $scope.VaciarCampos = function () {
                $scope.Coordinacion.IdArea = "";
                $scope.Coordinacion.IdCentro = "";
                $scope.Coordinacion.Cedula = "";
                $scope.Coordinacion.Nombre = "";
                $scope.Coordinacion.Apellido = "";
                $scope.Coordinacion.Telefono = "";
                $scope.Coordinacion.Correo = "";
                $scope.Coordinacion.TipoDocumento = "";
            };



            $scope.ConsultarProgramaxArea = function (IdArea) {
                CoordinacionService.ConsultarProgramaxArea($scope.Area.IdArea).then(function (response) {

                    if (response.data.success == true) {
                        $scope.Programa = response.data.programa;
                    }
                });

            }

            //Función para consultar las areas 
            CoordinacionService.ConsultarAreasss().then(function (response) {
                if (response.data.success == true) {
                    $scope.Area = response.data.datos;
                }
            });

            //Función para consultar los Centros 
            CoordinacionService.ConsultarCentrosss().then(function (response) {
                if (response.data.success == true) {
                    $scope.Centro = response.data.datos;
                }
            });

            //Función para Guardar la coordinacion 
            $scope.Guardar = function () {

                $.each($scope.Area, function (index, value) {
                    if (value.IdArea == $scope.Area.IdArea) {
                        $scope.Coordinacion.IdArea = value.IdArea
                    }
                });
                $.each($scope.Centro, function (index, value) {
                    if (value.IdCentro == $scope.Centro.IdCentro) {
                        $scope.Coordinacion.IdCentro = value.IdCentro;
                    }
                });
                $.each($scope.Programa, function (index, value) {
                    if (value.IdPrograma == $scope.Programa.IdPrograma) {
                        $scope.Coordinacion.IdPrograma = value.IdPrograma;
                    }
                });

                if ($scope.Coordinacion.NumeroDocumento == "" || $scope.Coordinacion.Nombres == "" || $scope.Coordinacion.TipoDocumento == "" ||
                    $scope.Coordinacion.Apellidos == "" || $scope.Coordinacion.Telefono == "" || $scope.Coordinacion.Correo == "" || $scope.Coordinacion.IdCentro == "" ||
                    $scope.Coordinacion.IdArea == "") {
                 swal("Información", "Debe diligenciar todos los campos", "error");
                } else {
                    CoordinacionService.GuardarCoordinacion($scope.Coordinacion).then(function (response) {
                        if (response.data.success == true) {

                            CoordinacionService.ConsultarCoordinacionnn().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                }
                            });
                            swal("Información", "El registro se realizó con éxito", "success");
                        }

                        $("#ModalCoordinacion").modal("hide");
                        $scope.VaciarCampos();

                    }
                    );
                }
            };

            //Función para consultar la coordinacion 
            CoordinacionService.ConsultarCoordinacionnn().then(function (response) {
                if (response.data.success == true) {
                    $scope.curPage = 0;
                    $scope.pageSize = 10;
                    $scope.datalists = response.data.datos;

                    $scope.numberOfPages = function () {
                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                    };

                    $scope.Datos = $scope.datalists;
                }
            });

            //Función para Cambiar estado de la coordinacion 
            $scope.CambiarEstadoSeleccionados = function () {
                var UsariosBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (UsariosBorrar.length == 0) {

                    swal("¡Ten cuidado!", "Primero debes elegir la coordinacion que deseas inhabilitar.");

                } else {
                    $("#modalInhabilitar").modal("show");
                }
            };

            //Función para inhabilitar la coordinacion 
            $scope.inhabilitar = function () {

                var CoordinacionesBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                CoordinacionService.BorrarCoordinacionessss(CoordinacionesBorrar).then(function (response) {

                    if (response.data.success == true) {
                        CoordinacionService.ConsultarCoordinacionnn().then(function (response) {
                            if (response.data.success == true) {
                                $scope.datalists = response.data.datos;
                                $scope.ListaCompleta = response.data.datos;
                                swal("Inhabilitar", "Se inhabilitaron los registros ", "warning")
                            } 
                        });
                    }
                    else {
                        swal("Inhabilitar", "No se puedieron inhabilitar los registros porque estan asociados a otras tablas.", "warning")
                    }
                });
            };

            //Función para Modificar la coordinacion 
            $scope.Modificar = function () {
                var ModificarC = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (ModificarC.length == 1) {

                    CoordinacionService.ModificarCoor(ModificarC).then(function (response) {

                        if (response.data.success == true) {

                            $scope.Coordinacion.IdCoordinacion = response.data.Coordinacion.IdCoordinacion;
                            $scope.Coordinacion.NumeroDocumento = parseInt(response.data.Coordinacion.NumeroDocumento);
                            $scope.Coordinacion.TipoDocumento = response.data.Coordinacion.TipoDocumento;
                            $scope.Coordinacion.Nombres = response.data.Coordinacion.Nombres;
                            $scope.Coordinacion.Apellidos = response.data.Coordinacion.Apellidos;
                            $scope.Coordinacion.Telefono = parseInt(response.data.Coordinacion.Telefono);
                            $scope.Coordinacion.Correo = response.data.Coordinacion.Correo

                            CoordinacionService.ConsultarIds(ModificarC).then(function (response) {

                                if (response.data.success == true) {

                                    console.log(response.data);

                                    $scope.Coordinacion.IdCentro = response.data.Coordinacion.IdCentro;
                                    $scope.Programa.IdPrograma = response.data.Coordinacion.IdPrograma;

                                    $scope.Area.IdArea = response.data.Coordinacion.IdArea;
                                    var sd = $('#AreaLista');
                                    setTimeout(function () {
                                        sd.val($scope.Area.IdArea).trigger("change");
                                    }, 100);
                                    setTimeout(function () {
                                        var sp = $('#programaLista');
                                        setTimeout(function () {
                                            sp.val($scope.Programa.IdPrograma).trigger("change");
                                        }, 100);
                                    }, 200);
                                  

                                    var sf = $('#CentroLista');
                                    setTimeout(function () {
                                        sf.val(response.data.Coordinacion.IdCentro).trigger("change");
                                    }, 100);
                                    $("#ModalEditar").modal("show");
                                }
                            });
                        }
                    });
                } else {
                    swal("¡Ten cuidado!", "Primero debes elegir la coordinación que deseas modificar.");
                }
            }

            //Función para Guardar la modificación de la coordinacion 
            $scope.GuardarEdicionCoordinacion = function () {
                $.each($scope.Area, function (index, value) {

                    if (value.IdArea == $scope.Area.IdArea) {

                        $scope.Coordinacion.IdArea = value.IdArea
                    }
                });
                $.each($scope.Centro, function (index, value) {

                    if (value.IdCentro == $scope.Centro.IdCentro) {

                        $scope.Coordinacion.IdCentro = value.IdCentro;
                    }
                });

                $.each($scope.Programa, function (index, value) {
                    if (value.IdPrograma == $scope.Programa.IdPrograma) {
                        $scope.Coordinacion.IdPrograma = value.IdPrograma;
                    }
                });

                if ($scope.Coordinacion.NumeroDocumento == "" || $scope.Coordinacion.Nombres == "" || $scope.Coordinacion.TipoDocumento == "" ||
                    $scope.Coordinacion.Apellidos == "" || $scope.Coordinacion.Telefono == "" || $scope.Coordinacion.Correo == "" || $scope.Coordinacion.IdCentro == "" ||
                    $scope.Coordinacion.IdArea == "" || $scope.Coordinacion.IdPrograma == "") {
                    swal("¡Ten cuidado!", "Primero debes diligenciar todos los campos.");
                } else {
                    CoordinacionService.GuardarModificacionCoordinacion($scope.Coordinacion).then(function (response) {
                        if (response.data.success == true) {
                            swal("Modificación exitosa!", "La coordinación fue modificada exitosamente", "success")
                            $scope.VaciarCampos();
                            $("#ModalEditar").modal("hide");
                            CoordinacionService.ConsultarCoordinacionnn().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                }
                            });
                        }
                    });
                }
            };

            //Función para filtrar la coordinacion 
            $scope.Filter = function (e) {

                var Busqueda = $("#Buscar").val();
                var exp = new RegExp(Busqueda);
                if (Busqueda == "") {
                    CoordinacionService.ConsultarCoordinacionnn().then(function (response) {
                        if (response.data.success == true) {
                            $scope.datalists = response.data.datos;
                            $scope.ListaCompleta = response.data.datos;
                        }
                    });
                }
                var Coordinacion = [];
                $scope.datalists = $scope.ListaCompleta;
                Coordinacion = $scope.datalists.filter(function (item) {

                    if (exp.test(item.NumeroDocumento.toLowerCase()) || exp.test(item.NumeroDocumento.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Nombres.toLowerCase()) || exp.test(item.Nombres.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Apellidos.toLowerCase()) || exp.test(item.Apellidos.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Correo.toLowerCase()) || exp.test(item.Correo.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Telefono.toLowerCase()) || exp.test(item.Telefono.toUpperCase())) {
                        return item;
                    }

                });
                $scope.datalists = Coordinacion;
                //Variable para setear la paginación 
                $scope.curPage = 0;
            };
        }]);