﻿ProgramacionApp.factory('CoordinacionService',
    ['$http', '$rootScope', '$routeParams',
    function ($http, $rootScope, $routeParams) {
        var service = {};

        service.GuardarCoordinacion = function (Coordinacion1, callback) {
            return $http({
                method: 'POST',
                url: URLServices + "Coordinacion/GuardarCoordinacion/",
                data: Coordinacion1
            })

            //$http.post(URLServices + "Coordinacion/GuardarCoordinacion/", Coordinacion1)
            //    .then(function (response) {
            //        callback(response);
            //    });
        };

        service.ConsultarCoordinacionnn = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Coordinacion/ConsultarCoordinacion/"
            })

            //$http.get(URLServices + "Coordinacion/ConsultarCoordinacion/")
            //    .then(function (response) {
            //        callback(response);
            //    });
        };

        service.ConsultarProgramaxArea = function (IdArea, callback) {

            item = {
                Parametro1: IdArea
            };
            return $http({
                method: 'POST',
                url: URLServices + "Programa/ConsultarProgramaxArea/",
                data: item
            })
            //$http.post(URLServices + "Programa/ConsultarProgramaxArea/", item)
            //    .then(function (response) {
            //        callback(response);
            //    });
        };


        service.ModificarCoor = function (Coordi, callback) {

            Item = {
                Parametro1: Coordi[0].Parametro1
            };
            return $http({
                method: 'POST',
                url: URLServices + "Coordinacion/ModificarCoordinacion/",
                data: Item
            })
            //$http.post(URLServices + "Coordinacion/ModificarCoordinacion/", Item)
            //  .then(function (response) {
            //      callback(response);
            //  })
        };


        service.ConsultarIds = function (coordinacion, callback) {
            Item = {
                Parametro1: coordinacion[0].Parametro1
            };
            return $http({
                method: 'POST',
                url: URLServices + "Coordinacion/ConsultarIds/",
                data: Item
            })
            //$http.post(URLServices + "Coordinacion/ConsultarIds/", Item)
            //    .then(function (response) {
            //        callback(response);
            //    })
        };


  

        service.ConsultarAreasss = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Programa/ConsultarAreas/"
            })

            //$http.get(URLServices + "Programa/ConsultarAreas/")
            //    .then(function (response) {
            //        callback(response);
            //    });
        };

        service.ConsultarCentrosss = function (callback) {

            return $http({
                method: 'GET',
                url: URLServices + "Coordinacion/ConsultarCentros/"
            })

            //$http.get(URLServices + "Coordinacion/ConsultarCentros/")
            //    .then(function (response) {
            //        callback(response);
            //    });
        };

        service.GuardarModificacionCoordinacion = function (coordinacion, callback) {
            return $http({
                method: 'POST',
                url: URLServices + "Coordinacion/GuardarModificacionCoordinacion/",
                data: coordinacion
            })

            //$http.post(URLServices + "Coordinacion/GuardarModificacionCoordinacion", coordinacion)
            //    .then(function (response) {
            //        callback(response);
            //    })
        };


        service.BorrarCoordinacionessss = function (Coordinaciones, callback) {
            var Item = {
                Parametros: []
            };

            $.each(Coordinaciones, function (index, value) {
                Item.Parametros.push(
                    {
                        Parametro1: value.Parametro1
                    })
            });


            return $http({
                method: 'POST',
                url: URLServices + "Coordinacion/EliminarCoordinacion/",
                data: Item
            })
            //$http.post(URLServices + "Coordinacion/EliminarCoordinacion/", Item)
            //    .then(function (response) {
            //        callback(response);
            //    });
        };


        return service;

    }]);

