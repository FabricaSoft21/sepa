﻿ProgramacionApp.factory('ProgramaService',
    ['$http', '$rootScope', '$routeParams',
    function ($http, $rootScope, $routeParams) {
        var service = {};


        service.SubirArchivo = function (data, callback) {
            //waitingDialog.show();
            var ajaxRequest = $.ajax({
                type: "POST",
                url: URLServices + "File/UploadFilePrograma",
                contentType: false,
                processData: false,
                data: data
            }).done(function (responseData, textStatus) {
                callback(responseData);
                //waitingDialog.hide();
            }).fail(function () {
                //waitingDialog.hide();
            });
        };


        service.GuardarProgramaaa = function (Programa1, callback) {
            return $http({
                method: 'POST',
                url: URLServices + "Programa/GuardarPrograma/",
                data: Programa1
            })


            //$http.post(URLServices + "Programa/GuardarPrograma/", Programa1)
            //    .success(function (response) {
            //        callback(response);
            //    });
        };

        service.ConsultarProgramas = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Programa/ConsultarProgramas/"
            })

            //$http.get(URLServices + "Programa/ConsultarProgramas/")
            //    .success(function (response) {
            //        callback(response);
            //    });
        };

        service.ProgramaxCoordinacion = function (IdCoordinador, callback) {
            Item = {
                Parametro1: IdCoordinador
            };

            return $http({
                method: 'GET',
                url: URLServices + "Programa/ProgramaxCoordinacion/",
                data: Item
            })

            //$http.post(URLServices + "Programa/ProgramaxCoordinacion/", Item)
            //    .success(function (response) {
            //        callback(response);
            //    });
        };

        service.ModificarPrograma = function (Programa, callback) {
            
            Item = {
                Parametro1: Programa[0].Parametro1
            };

            return $http({
                method: 'POST',
                url: URLServices + "Programa/ModificarPrograma/",
                data: Item
            })

            //$http.post(URLServices + "Programa/ModificarPrograma/", Item)
            //  .success(function (response) {
            //      callback(response);
            //  })
        };


        service.GuardarModificacionPrograma = function (Programa, callback) {
            return $http({
                method: 'POST',
                url: URLServices + "Programa/GuardarModificacionPrograma/",
                data: Programa
            })

            //$http.post(URLServices + "Programa/GuardarModificacionPrograma", Programa)
            //    .success(function (response) {
            //        callback(response);
            //    });
        };



        service.BorrarPrograma = function (Programa, callback) {
            
            var Item = {
                Parametros: []
            }
            $.each(Programa, function (index, value) {
                Item.Parametros.push(
                    {
                        Parametro1: value.Parametro1
                    })
            });

            return $http({
                method: 'POST',
                url: URLServices + "Programa/EliminarPrograma/",
                data: Item
            })

            //$http.post(URLServices + "Programa/EliminarPrograma/", Item)
            //    .success(function (response) {
            //        callback(response);
            //    });
        };

        service.ConsultarAreas = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Programa/ConsultarAreas/"
            })

            //$http.get(URLServices + "Programa/ConsultarAreas/")
            //    .success(function (response) {
            //        callback(response);
            //    });
        };
       

        return service;

    }]);