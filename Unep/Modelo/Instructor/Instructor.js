﻿ProgramacionApp.controller('InstructorController',
    ['$scope', '$rootScope', '$location', 'InstructorService', '$routeParams', '$sce',
        function ($scope, $rootScope, $location, InstructorService, $routeParams, $sce) {

            var array = [];
            var count = 0;
            var nav = $("#navbar").hasClass("gn-menu-wrapper gn-open-all");
            if (nav == true) {
                $(".Principal").css("width", "80%");

            } else {
                $(".Principal").css("width", "95%");
            }

            $scope.Centro = {
                IdCentro: "",
                NombreCentro: "",
                IdRegional: "",
                Direccion: ""
            };

            $("#atras").hide();
            $("#habilitar").hide();

            //Funciones para la carga del archivo de excel
            $scope.SubirArchivo = function () {
                InstructorService.alerta();
            }

            $scope.Estado = [
                { Id: 1, Nombre: "Activo" }, { Id: 0, Nombre: "Inactivo" }
            ];

            //Función para abrir una modal
            $scope.AbrirModal = function () {
                $("#ModalInstructor").modal("show");
                $scope.VaciarCampos();
            };

            //Función para abrir una modal
            $scope.ConsultarProgramacion = function (doc) {
                $scope.ocultar();
                $scope.agre($scope.Programacion = [])
                InstructorService.ConsultarProgramacion(doc).then(function (response) {
                    if (response.data.success == true) {
                        for (var i = 0; i < response.data.datos.length; i++) {
                            var Ficha = response.data.datos[i].IdFicha;
                            var Instructor = response.data.datos[i].IdInstructor;
                            InstructorService.ConsultarIdentificadorFicha(response.data.datos[i].IdFicha).then(function (responses) {
                                if (response.data.success == true) {
                                    $scope.Programacion.push({ "IdProgramacion": response.data.datos[0].IdProgramacion, "IdFicha": responses.data.datos[0].IdFicha, "IdentificadorFicha": responses.data.datos[0].IdentificadorFicha, "IdInstructor": Instructor });
                                }
                            });
                        }
                        setTimeout(function () {
                            $scope.agre($scope.Programacion)
                        }, 1000)
                        $("#ModalProgramacionConsultar").modal("show");
                    }
                });
            };

            //Funcion eliminar programaciones
            $scope.Eliminar = function (id) {
                swal({
                    title: "Eliminar",
                    text: "Esta seguro de eliminar esta ficha de la programación",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, Eliminar",
                    closeOnConfirm: false
                },
function () {
    swal("Eliminada!", "La ficha se elimino satisfactoriamente", "success");

    console.log(id)
    $scope.agre($scope.Programacion = [])
    InstructorService.Eliminar(id).then(function (response) {
        if (response.data.success == true) {
            for (var i = 0; i < response.data.datos.length; i++) {
                var Ficha = response.data.datos[i].IdFicha;
                var Instructor = response.data.datos[i].IdInstructor;
                InstructorService.ConsultarIdentificadorFicha(response.data.datos[i].IdFicha).then(function (responses) {
                    if (response.data.success == true) {
                        $scope.Programacion.push({ "IdProgramacion": response.data.datos[0].IdProgramacion, "IdFicha": responses.data.datos[0].IdFicha, "IdentificadorFicha": responses.data.datos[0].IdentificadorFicha, "IdInstructor": Instructor });
                    }
                });
            }
            setTimeout(function () {
                $scope.agre($scope.Programacion)
            }, 200)
        }
    })
});
            }

            //Función para abrir una modal
            $scope.CerrarProgramacion = function (doc) {
                $scope.ocultar();
                for (var i = 1; i < 7; i++) {
                    $('#boton' + i).val("");
                    if (i <= 3) {
                        $('#check' + i).val(false);
                    }
                }
                $("#ModalProgramacion").modal("hide");
                $scope.agre($scope.Programacion = []);
            };

            //Funcion para abrir modal de programacion
            $scope.programar = function () {
                $scope.agre($scope.Programacion = [])
                var InstructorProgramacion = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                $('#activos').val(0);
                if (InstructorProgramacion.length == 0) {
                    swal("Error!", "Debe por lo menos seleccionar un instructor", "error")
                } else {
                    InstructorService.ConsultarFichas().then(function (response) {
                        if (response.data.success == true) {
                            $scope.Ficha = response.data.datos;
                            $("#ModalProgramacion").modal("show");
                        } else {
                            swal("Error!", "Algo anda mal, recargue la pagina", "error")
                        }
                    });

                }
            };

            //Declaración del objeto instructor
            $scope.Instructor = {
                IdInstructor: "",
                TipoDocumento: "",
                Nombres: "",
                Apellidos: "",
                NumeroDocumento: "",
                Correo: "",
                Estado: "",
                Telefono: "",
                IdArea: "",
                IdCentro: "",
                Idcordi: "",
                PicoPlaca: ""
            };

            //Declarar objeto Coordinacion
            $scope.Coordinacion = {
                IdCoordinacion: "",
                NumeroDocumento: "",
                TipoDocumento: "",
                Nombres: "",
                Apellidos: "",
                Telefono: "",
                Correo: ""
            };

            //Declaracion objeto pico y placa
            $scope.Placa = {
                Lunes: false,
                Martes: false,
                Miercoles: false,
                Jueves: false,
                Viernes: false,
                Sabado: false,
            };

            //Función para vaciar los campos de las modales
            $scope.VaciarCampos = function () {
                $scope.Instructor.Area = "";
                $scope.Instructor.Nombres = "";
                $scope.Instructor.Apellidos = "";
                $scope.Instructor.NumeroDocumento = "";
                $scope.Instructor.Correo = "";
                $scope.Instructor.Telefono = "";
                $scope.Placa.Lunes = false;
                $scope.Placa.Martes = false;
                $scope.Placa.Miercoles = false;
                $scope.Placa.Jueves = false;
                $scope.Placa.Viernes = false;
                $scope.Placa.Sabado = false;
            }

            //Ocultar elementos
            $scope.ocultar = function () {
                for (var i = 1; i < 7; i++) {
                    $('#label' + i).hide();
                }
                for (var i = 1; i < 7; i++) {
                    $('#boton' + i).hide();
                }
                for (var i = 1; i < 3; i++) {
                    $('#check' + i).hide();
                }
            }

            //Mostrar elementos
            $scope.mostrar = function () {
                for (var i = 1; i < 7; i++) {
                    $('#label' + i).show();
                }
                for (var i = 1; i < 7; i++) {
                    $('#boton' + i).show();
                }
                for (var i = 1; i < 3; i++) {
                    $('#check' + i).show();
                }
            }

            //Agregar fichas
            $scope.Agregar = function () {
                var instructorSelect = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                var texto = $('#listarFicha :selected').text();
                var valor = $('#listarFicha').val();
                var vali = true;
                if (texto != "") {
                    for (var i = 0; i < $scope.Programacion.length; i++) {
                        if ($scope.Programacion[i].IdentificadorFicha == texto) {
                            vali = false;
                            break;
                        }
                    }
                    if (vali) {
                        $scope.Programacion.push({ "IdFicha": valor, "IdentificadorFicha": texto, "IdInstructor": instructorSelect[0].IdInstructor })
                        for (var i = 0; i < $scope.Ficha.length; i++) {
                            if ($scope.Ficha[i].IdFicha == valor) {
                                var act = 0;
                                if ($("#activos").val() == "") {
                                    act = 0;
                                } else {
                                    act = parseInt($("#activos").val());
                                }
                                var total = parseInt($scope.Ficha[i].AprendicesActivos) + parseInt(act);
                                $("#activos").val(total);
                                break;
                            }
                        }
                        $scope.agre($scope.Programacion);
                    } else {
                        swal("Error!", "La ficha " + texto + " ya fue ingresada", "error")
                    }
                } else {
                    swal("Error!", "Debe seleccionar una ficha", "error")
                }
            };

            $scope.agre = function (dato) {
                $scope.Programacion = dato;
                console.log($scope.Programacion, dato)
            }

            //Eliminar del array
            $scope.QuitarFicha = function (InstructorBorrare) {
                var pos = 0;
                for (var i = 0; i < $scope.Programacion.length; i++) {
                    if ($scope.Programacion[i].IdentificadorFicha == InstructorBorrare) {
                        pos = i;
                        var total = parseInt($("#activos").val());
                        for (var e = 0; e < $scope.Ficha.length; e++) {
                            if ($scope.Programacion[i].IdFicha == $scope.Ficha[e].IdFicha) {
                                var tot = parseInt($scope.Ficha[e].AprendicesActivos);
                                break;
                            }
                        }
                        total = total - tot;
                        $("#activos").val(total);
                        break;
                    }
                }
                $scope.Programacion.splice(pos, 1);
                $scope.agre($scope.Programacion)
                console.log(InstructorBorrare);
            }

            //Detalle Ficha
            $scope.Detalle = function (NumProgramacion) {
                var Ficha = $scope.Programacion[NumProgramacion].IdFicha;
                var instructor = $scope.Programacion[NumProgramacion].IdInstructor;
                $scope.mostrar();
                $('#boton6').val($scope.Programacion[NumProgramacion].IdentificadorFicha)
                InstructorService.ConsultarIdentificadorFicha(Ficha).then(function (responses) {
                    if (responses.data.success == true) {
                        $('#boton1').val(responses.data.datos[0].AprendicesActivos);
                        $('#boton2').val(responses.data.datos[0].FechaMomentoDos.substring(0, 10));
                        $('#boton3').val(responses.data.datos[0].FechaMomentoTres.substring(0, 10));
                        InstructorService.ConsultarProgramacion(instructor).then(function (responder) {
                            if (responder.data.success == true) {
                                for (var i = 0; i < responder.data.datos.length; i++) {
                                    if (responder.data.datos[i].IdFicha == Ficha) {
                                        document.getElementById("check1").checked = (responder.data.datos[i].FechaMomentoDos == null) ? false : true;
                                        document.getElementById("check2").checked = (responder.data.datos[i].FechaMomentoTres == null) ? false : true;
                                        $('#boton4').val((responder.data.datos[i].FechaMomentoDos == null) ? "Sin realizar" : responder.data.datos[i].FechaMomentoDos.substring(0, 10));
                                        $('#boton5').val((responder.data.datos[i].FechaMomentoTres == null) ? "Sin realizar" : responder.data.datos[i].FechaMomentoTres.substring(0, 10));
                                    }
                                }
                            }
                        });
                    }
                });
            }

            //objeto ficha
            $scope.Ficha = {
                IdFicha: "",
                IdentificadorFicha: "",
                Estado: "",
                Jornada: "",
                TipoRespuesta: "",
                FechaInicioFicha: "",
                FechaFinFicha: "",
                EtapaFicha: "",
                ModalidadFormacion: "",
                AprendicesActivos: "",
                FechaLimite: "",
                FechaMomentoDos: "",
                FechaMomentoTres: "",
                FechaInicioProductiva: "",
                CapacitacionSGVA: "",
                IdDetalle: ""
            };

            $scope.Programacion = [];

            //Funcion para subir listado de instructores 

            $scope.UploadFileWeb = function () {
                $("#fileUploadWeb").trigger('click');
            };
            $("#fileUploadWeb").change(function () {
                dataweb = new FormData();
                var files = $("#fileUploadWeb").get(0).files;
                var fileExtension = ['xlsx'];
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    swal("Error!", "La extencion del archivo no es valida", "error")
                    $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                    return false;
                }
                // Add the uploaded image content to the form data collection
                if (files.length > 0) {
                    readURL(this, "logoweb");
                    dataweb.append("UploadedImage", files[0]);
                    if (dataweb != null) {
                        InstructorService.SubirArchivo(dataweb, function (response) {
                            if (response.success) {
                                swal("Correcto!", "La importación del archivo se realizó con éxito", "success")
                                $scope.path = response.path;
                                $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                                InstructorService.ConsultarInstructores(function (response) {
                                    if (response.success == true) {
                                        $scope.datalists = response.datos;
                                        $scope.ListaCompleta = response.datos;
                                        $scope.numberOfPages = function () {
                                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                                        };
                                    }
                                });
                                $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                                return;
                            }
                        });
                    }
                }
            });

            function readURL(input, control) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#' + control + '').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            };

            //Dias Pico y Placa
            $scope.Pico = function () {
                var Placas = "";
                var tol = 0;
                Placas += ($scope.Placa.Lunes) ? "Lunes - " : "";
                Placas += ($scope.Placa.Martes) ? "Martes - " : "";
                Placas += ($scope.Placa.Miercoles) ? "Miercoles - " : "";
                Placas += ($scope.Placa.Jueves) ? "Jueves - " : "";
                Placas += ($scope.Placa.Viernes) ? "Viernes - " : "";
                Placas += ($scope.Placa.Sabado) ? "Sabado - " : "";
                tol = Placas.length;
                Placas = Placas.substring(0, tol - 3);
                return Placas;
            }

            //Función para Consultar las areas
            $scope.areas = function (coordi) {
                if (coordi == null) {
                    var sd = $('#listasAreaR');
                    setTimeout(function () {
                        sd.val("").trigger("change");
                    }, 100);
                    $('#listasAreaR').attr('disabled', true);
                } else {
                    $('#listasAreaR').attr('disabled', false);
                    var centro = $('#listasCentroR').val()
                    InstructorService.ConsultarAreas(centro, coordi).then(function (response) {
                        if (response.data.success == true) {
                            $scope.Area = response.data.datos;
                        }
                    });
                }
            }

            //Función para Consultar las areas
            $scope.areasE = function (coordi) {
                if (coordi == null) {
                    var sd = $('#listasAreaE');
                    setTimeout(function () {
                        sd.val("").trigger("change");
                    }, 100);
                    $('#listasAreaE').attr('disabled', true);
                } else {
                    $('#listasAreaE').attr('disabled', false);
                    var centro = $('#listasCentroE').val()
                    InstructorService.ConsultarAreas(centro, coordi).then(function (response) {
                        if (response.data.success == true) {
                            $scope.Area = response.data.datos;
                        }
                    });
                }
            }

            //Función para Consultar los centros 
            InstructorService.ConsultarCentros().then(function (response) {
                if (response.data.success == true) {
                    $scope.Centro = response.data.datos;
                }
            });

            //Función para Consultar las coordinaciones 
            $scope.Coordinaciones = function (id) {
                if (id == null) {
                    var sf = $('#listasCoordiR');
                    var sd = $('#listasAreaR');
                    setTimeout(function () {
                        sf.val("").trigger("change");
                        sd.val("").trigger("change");
                    },100);
                    $('#listasCoordiR').attr('disabled', true);
                    $('#listasAreaR').attr('disabled', true);
                } else {
                    $('#listasCoordiR').attr('disabled', false);
                    InstructorService.ConsultarCoordinaciones(id).then(function (response) {
                        if (response.data.success == true) {
                            $scope.Coordinacion = response.data.datos;
                        }
                    });
                }
            }

            //Función para Consultar las coordinaciones 
            $scope.CoordinacionesE = function (id) {
                if (id == null) {
                    var sf = $('#listasCoordiE');
                    var sd = $('#listasAreaE');
                    setTimeout(function () {
                        sf.val("").trigger("change");
                        sd.val("").trigger("change");
                    }, 100);
                    $('#listasCoordiE').attr('disabled', true);
                    $('#listasAreaE').attr('disabled', true);
                } else {
                    $('#listasCoordiE').attr('disabled', false);
                    InstructorService.ConsultarCoordinaciones(id).then(function (response) {
                        if (response.data.success == true) {
                            $scope.Coordinacion = response.data.datos;
                        }
                    });
                }
            }

            //Función para Consultar los instructores 
            InstructorService.ConsultarInstructores().then(function (response) {
                if (response.data.success == true) {
                    if (response.data.datos.length > 0) {
                        $('#labelaprendi').hide();
                        $scope.curPage = 0;
                        $scope.pageSize = 8;
                        $scope.datalists = response.data.datos;

                        $scope.numberOfPages = function () {
                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                        };

                        $scope.Datos = $scope.datalists;
                    } else {
                        $('#labelaprendi').show();
                    }
                }
            });
                  
            //Función para Guardar los instructores 
            $scope.Guardar = function () {
                $.each($scope.Estado, function (index, value) {
                    if (value.Id == $scope.Estado.Id) {
                        $scope.Instructor.Estado = value.Id
                    }
                });
                $.each($scope.Area, function (index, value) {
                    if (value.IdArea == $scope.Area.IdArea) {
                        $scope.Instructor.IdArea = value.IdArea;
                    }
                });
                $.each($scope.Centro, function (index, value) {
                    if (value.IdCentro == $scope.Centro.IdCentro) {
                        $scope.Instructor.IdCentro = value.IdCentro;
                    }
                });
                $.each($scope.Coordinacion, function (index, value) {
                    if (value.IdCoordinacion == $scope.Coordinacion.IdCoordinacion) {
                        $scope.Instructor.Idcordi = value.IdCoordinacion;
                    }
                });

                if ($scope.Instructor.Nombres == "" || $scope.Instructor.IdArea == "" || $scope.Instructor.Apellidos == "" || $scope.Instructor.NumeroDocumento == null || $scope.Instructor.Correo == "" || $scope.Instructor.Estado == null) {
                    swal("Error!", "Debe diligenciar todos los campos", "error")
                } else {
                    $scope.Instructor.PicoPlaca = $scope.Pico();
                    InstructorService.GuardarInstructor($scope.Instructor).then(function (response) {
                        if (response.data.success == true) {
                            $scope.VaciarCampos();

                            InstructorService.ConsultarInstructores().then(function (response) {

                                if (response.data.success == true) {
                                    $('#labelaprendi').hide();
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                }
                            });
                            swal("Correcto!", "El registro se realizó con éxito", "success")
                        }
                        $("#ModalInstructor").modal("hide");    
                    })
                }
            }

            //Función para filtrar 
            $scope.Filter = function (e) {



                var Busqueda = $("#Buscar").val();
                var exp = new RegExp(Busqueda);
                if (Busqueda == "") {
                    InstructorService.ConsultarInstructores().then(function (response) {
                        if (response.data.success == true) {
                            $scope.datalists = response.data.datos;
                            $scope.ListaCompleta = response.data.datos;
                        }
                    });
                }
                var Instructor = [];
                $scope.datalists = $scope.ListaCompleta;
                Instructor = $scope.datalists.filter(function (item) {



                    if (exp.test(item.Nombre.toLowerCase()) || exp.test(item.Nombre.toUpperCase())) {

                        return item;
                    }

                    else if (exp.test(item.Apellido.toLowerCase()) || exp.test(item.Apellido.toUpperCase())) {
                        return item;
                    }

                    else if (exp.test(item.Telefono.toLowerCase()) || exp.test(item.Telefono.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Email.toLowerCase()) || exp.test(item.Email.toUpperCase())) {
                        return item;
                    }

                });
                $scope.datalists = Instructor;
                //Variable para setear la paginación 
                $scope.curPage = 0;
            };

            //Función para seleccionar todos los instructores de la tabla 
            $scope.SeleccionarTodos = function () {
                $.each($scope.Datos, function (index, value) {
                    value.Seleccionado = $scope.SeleccionTodos;
                });
            };

  
            //Cambiar estado de los instructores
            $scope.CambiarEstadoSeleccionados = function () {
                var UsariosBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (UsariosBorrar.length == 0) {
                    swal("Error!", "Seleccione por lo menos un instructor", "error")
                } else {

                    $("#modalInhabilitar").modal("show");
                }
            };

            //Inhabilitar los instructores
            $scope.inhabilitar = function () {
                var InstructorBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                InstructorService.CambiarEstado(InstructorBorrar).then(function (response) {

                    if (response.data.success == true) {

                        InstructorService.ConsultarInstructores().then(function (response) {
                            if (response.data.success == true) {
                                if (response.data.datos.length > 0) {
                                    $('#labelaprendi').hide();
                                } else {
                                    $('#labelaprendi').show();
                                }
                                swal("Correcto!", "El instructor se inhabilito", "success")
                                $scope.datalists = response.data.datos;
                                $scope.ListaCompleta = response.data.datos;
                            }


                        });
                    }
                });
            }

            //Registrar programacion
            $scope.GuardarProgramacion = function () {
                console.log($scope.Programacion);
                if ($scope.Programacion.length > 0) {
                    InstructorService.ProgramacionInstructor($scope.Programacion).then(function (response) {
                        if (response.data.success == true) {
                            swal("Correcto!", "Se registro la programacion correctamente", "success")
                            $("#ModalProgramacion").modal("hide")
                        }
                    });
                } else {
                    swal("Error!", "Debe seleccionar por lo menos una ficha", "error")
                }
            }

            //Función para Consultar los instructores Inhabilitados
            $scope.ConsultarInhabilitados = function () {
                InstructorService.ConsultarInhabilitados().then(function (response) {
                    if (response.data.success == true) {
                        if (response.data.datos.length > 0) {
                            $('#labelaprendi').hide();
                        } else {
                            $('#labelaprendi').show();
                        }
                        $scope.datalists = response.data.datos;
                        $scope.ListaCompleta = response.data.datos;
                        $scope.numberOfPages = function () {
                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                        };
                    } 
                });


                $("#atras").show();
                $("#habilitar").show();
                $("#eliminar").hide();
                $("#modificar").hide();
                $("#descargar").hide();
                $("#excel").hide();
                $("#Programa").hide();
                $("#inhabilitados").hide();
            };


            //Función para Consultar los instructores Habilitados
            $scope.ConsultarHabilitados = function () {
                InstructorService.ConsultarInstructores().then(function (response) {
                    if (response.data.success == true) {
                        if (response.data.datos.length > 0) {
                            $('#labelaprendi').hide();
                        } else {
                            $('#labelaprendi').show();
                        }
                        $scope.curPage = 0;
                        $scope.pageSize = 8;
                        $scope.datalists = response.data.datos;

                        $scope.numberOfPages = function () {
                            return Math.ceil($scope.datalists.length / $scope.pageSize);
                        };

                        $scope.Datos = $scope.datalists;
                    }
                }
              )

                $("#atras").hide();
                $("#habilitar").hide();
                $("#eliminar").show();
                $("#modificar").show();
                $("#descargar").show();
                $("#excel").show();
                $("#Programa").show();
                $("#inhabilitados").show();

            };
  

            //Función para Habilitar los instructores Inhabilitados
            $scope.HabilitarInstructor = function () {
                var InstructorHabilitar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (InstructorHabilitar.length == 0) {
                    swal("Error!", "Debe seleccionar un instructor", "error")
                } else {
                    InstructorService.HabilitarInstructor(InstructorHabilitar).then(function (response) {
                        if (response.data.success == true) {
                            InstructorService.ConsultarInstructores().then(function (response) {
                                if (response.data.success == true) {
                                    $('#labelaprendi').hide();
                                    $scope.curPage = 0;
                                    $scope.pageSize = 8;
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                    $scope.numberOfPages = function () {
                                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                                    };

                                    $scope.Datos = $scope.datalists;
                                    swal("Correcto!", "Se habilito el instructor correctamente", "success")
                                }
                            });
                        }
                        $("#atras").hide();
                        $("#habilitar").hide();
                        $("#eliminar").show();
                        $("#modificar").show();
                        $("#descargar").show();
                        $("#Programa").show();
                        $("#excel").show();
                        $("#inhabilitados").show()
                    });
                }

            };


            //Funciona para modificar los datos del instructor
            $scope.Modificar = function () {
                var InstructorModificar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;

                });

                if (InstructorModificar.length == 1) {

                    InstructorService.ModificarInstructor(InstructorModificar).then(function (response) {

                        if (response.data.success == true) {

                            $scope.Instructor.IdInstructor = response.data.Instructor.IdInstructor;
                            $scope.Instructor.Nombres = response.data.Instructor.Nombres;
                            $scope.Instructor.Apellidos = response.data.Instructor.Apellidos;
                            $scope.Instructor.NumeroDocumento = parseInt(response.data.Instructor.NumeroDocumento);
                            $scope.Instructor.Correo = response.data.Instructor.Correo;
                            $scope.Instructor.Telefono = parseInt(response.data.Instructor.Telefono);
                            var sc = $('#listasCentroE');
                            setTimeout(function () {
                                sc.val(response.data.Instructor.IdCentro).trigger("change");
                            }, 1000);
                            $scope.CoordinacionesE(response.data.Instructor.IdCentro);
                            $scope.areasE(response.data.Instructor.Idcordi);
                            var sce = $('#listasCoordiE');
                            var sf = $('#listasAreaE');
                            var sd = $('#listastde');
                            setTimeout(function () {
                                sce.val(response.data.Instructor.Idcordi).trigger("change");
                            }, 1100);
                            setTimeout(function () {
                                sf.val(response.data.Instructor.IdArea).trigger("change");
                            }, 1200);
                            setTimeout(function () {
                                sd.val(response.data.Instructor.TipoDocumento).trigger("change");
                            }, 1000);
                            InstructorService.Picoyplaca(response.data.Instructor.PicoPlaca).then(function (res) {
                                if (res.data.success == true) {
                                    $scope.Placa.Lunes = (res.data.datos.Parametro2 == "true") ? true : false;
                                    $scope.Placa.Martes = (res.data.datos.Parametro3 == "true") ? true : false;
                                    $scope.Placa.Miercoles = (res.data.datos.Parametro4 == "true") ? true : false;
                                    $scope.Placa.Jueves = (res.data.datos.Parametro5 == "true") ? true : false;
                                    $scope.Placa.Viernes = (res.data.datos.Parametro6 == "true") ? true : false;
                                    $scope.Placa.Sabado = (res.data.datos.Parametro7 == "true") ? true : false;
                                } else {
                                    $scope.Placa.Lunes = false;
                                    $scope.Placa.Martes = false;
                                    $scope.Placa.Miercoles = false;
                                    $scope.Placa.Jueves = false;
                                    $scope.Placa.Viernes = false;
                                    $scope.Placa.Sabado = false;
                                }
                            });
                            $("#ModalEditar").modal("show");
                        }

                    });

                } else {
                    swal("Error!", "Debe seleccionar un instructor", "error")
                }

            };

            //Funciona para guardar la modificacion del instructor
            $scope.GuardarEdicionInstructor = function () {

                $.each($scope.Estado, function (index, value) {
                    if (value.Id == $scope.Estado.Id) {
                        $scope.Instructor.Estado = value.Id
                    }
                });
                $.each($scope.Area, function (index, value) {
                    if (value.IdArea == $scope.Area.IdArea) {
                        $scope.Instructor.IdArea = value.IdArea;
                    }
                });
                $.each($scope.Coordinacion, function (index, value) {
                    if (value.IdCoordinacion == $scope.Coordinacion.IdCoordinacion) {
                        $scope.Instructor.Idcordi = value.IdCoordinacion;
                    }
                });
                $.each($scope.Centro, function (index, value) {
                    if (value.IdCentro == $scope.Centro.IdCentro) {
                        $scope.Instructor.IdCentro = value.IdCentro;
                    }
                });

                if ($scope.Instructor.Nombres == null || $scope.Instructor.IdArea == "" || $scope.Instructor.Apellidos == "" || $scope.Instructor.NumeroDocumento == null || $scope.Instructor.Correo == "" || $scope.Instructor.TipoDocumento == "") {
                    swal("Error!", "Debe diligenciar todos los campos", "error")
                } else {
                    $scope.Instructor.PicoPlaca = $scope.Pico();
                    InstructorService.GuardarModificacionInstructor($scope.Instructor).then(function (response) {
                        if (response.data.success == true) {
                            $scope.VaciarCampos();

                            InstructorService.ConsultarInstructores().then(function (response) {

                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                }
                            });
                            swal("Correcto!", "Se actualizo el registro", "success")
                        }
                        $scope.VaciarCampos();
                        $("#ModalEditar").modal("hide");
                    })
                }


            };

            //Registrar programacion
            $scope.programare = function () {
                var InstructorProgramacion = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (InstructorProgramacion.length == 0) {
                    swal("Error!", "Debe por lo menos seleccionar un instructor", "error")
                } else {
                    $("#ModalProgramacion").modal("show");
                    InstructorService.HabilitarInstructor(InstructorHabilitar).then(function (response) {
                        if (response.data.success == true) {
                            InstructorService.ConsultarInstructores().then(function (response) {
                                if (response.success == true) {

                                    $scope.curPage = 0;
                                    $scope.pageSize = 8;
                                    $scope.datalists = response.datos;
                                    $scope.ListaCompleta = response.datos;
                                    $scope.numberOfPages = function () {
                                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                                    };

                                    $scope.Datos = $scope.datalists;
                                }
                            });
                        }
                    });
                }
            }
        }]);

