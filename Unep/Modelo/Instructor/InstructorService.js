﻿ProgramacionApp.factory('InstructorService',
    ['$http', '$rootScope', '$routeParams',
    function ($http, $rootScope, $routeParams) {
        var service = {};

        service.SubirArchivo = function (data) {
            waitingDialog.show();
            console.log(data);
            var ajaxRequest = $.ajax({
                type: "POST",
                url: URLServices + "File/UploadFileInstructor",
                contentType: false,
                processData: false,
                data: data,
                async: true,
            }).done(function (responseData, textStatus) {
                (responseData);
                waitingDialog.hide();
            }).fail(function () {
                waitingDialog.hide();
                location.reload();
            });
        };

        service.ConsultarProgramas = function () {
            return $http({
                method: "GET",
                url: URLServices + "Instructor/ConsultarProgramas/"
            })
        };

        service.ConsultarIdentificadorFicha = function (ficha) {
            item = {
                Parametro2: ficha
            };
            return $http({
                method: "POST",
                url: URLServices + "Instructor/ConsultarIdentificadorFicha/",
                data: item
            })
        };

        service.ConsultarProgramacion = function (doc) {
            item = {
                Parametro2: doc
            };
            return $http({
                method: "POST",
                url: URLServices + "Instructor/ConsultarProgramacion/",
                data:item
            })
        };

        service.ConsultarFichas = function () {
            return $http({
                method: "GET",
                url: URLServices + "Instructor/fichas/"
            })
        };

        service.GuardarInstructor = function (Instructor1) {
            return $http({
                method: "POST",
                url: URLServices + "Instructor/GuardarInstructor/",
                data: Instructor1
            })
        };


        service.Eliminar = function (id) {
            item = {
                Parametro2: id
            };
            return $http({
                method: "POST",
                url: URLServices + "Instructor/EliminarProgramacion/",
                data: item
            })
        };

        service.ProgramacionInstructor = function (Programa) {
            return $http({
                method: "POST",
                url: URLServices + "Instructor/RegistrarProgramacion",
                data: Programa
            })
        };

        service.ConsultarInstructores = function () {
            return $http({
                method: "POST",
                url: URLServices + "Instructor/ConsultarInstructores/"
            })
        };

        service.Picoyplaca = function (pico) {
            item = {
                parametro1: pico
            };
            return $http({
                method: "POST",
                url: URLServices + "Instructor/Picoyplaca/",
                data: item
            })
        };

        service.CambiarEstado = function (Instructor) {
            return $http({
                method: "POST",
                url: URLServices + "Instructor/CambiarEstado/",
                data: Instructor
            })
        };

        service.HabilitarInstructor = function (Instructor) {
            return $http({
                method: "POST",
                url: URLServices + "Instructor/HabilitarInstructor/",
                data: Instructor
            })
        };

        service.ModificarInstructor = function (Instructor) {
            return $http({
                method: "POST",
                url: URLServices + "Instructor/ModificarInstructor/",
                data: Instructor[0]
            })
        };

        service.GuardarModificacionInstructor = function (Instructor) {
            return $http({
                method: "POST",
                url: URLServices + "Instructor/GuardarModificacionInstructor/",
                data: Instructor
            })
        };

        service.ConsultarCentros = function () {
            return $http({
                method: "GET",
                url: URLServices + "Instructor/ConsultarCentros/"
            })
        };

        service.ConsultarCoordinaciones = function (id) {
            item = {
                Parametro2: id
            };
            return $http({
                method: "POST",
                url: URLServices + "Instructor/Consultarcoordinacion/",
                data: item
            });
        }

        service.ConsultarAreas = function (centro, coordinacion) {
            item = {
                Parametro2: centro,
                Parametro3: coordinacion
            };
            return $http({
                method: "POST",
                url: URLServices + "Instructor/ConsultarAreas/",
                data: item
            });
        };

        service.ConsultarInhabilitados = function () {
            return $http({
                method: "POST",
                url: URLServices + "Instructor/ConsultarInhabilitados"
            })
        };

        return service;

    }]);
         
         
         