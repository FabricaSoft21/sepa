﻿ProgramacionApp.controller('AreaController',
    ['$scope', '$rootScope', '$location', 'AreaService', '$routeParams', '$sce',
        function ($scope, $rootScope, $location, AreaService, $routeParams, $sce) {

            var nav = $("#navbar").hasClass("gn-menu-wrapper gn-open-all");
            if (nav == true) {
                $(".Principal").css("width", "80%");

            } else {
                $(".Principal").css("width", "95%");
            }



            $scope.UploadFileWeb = function () {
                $("#fileUploadWeb").trigger('click');
            };

            $("#fileUploadWeb").change(function () {
                dataweb = new FormData();

                var files = $("#fileUploadWeb").get(0).files;

                //
                var fileExtension = ['xlsx'];
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    bootbox.dialog({
                        title: "Importar Archivo",
                        message: "La extencion del archivo no es valida",
                        buttons: {
                            success: {
                                label: "Cerrar",
                                className: "btn-primary",
                            }
                        }
                    });
                    $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));

                    //waitingDialog.hide();
                    return false;

                }


                // Add the uploaded image content to the form data collection
                if (files.length > 0) {

                    readURL(this, "logoweb");

                    dataweb.append("UploadedImage", files[0]);
                    if (dataweb != null) {
                        AreaService.SubirArchivo(dataweb, function (response) {
                            if (response.success) {

                                bootbox.dialog({
                                    title: "Importar Archivo",
                                    message: "La importación del archivo se realizó con éxito ",
                                    buttons: {
                                        success: {
                                            label: "Cerrar",
                                            className: "btn-primary",
                                        }
                                    }
                                });



                                $scope.path = response.path;

                                $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                                AreaService.ConsultarAreas().then(function (response) {
                                    if (response.data.success == true) {
                                        $scope.datalists = response.data.datos;
                                        $scope.ListaCompleta = response.data.datos;
                                    }
                                });

                                $("#fileUploadWeb").replaceWith($("#fileUploadWeb").val('').clone(true));
                                //waitingDialog.hide();

                                return;
                            }
                        });
                    }
                }
            });

            function readURL(input, control) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#' + control + '').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            };

            $scope.Area = {
                IdArea: "",
                Nombre: "",
                Descripcion: "",
                CodigoArea: ""
            };

            $scope.AbrirModal = function () {
                $("#ModalArea").modal("show");
                $scope.VaciarCampos();
            };

            //Función para guardar un area
            $scope.Guardar = function () {
                if ($scope.Area.Nombre == "" || $scope.Area.Descripcion == "" || $scope.Area.CodigoArea == "") {

                    swal("¡Ten cuidado!", "Primero debes diligenciar todos los campos.");

                } else {
                    AreaService.GuardarArea($scope.Area).then(function (response) {
                        if (response.data.success == true) {

                            $scope.VaciarCampos();
                            AreaService.ConsultarAreas().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;

                                    $scope.ListaCompleta = response.data.datos;
                                    swal("Registro exitoso!", "El área fue registrada correctamente", "success");
                                }
                                $("#ModalArea").modal("hide");
                            });
                        } else {
                            swal("¡Información!", "Esta área ya esta registrada.");
                        }
                    });
                }
            };


            //Funcion para vaciar los campos
            $scope.VaciarCampos = function () {
                $scope.Area.IdArea = "";
                $scope.Area.Nombre = "";
                $scope.Area.Descripcion = "";
                $scope.Area.CodigoArea = "";

            };

            //Función para consultar las areas
            AreaService.ConsultarAreas().then(function (response) {
             
                if (response.data.success == true) {

                    $scope.curPage = 0;
                    $scope.pageSize = 10;
                    $scope.datalists = response.data.datos;
                    $scope.ListaCompleta = response.data.datos;
                    $scope.numberOfPages = function () {
                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                    };

                    $scope.Datos = $scope.datalists;
                }
            });


            //Función para Seleccionar todos las registros de la tabla
            $scope.SeleccionarTodos = function () {

                contador = (($scope.curPage + 1) * 3) - 3;
                var item = (($scope.curPage + 1) * 3) - 3;
                var items1 = (($scope.curPage + 1) * 3);


                $.each($scope.datalists, function (index, value) {

                    if (contador < items1) {
                        value[contador];
                        value.Seleccionado = $scope.SeleccionTodos;
                        contador++;

                    }
                });


            };

            //Función para Borrar todos las registros de la tabla
            $scope.BorrarSeleccionados = function () {
                var AreasBorrar = $scope.Datos.filter(function (item) {
                    return item.Seleccionado === true;
                });
                if (AreasBorrar.length == 0) {
                    alert("Seleccione un área para borrar");
                }
                if (AreasBorrar.length > 0) {
                    r = confirm("¿Desea eliminar las áreas?");
                    if (r == true) {
                        AreaService.BorrarAreas().then(AreasBorrar, function (response) {
                            if (response.success == true) {
                                alert("Eliminado!!");
                            }
                        })
                    }
                }
            };

            //Función 1 para el borrado de las areas
            $scope.CambiarEstadoSeleccionados = function () {
                var UsariosBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (UsariosBorrar.length == 0) {
                    swal("¡Ten cuidado!", "Primero debes elegir el área que deseas inhabilitar.");
                } else {
                    $("#modalInhabilitar").modal("show");
                }
            }

            //Función 2 para el borrado de las areas
            $scope.inhabilitar = function () {

                var AreaBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                AreaService.BorrarArea(AreaBorrar).then(function (response) {
                    if (response.data.success == true && response.data.respuesta == true) {
                        AreaService.ConsultarAreas().then(function (response) {
                            if (response.data.success == true) {
                                $scope.datalists = response.data.datos;
                                $scope.ListaCompleta = response.data.datos;
                            }
                        })
                        swal("Inhabilitar", "Se inhabilitaron la siguiente cantidad de registros " + response.data.contadorTrue, "warning")
                    } else {
                        swal("Inhabilitar", "No se puedieron elimanar la siguiente cantidad de registros " + response.data.contadorFalse + " porque estan asociados a otras tablas.", "warning")
                    }
                });
            };

            //Función 1 para editar las areas
            $scope.Modificar = function () {
                var AreaModificar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;

                });
                if (AreaModificar.length == 1) {

                    AreaService.ModificarArea(AreaModificar).then(function (response) {


                        console.log(AreaModificar);

                        if (response.data.success == true) {
                            $scope.Area.CodigoArea = response.data.Area.CodigoArea;
                            $scope.Area.IdArea = response.data.Area.IdArea;
                            $scope.Area.Nombre = response.data.Area.Nombre;
                            $scope.Area.Descripcion = response.data.Area.Descripcion;
                            $("#ModalEditar").modal("show");
                        }
                    });
                } else {
                    swal("¡Ten cuidado!", "Primero debes elegir el área que deseas modificar.");
                }
            }

            //Función 2 para editar las areas
            $scope.GuardarEdicionArea = function () {
                if ($scope.Area.Nombre == "" || $scope.Area.Descripcion == "" || $scope.Area.CodigoArea == "") {

                    swal("¡Ten cuidado!", "Primero debes diligenciar todos los campos.");

                } else {
                    AreaService.GuardarModificacionArea($scope.Area).then(function (response) {
                        if (response.data.success == true) {

                            swal("Modificación exitosa", "El área fue modificada correctamente", "success")

                            $("#ModalEditar").modal("hide");
                            $scope.VaciarCampos();
                            AreaService.ConsultarAreas().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                }
                            });
                        }
                    });
                }
            }

            //Función 1 para filtrar la tabla
            $scope.Filter = function (e) {
                var Busqueda = $("#Buscar").val();
                var exp = new RegExp(Busqueda);
                if (Busqueda == "") {
                    AreaService.ConsultarAreas().then(function (response) {
                        if (response.data.success == true) {
                            $scope.datalists = response.data.datos;
                            $scope.ListaCompleta = response.data.datos;
                        }
                    });
                }
                var Area = [];
                $scope.datalists = $scope.ListaCompleta;
                Area = $scope.datalists.filter(function (item) {

                    if (exp.test(item.Parametro5.toLowerCase()) || exp.test(item.Parametro5.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Parametro4.toLowerCase()) || exp.test(item.Parametro4.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Parametro2.toLowerCase()) || exp.test(item.Parametro2.toUpperCase())) {
                        return item;
                    }
                });
                $scope.datalists = Area;
                //Variable para setear la paginación 
                $scope.curPage = 0;
            };
        }]);