﻿ProgramacionApp.factory('AreaService',
    ['$http', '$rootScope', '$routeParams',
    function ($http, $rootScope, $routeParams) {
        var service = {};

        service.SubirArchivo = function (data, callback) {
            waitingDialog.show();
            var ajaxRequest = $.ajax({
                type: "POST",
                url: URLServices + "File/UploadFileArea",
                contentType: false,
                processData: false,
                data: data
            }).done(function (responseData, textStatus) {
                callback(responseData);
                waitingDialog.hide();
            }).fail(function () {
                waitingDialog.hide();
            });
        };


        service.GuardarArea = function (Area, callback) {
           return $http({
               method: 'POST',
               url: URLServices + "Area/GuardarArea/",
               data: Area
           })
        };

        service.ConsultarAreas = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Area/ConsultarAreas/"
            }) 
        };

        service.ModificarArea = function (Area, callback) {
            return $http({
                method: 'POST',
                url: URLServices + "Area/ModificarArea/",
                data: Area[0]
            })
        };

            service.GuardarModificacionArea = function (Area, callback) {
                return $http({
                    method: 'POST',
                    url: URLServices + "Area/GuardarModificacionArea/",
                    data: Area
                })
        };

        service.BorrarArea = function (Area, callback) {
            var Item = {
                Parametros: []
            }
            $.each(Area, function (index, value) {
                Item.Parametros.push(
                    {
                        Parametro1: value.Parametro1
                    })
            });
            return $http({
                method: 'POST',
                url: URLServices + "Area/EliminarArea/",
                data: Item
            })
        };
        
        return service;

    }]);