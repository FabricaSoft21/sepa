﻿ProgramacionApp.factory('GestionService',
    ['$http', '$rootScope', '$routeParams',
    function ($http, $rootScope, $q, $routeParams) {
        var service = {};



        service.ConsultarAprendiz = function () {
            return $http({
                method: 'GET',
                url: URLServices + "Aprendiz/ConsultarAprendiz/"
            })
        };


        service.ModificarAprendiz = function (doc) {
            item = {
                Parametro1: doc
            };
            return $http({
                method: 'POST',
                url: URLServices + "Gestion/ModificarAprendiz/",
                data: item
            })
        };

        

        service.ConsultarAlternativa = function () {
            return $http({
                method: 'GET',
                url: URLServices + "Gestion/ConsultarAlternativa/"
            })
        };

        service.selectfecha = function (doc) {
            var Item = {
                Parametro1 : doc
            }
            return $http({
                method: 'POST',
                url: URLServices + "Gestion/selectfecha/",
                data:Item
            })
        };

        service.ConsultarEmpresaSede = function () {
            return $http({
                method: 'GET',
                url: URLServices + "Gestion/ConsultarEmpresaSede/"
            })
        };


        service.GuardarModificacionAprendiz = function (Aprendiz) {
            return $http({
                method: 'POST', 
                url: URLServices + "Gestion/GuardarModificacionAprendiz/",
                data: Aprendiz
            })
        };

        service.GuardarGestion = function (gestion) {
            return $http({
                method: 'POST',
                url: URLServices + "Gestion/GuardarGestion/",
                data: gestion
            })
        };

        return service;
    }])