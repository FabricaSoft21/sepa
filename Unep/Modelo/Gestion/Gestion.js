﻿ProgramacionApp.controller('GestionController',
    ['$scope', '$rootScope', '$location', 'GestionService', '$routeParams', '$sce',
        function ($scope, $rootScope, $location, GestionService, $routeParams, $sce) {


            $scope.AbrirModalGestion = function () {
                $scope.VaciarCampos();

                $("#modificar").show();
                $("#panel1").css({ "display": "block" });
                $("#btnAtras").hide();
                $("#siguiente").show();
                $("#guardartodo").hide();
                $("#mostrarDos").css({ "background": "white", "border": "1px solid #fc7323", "color": "black" });
                $("#mostrarUno").css({ "background": "#fc7323", "color": "white" });
                $("#btnAtras").hide();
                $("#panel2").hide();
                $scope.deshabilitar();
                $("#selectemp").hide();
                $("#motivoid").hide();
                $("#guardartodo").hide();
                $("#guardar").hide();
                $("#ModalGestion").modal("show");
            };
            var nav = $("#navbar").hasClass("gn-menu-wrapper gn-open-all");
            if (nav == true) {
                $(".Principal").css("width", "80%");

            } else {
                $(".Principal").css("width", "95%");
            }
            $("#atras").hide();



            $scope.Aprendiz = {
                IdAprendiz: "",
                TipoDocumento: "",
                Nombres: "",
                PrimerApellido: "",
                SegundoApellido: "",
                NumeroDocumento: "",
                Correo: "",
                Estado: "",
                Telefono: "",
                PoblacionDPS: true,
                IdFicha: "",
                IdUsuario: ""

            }

            $scope.alternativa = {
                IdAlternativa: "",
                NombreAlternativa:""
            }


            GestionService.ConsultarAlternativa().then(function (response) {
                if (response.data.success == true) {
                    $scope.alternativa = response.data.datos;
                }
            });

            $scope.gestion = {
                IdGestion :"",
                 Arl :"",
                EmpresaSedido:"",
                DescripcionSedido :"",
                IdAprendiz:"",
                IdAlternativa :"",
                Idsede: "",
                FechaInicioProductiva: ""
            };
            //Funcion para consultar los aprendices de ficha abierta
            GestionService.ConsultarAprendiz().then(function (response) {
                $("#panel2").hide();
                $("#btnAtras").hide();
                if (response.data.success == true) {
                    $scope.curPage = 0;
                    $scope.pageSize = 10;
                    $scope.datalists = response.data.datos;
                    $scope.ListaCompleta = response.data.datos;
                    $scope.numberOfPages = function () {
                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                    };

                    $scope.Datos = $scope.datalists;
                }
            });

            $scope.Gestionar = function (doc) {
                GestionService.ModificarAprendiz(doc).then(function (response) {

                    if (response.data.success == true) {
                        $scope.Aprendiz.IdAprendiz = parseInt(response.data.Aprendiz.IdAprendiz);
                        $scope.Aprendiz.TipoDocumento = response.data.Aprendiz.TipoDocumento;
                        $scope.Aprendiz.NumeroDocumento = parseInt(response.data.Aprendiz.NumeroDocumento);
                        $scope.Aprendiz.Nombres = response.data.Aprendiz.Nombres;
                        $scope.Aprendiz.PrimerApellido = response.data.Aprendiz.PrimerApellido ;
                        $scope.Aprendiz.SegundoApellido = response.data.Aprendiz.SegundoApellido;
                        $scope.Aprendiz.Correo = response.data.Aprendiz.Correo;
                        $scope.Aprendiz.Telefono = parseInt(response.data.Aprendiz.Telefono);
                        $scope.Aprendiz.IdFicha = response.data.Aprendiz.IdFicha;
                        $scope.Aprendiz.IdUsuario = response.data.Aprendiz.IdUsuario;
                        $scope.AbrirModalGestion();
                    }

                });


                GestionService.selectfecha(doc).then(function (response) {

                    if (response.data.success == true) {

                       
                        $("#fichaini").val(response.data.Aprendiz);
                      


                    }

                });
               


            }

            $scope.Modificar = function () {

                $("#NombreAprendiz").removeAttr("disabled");
                $("#ApellidoAprendiz1").removeAttr("disabled");
                $("#ApellidoAprendiz2").removeAttr("disabled");
                $("#correo2").removeAttr("disabled");
                $("#telefono").removeAttr("disabled");
                $("#modificar").hide();
                $("#guardar").show();
            }


            $scope.GuardarModicarAprendiz = function () {
                if ($scope.Aprendiz.NumeroDocumento == null || $scope.Aprendiz.Nombres == null || $scope.Aprendiz.PrimerApellido == null || $scope.Aprendiz.SegundoApellido == null || $scope.Aprendiz.Correo == null || $scope.Aprendiz.Telefono == null ) {
                    swal("Información", "Debe diligenciar todos los campos", "error");
                } else {
                    GestionService.GuardarModificacionAprendiz($scope.Aprendiz).then(function (response) {
                        if (response.data.success == true) {
                            GestionService.ConsultarAprendiz().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                    swal("Información", "La modificación del registro se realizo con exito", "success");
                                    $scope.deshabilitar();
                                }
                            });
                        }
                    });
                }
            }

            $scope.habilitar = function () {

                $("#NombreAprendiz").removeAttr("disabled");
                $("#ApellidoAprendiz1").removeAttr("disabled");
                $("#ApellidoAprendiz2").removeAttr("disabled");
                $("#correo2").removeAttr("disabled");
                $("#telefono").removeAttr("disabled");
                $("#modificar").hide();
                $("#guardar").show();


            }

            $scope.deshabilitar = function () {
                $("#NombreAprendiz").attr("disabled","true");
                $("#ApellidoAprendiz1").attr("disabled", "true");
                $("#ApellidoAprendiz2").attr("disabled", "true");
                $("#correo2").attr("disabled", "true");
                $("#telefono").attr("disabled", "true");
                $("#modificar").show();
                $("#guardar").hide();


            }


            GestionService.ConsultarEmpresaSede().then(function (response) {
                if (response.data.success == true) {
                    $scope.Empresa = response.data.datos;
                }
            });


            $scope.GuardarGestion = function (response) {

                $scope.gestion.IdAprendiz = $scope.Aprendiz.IdAprendiz;
                $scope.gestion.IdAlternativa = $scope.alternativa.IdAlternativa;
                GestionService.GuardarGestion($scope.gestion).then(function (response) {

                    if (response.data.success == true) {
                        GestionService.ConsultarAprendiz().then(function (response) {
                            if (response.data.success == true) {
                               
                                swal("Información", "el registro se realizo con exito", "success");
                                $("#ModalGestion").modal("hide");
                             
                            }
                        });
                    } else {
                        swal("Error", "Este aprendiz ya tiene una gestion individual", "error");

                    }


                })


            }

            $scope.VaciarCampos = function () {

                setTimeout(function () {

                    $("#listasem").val("").trigger("change");
                    $("#listase").val("").trigger("change");
                    $("#listas").val("").trigger("change");
                });

                $scope.gestion.Arl = "";
                $scope.gestion.FechaInicioProductiva = "";
                $scope.gestion.DescripcionSedido = "";
                $scope.EmpresaSedido = "";
              

            }

            // pagination
            $scope.curPage = 0;
            $scope.pageSize = 10;
        }]);