﻿ProgramacionApp.controller('CentroController',
    ['$scope', '$rootScope', '$location', 'CentroService', '$routeParams', '$sce',
        function ($scope, $rootScope, $location, CentroService, $routeParams, $sce) {

            $scope.AbrirModal = function () {
                $("#ModalCentro").modal("show");
                $scope.VaciarCampos();
            };

            var nav = $("#navbar").hasClass("gn-menu-wrapper gn-open-all");
            if (nav == true) {
                $(".Principal").css("width", "80%");

            } else {
                $(".Principal").css("width", "95%");
            }
            $("#atras").hide();


            $scope.centro = {
                IdCentro: "",
                NombreCentro: "",
                IdRegional: "",
                Direccion: "",
                Codigo: ""
            };

            $scope.Zona = {
                IdZona: "",
                NombreZona: ""
            };

            $scope.Regional = {
                IdRegional: "",
                NombreRegional: "",
                IdZona: ""
            };

            // pagination
            $scope.curPage = 0;
            $scope.pageSize = 10;


            //Funcion para guardar los campos 
            $scope.Guardar = function () {
                $.each($scope.Regional, function (index, value) {
                    if (value.IdRegional == $scope.Regional.IdRegional) {
                        $scope.centro.IdRegional = value.IdRegional;
                    }
                });
                if ($scope.centro.NombreCentro != "" || $scope.centro.IdRegional != "" || $scope.centro.Direccion != ""  || $scope.centro.Codigo != "") {
                    CentroService.GuardarCentrooo($scope.centro).then(function (response) {
                        if (response.data.success == true) {
                            CentroService.ConsultarCentros().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;

                                    swal("Registro exitoso!", "El centro fue registrado correctamente", "success");
                                }
                                $("#ModalCentro").modal("hide");
                                $scope.VaciarCampos();
                            });
                        } else {
                            swal("¡Ten cuidado!", "Primero debes diligenciar todos los campos.");
                        }
                    });
                } else {
                    swal("¡Ten cuidado!", "Primero debes diligenciar todos los campos.");
                }
            };

            //Funcion para vaciar los campos
            $scope.VaciarCampos = function () {
                $scope.centro.NombreCentro = "";
                $scope.centro.Descripcion = "";
                $scope.centro.Direccion = "";
                $scope.centro.IdZona = "";
                $scope.centro.IdRegional = "";
                $scope.centro.Codigo = "";
            };

            //Funcion para consultar los centros
            CentroService.ConsultarCentros().then(function (response) {
                if (response.data.success == true) {

                    $scope.datalists = response.data.datos;

                    $scope.numberOfPages = function () {
                        return Math.ceil($scope.datalists.length / $scope.pageSize);
                    };

                    $scope.Datos = $scope.datalists;
                }
            });

            //Funcion para consultar las zonas
            CentroService.ConsultarZona().then(function (response) {
                if (response.data.success == true) {
                    $scope.Zona = response.data.datos;
                }
            });

            //Funcion para consultar las regionales
            $scope.ConsultarZonasRegionales = function (IdZona) {

                $.each($scope.Zona, function (index, value) {
                    if (value.IdZona == IdZona) {


                        CentroService.ConsultarRegionales(value.IdZona).then( function (response) {

                            if (response.data.success == true) {
                                $scope.Regional = response.data.datos;
                            }
                        });
                    }

                });
            };

            //Funcion para consultar los centros
            $scope.ConsultarCentrosRegionales = function (IdRegional) {

                $.each($scope.centro, function (index, value) {
                    if (value.IdRegional == IdRegional) {


                        CentroService.ConsultarCentrosIDRegional(value.IdRegional).then(function (response) {

                            if (response.data.success == true) {
                                $scope.centro = response.data.datos;
                            }
                        });
                    }

                });
            };

            //Funcion para Modificar los centros
            $scope.Editar = function () {
                var CentroModificar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                if (CentroModificar.length == 1) {
                    CentroService.ModificarCentrooo(CentroModificar).then(function (response) {
                     
                        if (response.data.success == true) {

                            $scope.centro.IdCentro = response.data.centro.IdCentro;
                            $scope.centro.NombreCentro = response.data.centro.NombreCentro;
                            $scope.centro.IdRegional = response.data.centro.IdRegional;
                            $scope.centro.Direccion = response.data.centro.Direccion;
                            $scope.centro.Codigo = response.data.centro.Codigo;
                            CentroService.ConsultarZonaxRegional($scope.centro.IdRegional).then(function (response) {
                                console.log(response);
                                if (response.data.success == true) {
                               
                                    var sf = $('#zona');
                                    setTimeout(function () {
                                        sf.val(response.data.datos.IdZona).trigger("change");
                                    }, 500);
                                    CentroService.ConsultarRegionales(response.data.datos.IdZona).then( function (response) {
                                        if (response.data.success == true) {
                                            $scope.Regional = response.data.datos;
                                            var sf = $('#regional');
                                            setTimeout(function () {
                                                sf.val($scope.centro.IdRegional).trigger("change");
                                            }, 200);
                                        }
                                    });
                                }
                            });
                            $("#ModalEditar").modal("show");
                        }
                    });
                } else {
                    swal("¡Ten cuidado!", "Primero debes elegir el centro que deseas modificar.");
                }
            };

            //Funcion para Guardar la modificación de los centros
            $scope.GuardarEdicionCentro = function () {
                $.each($scope.Regional, function (index, value) {
                    if (value.IdRegional == $scope.Regional.IdRegional) {
                        $scope.centro.IdRegional = value.IdRegional;
                    }
                });
                if ($scope.centro.NombreCentro != "" || $scope.centro.IdRegional != "" || $scope.centro.Direccion != "" || $scope.centro.Codigo != "") {
                    CentroService.GuardarModificacionCentrooo($scope.centro).then(function (response) {
                        if (response.data.success == true) {

                            swal("Modificación exitosa!", "El centro fue modificado exitosamente", "success")

                            $("#ModalEditar").modal("hide");
                            CentroService.ConsultarCentros().then(function (response) {
                                if (response.data.success == true) {
                                    $scope.datalists = response.data.datos;
                                    $scope.ListaCompleta = response.data.datos;
                                }
                            });
                        }
                    });
                } else {
                    swal("¡Ten cuidado!", "Primero debes diligenciar todos los campos.");
                }
            }

            //Funcion para Seleccionar los centros
            $scope.SeleccionarTodos = function () {

                contador = (($scope.curPage + 1) * 3) - 3;
                var item = (($scope.curPage + 1) * 3) - 3;
                var items1 = (($scope.curPage + 1) * 3);

                $.each($scope.datalists, function (index, value) {
                    if (contador < items1) {
                        value[contador];
                        value.Seleccionado = $scope.SeleccionTodos;
                        contador++;
                    }
                });
            };

            //Función 1 para filtrar la tabla
            $scope.Filter = function (e) {

                var Busqueda = $("#Buscar").val();
                var exp = new RegExp(Busqueda);
                if (Busqueda == "") {
                    CentroService.ConsultarCentros().then(function (response) {
                        if (response.data.success == true) {
                            $scope.datalists = response.data.datos;
                            $scope.ListaCompleta = response.data.datos;
                        }
                    });
                }
                var centro = [];
                $scope.datalists = $scope.ListaCompleta;
                centro = $scope.datalists.filter(function (item) {

                    if (exp.test(item.Parametro6.toLowerCase()) || exp.test(item.Parametro6.toUpperCase())) {
                        return item;
                    }

                    else if (exp.test(item.Parametro4.toLowerCase()) || exp.test(item.Parametro4.toUpperCase())) {
                        return item;
                    }

                    else if (exp.test(item.Parametro3.toLowerCase()) || exp.test(item.Parametro3.toUpperCase())) {
                        return item;
                    }
                    else if (exp.test(item.Parametro2.toLowerCase()) || exp.test(item.Parametro2.toUpperCase())) {
                        return item;
                    }
                });
                $scope.datalists = centro;
                //Variable para setear la paginación 
                $scope.curPage = 0;
            };

            //Funcion para Cambiar estado de un centro
            $scope.CambiarEstadoSeleccionados = function () {
                var UsariosBorrar = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });

                if (UsariosBorrar.length == 0) {
                    swal("¡Ten cuidado!", "Primero debes elegir el centro que deseas inhabilitar.");
                } else {

                    $("#modalInhabilitar").modal("show");
                }
            }

            //Funcion para eliminar un centro
            $scope.Eliminar = function () {
                var BorrarCentro = $scope.datalists.filter(function (item) {
                    return item.Seleccionado === true;
                });
                CentroService.EliminarCentroo(BorrarCentro).then(function (response) {
                    if (response.data.success == true && response.data.respuesta == true) {
                        CentroService.ConsultarCentros().then(function (response) {
                            if (response.data.success == true) {
                                $scope.datalists = response.data.datos;
                                $scope.ListaCompleta = response.data.datos;
                            }
                        })
                        swal("Inhabilitar", "Se inhabilitaron la siguiente cantidad de registros " + response.data.contadorTrue, "warning")
                    } else {
                        swal("Inhabilitar", "No se puedieron elimanar la siguiente cantidad de registros " + response.data.contadorFalse + " porque estan asociados a otras tablas.", "warning")
                    }
                });
            };




            //VISITA*******************************************************************************************************

            $scope.AbrirModalVisita = function () {
                $("#ModalVisita").modal("show");
                $scope.VaciarCampos();
            };


            $scope.Visita = {
                IdVisita: "",
                TipoVisita: "",
                HoraInicial: "",
                HoraFinal:"",
                Observaciones: "",
                IdMotivoVisita: ""
            };

            $scope.MotivoVisita = {
                IdMotivoVisita: "",
                Nombre: "",
                AccionCorrectiva: "",
            };



        }]);