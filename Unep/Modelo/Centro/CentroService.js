﻿ProgramacionApp.factory('CentroService',
    ['$http', '$rootScope', '$routeParams',
    function ($http, $rootScope, $routeParams) {
        var service = {};

        service.GuardarCentrooo = function (Centro1, callback) {

            return $http({
                method: 'POST',
                url: URLServices + "Centro/GuardarCentros/",
                data: Centro1
            })

        };

        service.IdCentro = function (callback) {
            debugger;


            return $http({
                method: 'GET',
                url: URLServices + "Centro/IdCentro/"
            })

        };

        service.ConsultarCentros = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Centro/ConsultarCentros/"
            })

        };

        service.ModificarCentrooo = function (centro, callback) {

            Item = {
                Parametro1: centro[0].Parametro1
            };

            return $http({
                method: 'POST',
                url: URLServices + "Centro/ModificarCentros/",
                data: Item
            })

        };

        service.GuardarModificacionCentrooo = function (Centro1, callback) {
            return $http({
                method: 'POST',
                url: URLServices + "Centro/GuardarModificacion/",
                data: Centro1
            })

    
        };

        service.EliminarCentroo = function (Centro, callback) {

            var Item = {
                Parametros: []
            }
            $.each(Centro, function (index, value) {
                Item.Parametros.push(
                    {
                        Parametro1: value.Parametro1
                    })
            });

            return $http({
                method: 'POST',
                url: URLServices + "Centro/EliminarCentro/",
                data: Item
            })


        };

        service.ConsultarZona = function (callback) {

            return $http({
                method: 'POST',
                url: URLServices + "Centro/ConsultarZonas/"
            })


        };

        service.ConsultarZonaxRegional = function (IdRegional, callback) {

            Item = {
                Parametro1: IdRegional,
            };
            return $http({
                method: 'POST',
                url: URLServices + "Centro/ConsultarZonaXRegionales/",
                data: Item
            })

        };

        service.ConsultarCentrosIDRegional = function (IdRegional, callback) {

            Item = {
                Parametro1: IdRegional,
            };
            return $http({
                method: 'POST',
                url: URLServices + "Centro/ConsultarCentroXRegionales/",
                data: Item
            })

        };

        service.ConsultarRegionales = function (IdZona, callback) {
            Item = {
                Parametro1: IdZona
            };

            return $http({
                method: 'POST',
                url: URLServices + "Centro/ConsultarRegionales/",
                data: Item
            })

        };

        //Visita*****************


        service.ConsultarVisitas = function (callback) {
            return $http({
                method: 'GET',
                url: URLServices + "Visita/ConsultarVisita/"
            })
        };


        return service;
    }]);


