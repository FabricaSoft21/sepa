﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class EmpresasDT
    {
        [Key]
        public int IdEmpresa { get; set; }

     
        public string NitEmpresa { get; set; }

        public string NombreEmpresa { get; set; }

        
        public string Direccion { get; set; }

    
        public string Telefono { get; set; }

 
        public string Correo { get; set; }

        public string NombreCoformador { get; set; }

      
        public string CargoCoformador { get; set; }

        public string TelefonoCoformador { get; set; }

   
        public string CorreoCoformador { get; set; }
      
  
        public int IdSede { get; set; }

        public long Codigo { get; set; }
    
        public string Estado { get; set; }
    }
}
