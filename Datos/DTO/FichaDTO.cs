﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class FichaDTO
    {

        public int IdFicha { get; set; }

        public string IdentificadorFicha { get; set; }

        public string Estado { get; set; }

        public string Jornada { get; set; }

        public string TipoRespuesta { get; set; }

        public DateTime FechaInicioFicha { get; set; }

        public DateTime FechaFinFicha { get; set; }

        public string EtapaFicha { get; set; }

        public string ModalidadFormacion { get; set; }

        public int? AprendicesActivos { get; set; }

        public DateTime? FechaLimite { get; set; }

        public DateTime? FechaMomentoDos { get; set; }

        public DateTime? FechaMomentoTres { get; set; }

        public DateTime? FechaInicioProductiva { get; set; }

        public bool CapacitacionSGVA { get; set; }

        public int IdDetalle { get; set; }

        public int TotalAprendices { get; set; }

        public string NumeroMesesEp { get; set; }

        public int IdPrograma { get; set; }

        public string CodigoPrograma { get; set; }

        public int IdCentro { get; set; }

        public string Codigo { get; set; }

        public int IdCoordinacion { get; set; }

        public string CodigoArea { get; set; }

        public int IdArea { get; set; }

        public string NumeroMesesEl { get; set; }
    }
}
