﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class RegistroError
    {
        public string NumeroDocumento { get; set; }

        public string Nombres { get; set; }

        public string Descripcion { get; set; }

    }
}
