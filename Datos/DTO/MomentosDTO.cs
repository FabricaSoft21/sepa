﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
   public class MomentosDTO
    {

        public int IdMomento { get; set; }


        public string MomentoUno { get; set; }

        public string MomentoDos { get; set; }

        public DateTime FechaUno { get; set; }

        public DateTime FechaDos { get; set; }

        public int IdFicha { get; set; }
    }
}
