﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class AprendizDTO
    {
        public string IdAprendiz { get; set; }

        public string NumeroDocumento { get; set; }

        public string TipoDocumento { get; set; }

        public string Nombres { get; set; }

        public string Apellidos { get; set; }

        public string Correo { get; set; }

        public string Telefono { get; set; }

        public string PoblacionDPS { get; set; }

        public string IdentificadorFicha { get; set; }

    }
}
