﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class InstructorDTO
    {
        public int IdInstructor { get; set; }
        
        public string Nombres { get; set; }
        
        public string Apellidos { get; set; }
        
        public string NumeroDocumento { get; set; }

        public string TipoDocumento { get; set; }

        public string Correo { get; set; }
        
        public bool Estado { get; set; }

        public string PicoPlaca { get; set; }

        public string Telefono { get; set; }

        public int IdUsuario { get; set; }

        public int IdDetalle { get; set; }

        public int IdCentro { get; set; }

        public int Idcordi { get; set; }

        public int IdArea { get; set; }

    }
}
