﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class Gestion
    {

        #region aprendiz
        public int IdAprendiz { get; set; }

        public string TipoDocumento { get; set; }

        public string NumeroDocumento { get; set; }

        public string Nombres { get; set; }

        public string PrimerApellido { get; set; }

        public string SegundoApellido { get; set; }

        public string Correo { get; set; }

        public string Telefono { get; set; }

        public bool PoblacionDPS { get; set; }

        public int IdFicha { get; set; }

        public int IdGestion { get; set; }
        #endregion

        #region Gestion
        public string Arl { get; set; }
        public string EmpresaSedido { get; set; }

      
        public string DescripcionSedido { get; set; }


        public int IdAlternativa { get; set; }

        public int Idsede { get; set; }


        public string FechaInicioProductiva { get; set; }
        #endregion
    }
}
