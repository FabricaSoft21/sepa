﻿namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Estado")]
    public partial class Estado
    {
        [Key]
        public int IdEstado { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

    }

}
