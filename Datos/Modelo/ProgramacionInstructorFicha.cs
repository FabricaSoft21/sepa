namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProgramacionInstructorFicha")]
    public partial class ProgramacionInstructorFicha
    {
        [Key]
        public int IdProgramacion { get; set; }

        [StringLength(20)]
        public string RealizoMomentos { get; set; }

        [StringLength(100)]
        public string Descripcion { get; set; }

        public int IdFicha { get; set; }

        public int IdInstructor { get; set; }

        public virtual Ficha Ficha { get; set; }

        public virtual Instructor Instructor { get; set; }
    }
}
