namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetalleCentrosAreasCoordinacion")]
    public partial class DetalleCentrosAreasCoordinacion
    {
        [Key]
        public int IdlDetalle { get; set; }

        public int IdCentro { get; set; }

        public int IdPrograma { get; set; }

        public int IdCoordinacion { get; set; }

        public int IdArea { get; set; }

    }
}
