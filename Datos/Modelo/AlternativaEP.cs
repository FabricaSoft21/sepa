namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AlternativaEP")]
    public partial class AlternativaEP
    {
   

        [Key]
        public int IdAlternativa { get; set; }

        [StringLength(45)]
        public string NombreAlternativa { get; set; }

   
    }
}
