namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GestionIndividual")]
    public partial class GestionIndividual
    {
        [Key]
        public int IdGestion { get; set; }

        [Required]
        [StringLength(20)]
        public string Arl { get; set; }

        [StringLength(45)]
        public string EmpresaSedido { get; set; }

        [StringLength(45)]
        public string DescripcionSedido { get; set; }

        public int IdAprendiz { get; set; }

        public int IdAlternativa { get; set; }

        public int Idsede { get; set; }

        public string FechaInicioProductiva { get; set; }

    
    }
}
