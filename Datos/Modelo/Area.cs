namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Area")]
    public partial class Area
    {

        [Key]
        public int IdArea { get; set; }


        public string CodigoArea { get; set; }

        [StringLength(45)]
        public string Nombre { get; set; }

        [StringLength(45)]
        public string Descripcion { get; set; }

  
    }
}
