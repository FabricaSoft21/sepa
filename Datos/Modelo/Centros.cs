namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Centros
    {
     

        [Key]
        public int IdCentro { get; set; }

        [StringLength(100)]
        public string NombreCentro { get; set; }

        [StringLength(200)]
        public string Direccion { get; set; }

        public int IdRegional { get; set; }

        public string Codigo { get; set; }

    }
}
