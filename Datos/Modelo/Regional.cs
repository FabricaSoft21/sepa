namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Regional")]
    public partial class Regional
    {
        [Key]
        public int IdRegional { get; set; }

        [StringLength(100)]
        public string NombreRegional { get; set; }

        public int IdZona { get; set; }

        public virtual Zona Zona { get; set; }
    }
}
