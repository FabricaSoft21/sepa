﻿
namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetalleEstadoAprendiz")]
    public class DetalleEstadoAprendiz
    {
         [Key]
        public int IdDetalleAprendiz { get; set; }

        public int IdEstado { get; set; }

        public int IdAprendiz { get; set; }

        public string Descripcion { get; set; }

    }
}
