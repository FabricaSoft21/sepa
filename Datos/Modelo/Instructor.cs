namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Instructor")]
    public partial class Instructor
    {

        [Key]
        public int IdInstructor { get; set; }

        [Required]
        [StringLength(45)]
        public string TipoDocumento { get; set; }

        public int NumeroDocumento { get; set; }

        [Required]
        [StringLength(45)]
        public string Nombres { get; set; }

        [Required]
        [StringLength(45)]
        public string Apellidos { get; set; }

        public int Telefono { get; set; }

        [Required]
        [StringLength(45)]
        public string Correo { get; set; }

        public bool Estado { get; set; }

        [Required]
        [StringLength(45)]
        public string PicoPlaca { get; set; }

        public int IdDetalleArea { get; set; }

        public int IdUsuario { get; set; }

        public bool EnvioCorreo { get; set; }


    }
}
