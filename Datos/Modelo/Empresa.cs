namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Empresa")]
    public partial class Empresa
    {
   

        [Key]
        public int IdEmpresa { get; set; }

        [StringLength(100)]
        public string NitEmpresa { get; set; }

        [StringLength(100)]
        public string NombreEmpresa { get; set; }

        [StringLength(200)]
        public string Direccion { get; set; }

        [StringLength(100)]
        public string Telefono { get; set; }

        [StringLength(200)]
        public string Correo { get; set; }

        [StringLength(100)]
        public string NombreCoformador { get; set; }

        [StringLength(100)]
        public string CargoCoformador { get; set; }

        [StringLength(100)]
        public string TelefonoCoformador { get; set; }

        [StringLength(100)]
        public string CorreoCoformador { get; set; }

        [StringLength(20)]
        public string Estado { get; set; }


    }
}
