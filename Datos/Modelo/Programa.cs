namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Programa")]
    public partial class Programa
    {
  

        [Key]
        public int IdPrograma { get; set; }

        public string CodigoPrograma { get; set; }

        [StringLength(200)]
        public string NombrePrograma { get; set; }

        [StringLength(45)]
        public string Nivel { get; set; }


        public int IdArea { get; set; }

    }
}
