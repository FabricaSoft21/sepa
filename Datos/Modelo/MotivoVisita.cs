namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MotivoVisita")]
    public partial class MotivoVisita
    {
        

        [Key]
        public int IdMotivoVisita { get; set; }

        [StringLength(45)]
        public string Nombre { get; set; }

        [StringLength(200)]
        public string AccionCorrectiva { get; set; }


    }
}
