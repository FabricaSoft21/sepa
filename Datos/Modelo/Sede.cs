namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sede")]
    public partial class Sede
    {
      

        [Key]
        public int IdSede { get; set; }

        [StringLength(100)]
        public string Codigo { get; set; }

        [StringLength(100)]
        public string Nombre { get; set; }

        [StringLength(100)]
        public string Telefono { get; set; }


        [StringLength(200)]
        public string Direccion { get; set; }

        [StringLength(100)]
        public string NombreCoformador { get; set; }

        [StringLength(100)]
        public string CargoCoformador { get; set; }

        [StringLength(100)]
        public string CorreoCoformador { get; set; }

        [StringLength(100)]
        public string TelefonoCoformador { get; set; }

        public int IdEmpresa { get; set; }

        public string Estado { get; set;}

    
    }
}
