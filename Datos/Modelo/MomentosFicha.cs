﻿namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MomentosFicha")]
    public partial class MomentosFicha
    {

        [Key]
        public int IdMomento { get; set; }


        public string Momento { get; set; }


        public DateTime Fecha { get; set; }

        public int IdFicha { get; set; }
    }
}
