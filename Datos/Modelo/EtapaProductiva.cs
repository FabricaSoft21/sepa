namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EtapaProductiva")]
    public partial class EtapaProductiva
    {
        [Key]
        public int IdEtapaProductiva { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Evaluacion1 { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Evaluacion2 { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Evaluacion3 { get; set; }

        public int IdFicha { get; set; }

        public virtual Ficha Ficha { get; set; }
    }
}
