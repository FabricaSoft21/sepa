namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Zona")]
    public partial class Zona
    {


        [Key]
        public int IdZona { get; set; }

        [StringLength(100)]
        public string NombreZona { get; set; }

    }
}
