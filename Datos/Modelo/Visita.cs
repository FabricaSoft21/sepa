namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Visita")]
    public partial class Visita
    {
        

        [Key]
        public int IdVisita { get; set; }

        [StringLength(45)]
        public string TipoVisita { get; set; }

        public TimeSpan? HoraInicial { get; set; }

        public TimeSpan? HoraFinal { get; set; }

        [StringLength(200)]
        public string Observaciones { get; set; }

        public int IdMotivoVisita { get; set; }

        public int IdGestion { get; set; }

    }
}
