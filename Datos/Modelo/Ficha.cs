namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ficha")]
    public partial class Ficha
    {

        [Key]
        public int IdFicha { get; set; }

        [Required]
        [StringLength(45)]
        public string IdentificadorFicha { get; set; }

        [StringLength(45)]
        public string Estado { get; set; }

        [StringLength(45)]
        public string Jornada { get; set; }

        [StringLength(45)]
        public string TipoRespuesta { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaInicioFicha { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaFinFicha { get; set; }

        [StringLength(45)]
        public string EtapaFicha { get; set; }

        [StringLength(45)]
        public string ModalidadFormacion { get; set; }

        public int? AprendicesActivos { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaLimite { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaMomentoDos { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaMomentoTres { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaInicioProductiva { get; set; }

        public bool CapacitacionSGVA { get; set; }

        public int IdDetalle { get; set; }

        public int TotalAprendices { get; set; }

        public string NumeroMesesEp { get; set; }


        public string NumeroMesesEl { get; set; }

    }
}
