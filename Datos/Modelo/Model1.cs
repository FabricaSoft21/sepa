namespace Datos.Modelo
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<AlternativaEP> AlternativaEP { get; set; }
        public virtual DbSet<Aprendiz> Aprendiz { get; set; }
        public virtual DbSet<Area> Area { get; set; }
        public virtual DbSet<Centros> Centros { get; set; }
        public virtual DbSet<Coordinacion> Coordinacion { get; set; }
        public virtual DbSet<DetalleCentrosAreasCoordinacion> DetalleCentrosAreasCoordinacion { get; set; }
        public virtual DbSet<DetalleFichaEmpresa> DetalleFichaEmpresa { get; set; }
        public virtual DbSet<DetalleEstadoAprendiz> DetalleEstadoAprendiz { get; set; }
        public virtual DbSet<Estado> Estado { get; set; }
        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<EtapaProductiva> EtapaProductiva { get; set; }
        public virtual DbSet<Ficha> Ficha { get; set; }
        public virtual DbSet<GestionIndividual> GestionIndividual { get; set; }
        public virtual DbSet<Instructor> Instructor { get; set; }
        public virtual DbSet<MotivoVisita> MotivoVisita { get; set; }
        public virtual DbSet<Programa> Programa { get; set; }
        public virtual DbSet<ProgramacionInstructorFicha> ProgramacionInstructorFicha { get; set; }
        public virtual DbSet<Regional> Regional { get; set; }
        public virtual DbSet<Sede> Sede { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<Visita> Visita { get; set; }
        public virtual DbSet<Zona> Zona { get; set; }
        public virtual DbSet<MomentosFicha> MomentosFicha { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<AlternativaEP>()
            //    .Property(e => e.NombreAlternativa)
            //    .IsUnicode(false);

            //modelBuilder.Entity<AlternativaEP>()
            //    .HasMany(e => e.GestionIndividual)
            //    .WithRequired(e => e.AlternativaEP)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Aprendiz>()
            //    .Property(e => e.TipoDocumento)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Aprendiz>()
            //    .Property(e => e.NumeroDocumento)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Aprendiz>()
            //    .Property(e => e.Nombres)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Aprendiz>()
            //    .Property(e => e.PrimerApellido)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Aprendiz>()
            //    .Property(e => e.SegundoApellido)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Aprendiz>()
            //    .Property(e => e.Correo)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Aprendiz>()
            //    .Property(e => e.Telefono)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Aprendiz>()
            //    .Property(e => e.Estado)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Aprendiz>()
            //    .HasMany(e => e.GestionIndividual)
            //    .WithRequired(e => e.Aprendiz)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Area>()
            //    .Property(e => e.Codigo)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Area>()
            //    .Property(e => e.Nombre)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Area>()
            //    .Property(e => e.Descripcion)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Area>()
            //    .HasMany(e => e.DetalleCentrosAreasCoordinacion)
            //    .WithRequired(e => e.Area)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Area>()
            //    .HasMany(e => e.Instructor)
            //    .WithRequired(e => e.Area)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Centros>()
            //    .Property(e => e.NombreCentro)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Centros>()
            //    .Property(e => e.Direccion)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Centros>()
            //    .HasMany(e => e.DetalleCentrosAreasCoordinacion)
            //    .WithRequired(e => e.Centros)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Coordinacion>()
            //    .Property(e => e.TipoDocumento)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Coordinacion>()
            //    .Property(e => e.NumeroDocumento)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Coordinacion>()
            //    .Property(e => e.Nombres)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Coordinacion>()
            //    .Property(e => e.Apellidos)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Coordinacion>()
            //    .Property(e => e.Telefono)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Coordinacion>()
            //    .Property(e => e.Correo)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Coordinacion>()
            //    .HasMany(e => e.DetalleCentrosAreasCoordinacion)
            //    .WithRequired(e => e.Coordinacion)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<DetalleFichaEmpresa>()
            //    .HasMany(e => e.Ficha1)
            //    .WithRequired(e => e.DetalleFichaEmpresa1)
            //    .HasForeignKey(e => e.IdDetalle)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Empresa>()
            //    .Property(e => e.NitEmpresa)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Empresa>()
            //    .Property(e => e.NombreEmpresa)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Empresa>()
            //    .Property(e => e.Direccion)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Empresa>()
            //    .Property(e => e.Telefono)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Empresa>()
            //    .Property(e => e.Correo)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Empresa>()
            //    .Property(e => e.NombreCoformador)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Empresa>()
            //    .Property(e => e.CargoCoformador)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Empresa>()
            //    .Property(e => e.TelefonoCoformador)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Empresa>()
            //    .Property(e => e.CorreoCoformador)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Empresa>()
            //    .HasMany(e => e.DetalleFichaEmpresa)
            //    .WithRequired(e => e.Empresa)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Empresa>()
            //    .HasMany(e => e.Sede)
            //    .WithRequired(e => e.Empresa)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Ficha>()
            //    .Property(e => e.IdentificadorFicha)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Ficha>()
            //    .Property(e => e.Estado)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Ficha>()
            //    .Property(e => e.Jornada)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Ficha>()
            //    .Property(e => e.TipoRespuesta)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Ficha>()
            //    .Property(e => e.EtapaFicha)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Ficha>()
            //    .Property(e => e.ModalidadFormacion)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Ficha>()
            //    .HasMany(e => e.Aprendiz)
            //    .WithRequired(e => e.Ficha)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Ficha>()
            //    .HasMany(e => e.DetalleFichaEmpresa)
            //    .WithRequired(e => e.Ficha)
            //    .HasForeignKey(e => e.IdFicha)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Ficha>()
            //    .HasMany(e => e.EtapaProductiva)
            //    .WithRequired(e => e.Ficha)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Ficha>()
            //    .HasMany(e => e.ProgramacionInstructorFicha)
            //    .WithRequired(e => e.Ficha)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<GestionIndividual>()
            //    .Property(e => e.Arl)
            //    .IsUnicode(false);

            //modelBuilder.Entity<GestionIndividual>()
            //    .Property(e => e.EmpresaSedido)
            //    .IsUnicode(false);

            //modelBuilder.Entity<GestionIndividual>()
            //    .Property(e => e.DescripcionSedido)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Instructor>()
            //    .Property(e => e.TipoDocumento)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Instructor>()
            //    .Property(e => e.Nombres)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Instructor>()
            //    .Property(e => e.Apellidos)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Instructor>()
            //    .Property(e => e.Correo)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Instructor>()
            //    .Property(e => e.PicoPlaca)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Instructor>()
            //    .HasMany(e => e.ProgramacionInstructorFicha)
            //    .WithRequired(e => e.Instructor)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<MotivoVisita>()
            //    .Property(e => e.Nombre)
            //    .IsUnicode(false);

            //modelBuilder.Entity<MotivoVisita>()
            //    .Property(e => e.AccionCorrectiva)
            //    .IsUnicode(false);

            //modelBuilder.Entity<MotivoVisita>()
            //    .HasMany(e => e.Visita)
            //    .WithRequired(e => e.MotivoVisita)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Programa>()
            //    .Property(e => e.CodigoPrograma)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Programa>()
            //    .Property(e => e.NombrePrograma)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Programa>()
            //    .Property(e => e.Nivel)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Programa>()
            //    .HasMany(e => e.DetalleCentrosAreasCoordinacion)
            //    .WithRequired(e => e.Programa)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<ProgramacionInstructorFicha>()
            //    .Property(e => e.RealizoMomentos)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProgramacionInstructorFicha>()
            //    .Property(e => e.Descripcion)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Regional>()
            //    .Property(e => e.NombreRegional)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sede>()
            //    .Property(e => e.Nombre)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sede>()
            //    .Property(e => e.Telefono)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sede>()
            //    .Property(e => e.Direccion)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sede>()
            //    .Property(e => e.NombreCoformador)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sede>()
            //    .Property(e => e.CargoCoformador)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sede>()
            //    .Property(e => e.CorreoCoformador)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sede>()
            //    .Property(e => e.TelefonoCoformador)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sede>()
            //    .HasMany(e => e.GestionIndividual)
            //    .WithRequired(e => e.Sede)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Usuario>()
            //    .Property(e => e.NombreUsuario)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Usuario>()
            //    .Property(e => e.Password)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Usuario>()
            //    .Property(e => e.TipoUsuario)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Usuario>()
            //    .HasMany(e => e.Aprendiz)
            //    .WithRequired(e => e.Usuario)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Usuario>()
            //    .HasMany(e => e.Coordinacion)
            //    .WithRequired(e => e.Usuario)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Usuario>()
            //    .HasMany(e => e.Instructor)
            //    .WithRequired(e => e.Usuario)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Visita>()
            //    .Property(e => e.TipoVisita)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Visita>()
            //    .Property(e => e.HoraInicial)
            //    .HasPrecision(0);

            //modelBuilder.Entity<Visita>()
            //    .Property(e => e.HoraFinal)
            //    .HasPrecision(0);

            //modelBuilder.Entity<Visita>()
            //    .Property(e => e.Observaciones)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Visita>()
            //    .HasMany(e => e.GestionIndividual)
            //    .WithRequired(e => e.Visita)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Zona>()
            //    .Property(e => e.NombreZona)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Zona>()
            //    .HasMany(e => e.Regional)
            //    .WithRequired(e => e.Zona)
            //    .WillCascadeOnDelete(false);
        }
    }
}
