namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Coordinacion")]
    public partial class Coordinacion
    {
   

        [Key]
        public int IdCoordinacion { get; set; }

        [StringLength(45)]
        public string TipoDocumento { get; set; }

        [StringLength(45)]
        public string NumeroDocumento { get; set; }

        [StringLength(45)]
        public string Nombres { get; set; }

        [StringLength(45)]
        public string Apellidos { get; set; }

        [StringLength(45)]
        public string Telefono { get; set; }

        [StringLength(45)]
        public string Correo { get; set; }

        public int IdUsuario { get; set; }


    }
}
