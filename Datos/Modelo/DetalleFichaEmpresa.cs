namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetalleFichaEmpresa")]
    public partial class DetalleFichaEmpresa
    {
       

        [Key]
        public int IdDetalle { get; set; }

        public int IdFicha { get; set; }

        public int IdEmpresa { get; set; }



    }
}
