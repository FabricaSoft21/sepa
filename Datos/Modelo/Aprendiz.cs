namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Aprendiz")]
    public partial class Aprendiz
    {
   

        [Key]
        public int IdAprendiz { get; set; }

        [StringLength(45)]
        public string TipoDocumento { get; set; }

        [StringLength(20)]
        public string NumeroDocumento { get; set; }

        [StringLength(45)]
        public string Nombres { get; set; }

        [StringLength(45)]
        public string PrimerApellido { get; set; }

        [StringLength(45)]
        public string SegundoApellido { get; set; }

        [StringLength(50)]
        public string Correo { get; set; }

        [StringLength(20)]
        public string Telefono { get; set; }

        public bool PoblacionDPS { get; set; }

        public int IdFicha { get; set; }

        public int IdUsuario { get; set; }

    }
}
