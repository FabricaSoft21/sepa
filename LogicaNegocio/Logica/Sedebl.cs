﻿using Datos.DTO;
using Datos.Modelo;
using LogicaNegocio.Enumeraciones;
using LogicaNegocio.Mail;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace LogicaNegocio.Logica
{
    public class Sedebl
    {
        Model1 entry = new Model1();

        public List<Sede> ConsultarSede(int id)
        {

            var lista = entry.Database.SqlQuery<Sede>("ConsultarSede @id",
                new SqlParameter("@id", id)).ToList();

            return lista;

        }

        public bool RegistrarPersona(Sede osede)
        {
            bool respuesta = false;
            try
            {

                var dato = (from i in entry.Sede
                            where i.IdEmpresa == osede.IdEmpresa
                            select i).Count();


                osede.Codigo = osede.Codigo + "/" +( dato + 1);
                var sedeexist = entry.Sede.Where(item => item.Codigo == osede.Codigo).FirstOrDefault();

                if (sedeexist == null)
                {
                    entry.Sede.Add(osede);
                    entry.SaveChanges();
                    respuesta = true;
                }
                else
                {

                    respuesta = false;
                }




            }
            catch
            {
                respuesta = false;

            }

            return respuesta;
        }


        public Sede SedeId(int id)
        {


            var sed = (from i in entry.Sede
                       where i.IdSede == id
                       select i).FirstOrDefault();

            return sed;
        }


        public void GuardarModificacionSede(Sede oSede)
        {
            Model1 entity = new Model1();

            var Item = (from i in entity.Sede

                        where i.IdEmpresa == oSede.IdEmpresa
                        select i).FirstOrDefault();

       
            Item.Codigo = oSede.Codigo;
            Item.Nombre = oSede.Nombre;
            Item.Telefono = oSede.Telefono;
            Item.Direccion = oSede.Direccion;
            Item.NombreCoformador = oSede.NombreCoformador;
            Item.TelefonoCoformador = oSede.TelefonoCoformador;
            Item.CargoCoformador = oSede.CargoCoformador;
            Item.CorreoCoformador = oSede.CorreoCoformador;
            Item.IdEmpresa = oSede.IdEmpresa;
            Item.Estado = oSede.Estado;
            entity.SaveChanges();

        }


        public bool inhabilitar(int sede)
        {
            var esta = false;
            Model1 entity = new Model1();

            var item = (from i in entity.Sede
                        where i.IdSede == sede
                        select i.Estado).FirstOrDefault();

            var item1 = (from i in entity.Sede
                        where i.IdSede == sede
                        select i).FirstOrDefault();

            

            if(item == "inactivo")
            {
                item1.Estado = "Activo";
                esta= true;
            }
            else
            {
                item1.Estado = "inactivo";
                esta = false;
            }


            entity.SaveChanges();
            return esta;
        }

        public List<Sede> ConsultarSedeinactiva(int id)
        {
            var lista = entry.Database.SqlQuery<Sede>("ConsultarSedeinactiva @id",
                new SqlParameter("@id",id)).ToList();

            return lista;



        }
        
    }

}
