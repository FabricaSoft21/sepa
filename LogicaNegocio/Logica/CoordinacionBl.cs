﻿using Datos.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogicaNegocio.Mail;
using LogicaNegocio.Enumeraciones;
using Datos.DTO;


namespace LogicaNegocio.Logica
{
    public class CoordinacionBl
    {
        Model1 entity = new Model1();

        public List<Coordinacion> ConsultarCoordinacion()
        {
            var Datoscoor = (from i in entity.Coordinacion
                             select i).ToList();
            return Datoscoor;
        }

        public int GuardarCoordinacion(CoordinacionDTO oCoordinacion)
        {
            Model1 entity = new Model1();

            var coordinacion = (from i in entity.DetalleCentrosAreasCoordinacion
                                where i.IdCoordinacion == oCoordinacion.IdCoordinacion
                                select i.IdCoordinacion).Count();

            var coordinador = (from i in entity.Coordinacion
                               where i.NumeroDocumento == oCoordinacion.NumeroDocumento
                               select i).FirstOrDefault();

            if (coordinador == null & coordinacion == 0)
            {
                Usuario oUsuario = new Usuario();
                oUsuario.NombreUsuario = oCoordinacion.NumeroDocumento;
                var Encriptar = SecurityEncode.SecurityEncode.Encriptar(oCoordinacion.NumeroDocumento);
                oUsuario.Password = Encriptar;
                oUsuario.TipoUsuario = (int)TipoUsuario.Coordinador;
                entity.Usuario.Add(oUsuario);
                entity.SaveChanges();

                Coordinacion Cordi = new Coordinacion();
                Cordi.Nombres = oCoordinacion.Nombres;
                Cordi.Apellidos = oCoordinacion.Apellidos;
                Cordi.IdUsuario = oUsuario.IdUsuario;
                Cordi.TipoDocumento = oCoordinacion.TipoDocumento;
                Cordi.NumeroDocumento = oCoordinacion.NumeroDocumento;
                Cordi.Telefono = oCoordinacion.Telefono;
                Cordi.Correo = oCoordinacion.Correo;
                entity.Coordinacion.Add(Cordi);
                entity.SaveChanges();

                DetalleCentrosAreasCoordinacion Detalle = new DetalleCentrosAreasCoordinacion();
                Detalle.IdCentro = oCoordinacion.IdCentro;
                Detalle.IdPrograma = oCoordinacion.IdPrograma;
                Detalle.IdArea = oCoordinacion.IdArea;
                Detalle.IdCoordinacion = Cordi.IdCoordinacion;
                entity.DetalleCentrosAreasCoordinacion.Add(Detalle);
                entity.SaveChanges();


                var Asunto = "Nuevo Usuario";
                var Plantilla = "Usuario : " + oUsuario.NombreUsuario + "<br/> Contraseña: " + oUsuario.NombreUsuario;
                SendMail.SendMailMessage(Asunto, Plantilla, oCoordinacion.Correo);
                return 1;
            }
            else
            {
                return 2;
            }
        }

        public void EliminarCoordinacion(int IdCoordinacion)
        {

            var Deta = (from i in entity.DetalleCentrosAreasCoordinacion
                        where i.IdCoordinacion == IdCoordinacion
                        select i).FirstOrDefault();
          


            var Datos = (from i in entity.Coordinacion
                         where i.IdCoordinacion == IdCoordinacion
                         select i).FirstOrDefault();
          

      

            var Usuario = (from i in entity.Usuario
                           where i.NombreUsuario == Datos.NumeroDocumento
                           select i).FirstOrDefault();


            entity.DetalleCentrosAreasCoordinacion.Remove(Deta);
            entity.SaveChanges();

            entity.Coordinacion.Remove(Datos);
            entity.SaveChanges();

            entity.Usuario.Remove(Usuario);
            entity.SaveChanges();

        
      

        }

    

        public CoordinacionDTO ConsultarIds(int IdCoordinacion)
        {
            CoordinacionDTO Cordi = new CoordinacionDTO();

            var Detalle = (from c in entity.Coordinacion
                           join d in entity.DetalleCentrosAreasCoordinacion on c.IdCoordinacion equals d.IdCoordinacion
                           join p in entity.Programa on d.IdPrograma equals p.IdPrograma
                           join a in entity.Area on p.IdArea equals a.IdArea 
                           where c.IdCoordinacion == IdCoordinacion
                           select d).FirstOrDefault();


            var Dto = (from p in entity.Programa
                       where Detalle.IdPrograma == p.IdPrograma
                       select p).FirstOrDefault();

      

            Cordi.IdCoordinacion = Detalle.IdCoordinacion;
            Cordi.IdPrograma = Detalle.IdPrograma;
            Cordi.IdCentro = Detalle.IdCentro;
            Cordi.IdArea = Dto.IdArea;


            return Cordi;
        }

        public Coordinacion ConsultarCoordinacionId(int IdCoordinacion)
        {
            var Datos = (from i in entity.Coordinacion
                         where i.IdCoordinacion == IdCoordinacion
                         select i).FirstOrDefault();

            return Datos;
        }

        public void ActualizarRegistro(CoordinacionDTO oCoordinacion)
        {
            var Item = (from i in entity.Coordinacion
                        where i.IdCoordinacion == oCoordinacion.IdCoordinacion
                        select i).FirstOrDefault();

            var Deta = (from i in entity.DetalleCentrosAreasCoordinacion
                        where i.IdCoordinacion == oCoordinacion.IdCoordinacion
                        select i).FirstOrDefault();


            Deta.IdCentro = oCoordinacion.IdCentro;
            Deta.IdPrograma = oCoordinacion.IdPrograma;
            Deta.IdArea = oCoordinacion.IdArea;
            Item.NumeroDocumento = oCoordinacion.NumeroDocumento;
            Item.Nombres = oCoordinacion.Nombres;
            Item.Apellidos = oCoordinacion.Apellidos;
            Item.Telefono = oCoordinacion.Telefono;
            Item.Correo = oCoordinacion.Correo;
            Item.TipoDocumento = oCoordinacion.TipoDocumento;
            entity.SaveChanges();
        }
    }
}
