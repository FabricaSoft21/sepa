﻿using Datos.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.DTO;
namespace LogicaNegocio.Logica
{
    public class EmpresaBl

    {
        Model1 entity = new Model1();

        public bool GuardarEmpresa(EmpresasDT empresa)
        {

            var empresabd = entity.Empresa.Where(item => item.NitEmpresa == empresa.NitEmpresa).FirstOrDefault();

            if (empresabd == null)
            {
                try
                {
                    Empresa array = new Empresa();

                    Sede array1 = new Sede();

                    array.NitEmpresa = empresa.NitEmpresa;
                    array.NombreEmpresa = empresa.NombreEmpresa;
                    array.Direccion = empresa.Direccion;
                    array.Telefono = empresa.Telefono;
                    array.Correo = empresa.Correo;
                    array.NombreCoformador = empresa.NombreCoformador;
                    array.CargoCoformador = empresa.CargoCoformador;
                    array.Estado = empresa.Estado;
                    array.TelefonoCoformador = empresa.TelefonoCoformador;
                    array.CorreoCoformador = empresa.CorreoCoformador;
                    entity.Empresa.Add(array);
                    entity.SaveChanges();

                    var id = (from i in entity.Empresa
                              where i.NitEmpresa == empresa.NitEmpresa
                              select i.IdEmpresa).Single();


                    array1.Codigo = empresa.NitEmpresa;
                    array1.Nombre = empresa.NombreEmpresa;
                    array1.Direccion = empresa.Direccion;
                    array1.Telefono = empresa.Telefono;
                    array1.NombreCoformador = empresa.NombreCoformador;
                    array1.CargoCoformador = empresa.CargoCoformador;
                    array1.TelefonoCoformador = empresa.TelefonoCoformador;
                    array1.CorreoCoformador = empresa.CorreoCoformador;
                    array1.IdEmpresa = Convert.ToInt32(id);
                    array1.Estado = "Activo";
                    entity.Sede.Add(array1);
                    entity.SaveChanges();

                    return true;
                }
                catch (Exception e)
                {
                    throw (e);

                }
            }
            else
            {
                return false;
            }
        }


        public bool GuardarEmpresaArchivo(List<EmpresasDT> empresa)
        {          

            var exist = (from a in entity.Empresa
                         select a.NitEmpresa).ToList();
            entity.Configuration.AutoDetectChangesEnabled = false;
            for (var i = 0; i < empresa.Count; i++)
            {
                var ide = empresa[i].NitEmpresa.Trim();

                if (!exist.Contains(ide))
                {
                    Empresa array = new Empresa();
                    array.NitEmpresa = empresa[i].NitEmpresa.Trim();
                    array.NombreEmpresa = empresa[i].NombreEmpresa.Trim();
                    array.Direccion = empresa[i].Direccion.Trim();
                    array.Telefono = empresa[i].Telefono.Trim();
                    array.Correo = empresa[i].Correo.Trim();
                    array.Estado = "Activo";
                    array.NombreCoformador = empresa[i].NombreCoformador;
                    array.CargoCoformador = empresa[i].CargoCoformador;
                    array.TelefonoCoformador = empresa[i].TelefonoCoformador;
                    array.CorreoCoformador = empresa[i].CorreoCoformador;
                    exist.Add(empresa[i].NitEmpresa.Trim());
                    entity.Empresa.Add(array);
                    
                }
            }
            entity.SaveChanges();


            var ids = (from ie in entity.Empresa
                       select ie).ToList();
            var nit = (from ie in entity.Empresa
                       select ie.NitEmpresa).ToList();
            List<EmpresasDT> dt = new List<EmpresasDT>();
            for (var t = 0; t < ids.Count; t++)
            {
                dt.Add(new EmpresasDT() { Codigo = Int64.Parse(ids[t].NitEmpresa), NombreEmpresa = ids[t].NombreEmpresa });
            }

            for (var s = 0; s < empresa.Count; s++)
            {
                Sede array1 = new Sede();
                array1.Codigo = empresa[s].NitEmpresa;
                array1.Nombre = empresa[s].NombreEmpresa.Trim();
                array1.Direccion = empresa[s].Direccion.Trim();
                array1.Telefono = empresa[s].Telefono.Trim();
                //array1.NombreCoformador = empresa[i].NombreCoformador.Trim();
                //array1.CargoCoformador = empresa[i].CargoCoformador.Trim();
                //array1.TelefonoCoformador = empresa[i].TelefonoCoformador.Trim();
                //array1.CorreoCoformador = empresa[i].CorreoCoformador.Trim();
                if (nit.Contains(empresa[s].NitEmpresa.Trim()))
                {
                    for (var id = 0; id < ids.Count; id++)
                    {
                        if (ids[id].NitEmpresa == empresa[s].NitEmpresa.Trim())
                        {
                            array1.IdEmpresa = ids[id].IdEmpresa;

                            ids.Remove(ids[id]);
                            nit.Remove(empresa[s].NitEmpresa.Trim());
                            break;
                        }
                    }
                    array1.Estado = "Activo";
                    entity.Sede.Add(array1);
                }
            }

            entity.SaveChanges();

            return true;

        }


        public List<Empresa> ConsultarEmpresa()
        {

            var lista = entity.Database.SqlQuery<Empresa>("ConsultarEmpresas").ToList();

            return lista;
        }


        public List<Empresa> ConsultarEmpresainactiva()
        {
            var lista = entity.Database.SqlQuery<Empresa>("ConsultarEmpresasInactiva").ToList();

            return lista;
           
        }


        public Empresa EmpresaId(int id)
        {
            var Datos = (from i in entity.Empresa
                         where i.IdEmpresa == id
                         select i).FirstOrDefault();
            return Datos;
        }


        public void ActualizarRegistro(Empresa oEmpresa)
        {
            var Item = (from i in entity.Empresa
                        where i.NitEmpresa == oEmpresa.NitEmpresa
                        select i).FirstOrDefault();
            Item.NitEmpresa = oEmpresa.NitEmpresa;
            Item.NombreEmpresa = oEmpresa.NombreEmpresa;
            Item.Correo = oEmpresa.Correo;
            Item.Telefono = oEmpresa.Telefono;
            Item.Direccion = oEmpresa.Direccion;
            entity.SaveChanges();
        }


        public void EliminarEmpresa(int id)
        {

            var Datos = (from i in entity.Empresa
                         where i.IdEmpresa == id
                         select i).FirstOrDefault();

            if(Datos.Estado == "Inactivo")
            {
                Datos.Estado = "Activo";
            }
            else
            {
                Datos.Estado = "Inactivo";
            }

            entity.SaveChanges();
        }


    }
}
