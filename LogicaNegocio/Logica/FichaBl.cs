﻿using Datos.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogicaNegocio.Mail;
using LogicaNegocio.Enumeraciones;
using System.Timers;
using Datos.DTO;
using System.Data.SqlClient;


namespace LogicaNegocio.Logica
{
    public class FichaBl
    {
        Model1 entity = new Model1();

        public bool GuardarFicha(FichaDTO oFicha)
        {
            Ficha ficha = new Ficha();

            //Calcular fecha limite de la ficha
            DateTime Fecha = Convert.ToDateTime(oFicha.FechaFinFicha);
            DateTime FechaLimite;
            FechaLimite = Fecha.AddMonths(18);


            //Calcular inicio productivaa
            DateTime FechaProductiva = Convert.ToDateTime(oFicha.FechaFinFicha);
            DateTime FechaPro;


            FechaPro = FechaProductiva.AddMonths(-6);

            //Calcular momento 3 de la ficha
            DateTime FechaMomentoTres = FechaPro;
            DateTime FechaFinal;
            FechaFinal = FechaMomentoTres.AddDays(-15);

            //Calcular momento 2 de la ficha
            DateTime FechaInicio = Convert.ToDateTime(oFicha.FechaInicioFicha);
            DateTime FechaFin = FechaPro;
            Int32 FechaMomentoDos;
            DateTime Fechaaa;

            Int32 anios;
            Int32 meses;
            anios = (FechaFin.Year - FechaInicio.Year);
            meses = (FechaFin.Month - FechaInicio.Month);

            if (anios > 0)
            {
                anios = anios * 12;
                meses = meses + anios;
            }

            FechaMomentoDos = meses / 2;

            Fechaaa = FechaInicio.AddMonths(FechaMomentoDos);

            var Deta = (from i in entity.Ficha
                        where i.IdDetalle == oFicha.IdDetalle
                        select i).FirstOrDefault();

            var Detalle = (from i in entity.DetalleCentrosAreasCoordinacion
                           where i.IdCentro == oFicha.IdCentro || i.IdPrograma == oFicha.IdPrograma
                           select i.IdlDetalle).First();

            var Datos = (from i in entity.Ficha
                         where i.IdentificadorFicha == oFicha.IdentificadorFicha
                         select i).FirstOrDefault();

            if (Datos == null & Deta == null)
            {
                ficha.Jornada = oFicha.Jornada;
                ficha.TipoRespuesta = oFicha.TipoRespuesta;
                ficha.FechaInicioFicha = oFicha.FechaInicioFicha;
                ficha.FechaFinFicha = oFicha.FechaFinFicha;
                ficha.ModalidadFormacion = oFicha.ModalidadFormacion;
                ficha.TotalAprendices = oFicha.TotalAprendices;
                ficha.AprendicesActivos = 0;
                ficha.NumeroMesesEp = oFicha.NumeroMesesEp;
                ficha.NumeroMesesEl = oFicha.NumeroMesesEl;
                ficha.IdentificadorFicha = oFicha.IdentificadorFicha;
                ficha.FechaInicioProductiva = FechaPro;
                ficha.FechaMomentoDos = Fechaaa;
                ficha.FechaMomentoTres = FechaFinal;
                ficha.FechaLimite = FechaLimite;
                ficha.CapacitacionSGVA = oFicha.CapacitacionSGVA;
                ficha.Estado = "EN FORMACION";
                ficha.EtapaFicha = "LECTIVA";
                ficha.IdDetalle = Detalle;
                entity.Ficha.Add(ficha);
                entity.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool GuardarFichaExcel(List<FichaDTO> oFicha)
        {
            var Detalles = 0;
            var Deta = (from i in entity.Ficha
                        select i.IdDetalle).ToList();

            var Detalle = (from i in entity.DetalleCentrosAreasCoordinacion
                           select i).ToList();

            var Datos = (from i in entity.Ficha
                         select i.IdentificadorFicha).ToList();

            Ficha ficha = new Ficha();

            entity.Configuration.AutoDetectChangesEnabled = false;
            for (var i = 0; i < oFicha.Count; i++)
            {
                //Calcular fecha limite de la ficha
                DateTime Fecha = Convert.ToDateTime(oFicha[i].FechaFinFicha);
                DateTime FechaLimite;
                FechaLimite = Fecha.AddMonths(18);


                //Calcular inicio productivaa
                DateTime FechaProductiva = Convert.ToDateTime(oFicha[i].FechaFinFicha);
                DateTime FechaPro;


                FechaPro = FechaProductiva.AddMonths(-6);

                //Calcular momento 3 de la ficha
                DateTime FechaMomentoTres = FechaPro;
                DateTime FechaFinal;
                FechaFinal = FechaMomentoTres.AddDays(-15);

                //Calcular momento 2 de la ficha
                DateTime FechaInicio = Convert.ToDateTime(oFicha[i].FechaInicioFicha);
                DateTime FechaFin = FechaPro;
                Int32 FechaMomentoDos;
                DateTime Fechaaa;

                Int32 anios;
                Int32 meses;
                anios = (FechaFin.Year - FechaInicio.Year);
                meses = (FechaFin.Month - FechaInicio.Month);

                if (anios > 0)
                {
                    anios = anios * 12;
                    meses = meses + anios;
                }

                FechaMomentoDos = meses / 2;

                Fechaaa = FechaInicio.AddMonths(FechaMomentoDos);
                if (!(Datos.Contains(oFicha[i].IdentificadorFicha) & Deta.Contains(oFicha[i].IdDetalle)))
                {

                    ficha.Jornada = oFicha[i].Jornada;
                    ficha.TipoRespuesta = oFicha[i].TipoRespuesta;
                    ficha.FechaInicioFicha = oFicha[i].FechaInicioFicha;
                    ficha.FechaFinFicha = oFicha[i].FechaFinFicha;
                    ficha.ModalidadFormacion = oFicha[i].ModalidadFormacion;
                    ficha.TotalAprendices = oFicha[i].TotalAprendices;
                    ficha.AprendicesActivos = 0;
                    ficha.NumeroMesesEp = oFicha[i].NumeroMesesEp;
                    ficha.IdentificadorFicha = oFicha[i].IdentificadorFicha;
                    ficha.FechaInicioProductiva = FechaPro;
                    ficha.FechaMomentoDos = Fechaaa;
                    ficha.FechaMomentoTres = FechaFinal;
                    ficha.FechaLimite = FechaLimite;
                    ficha.Estado = "EN FORMACION";
                    ficha.EtapaFicha = "LECTIVA";
                    for (var j = 0; j < Detalle.Count; j++)
                    {
                        if ((Detalle[j].IdCentro.ToString() == oFicha[i].Codigo) & (Detalle[j].IdPrograma.ToString() == oFicha[i].CodigoPrograma))
                            for (var e = 0; e < Detalle.Count; e++)
                            {
                                if ((Detalle[e].IdCentro.ToString() == oFicha[e].Codigo) & (Detalle[e].IdPrograma.ToString() == oFicha[e].CodigoPrograma))
                                {
                                    Detalles = Detalle[e].IdlDetalle;
                                }
                            }
                        ficha.IdDetalle = Detalles;
                        entity.Ficha.Add(ficha);
                    }
                }
                entity.SaveChanges();
                entity.Configuration.AutoDetectChangesEnabled = true;
                return true;
            }
            return false;

        }

        public class Cuenta
        {
            public int id;
            public int? faltante;
        }

        public bool ConsultarFichas()
        {   //Obtener valores
            List<Cuenta> instructores = new List<Cuenta>();

            var generalAprendices = (from i in entity.Ficha
                                     where i.Estado == "EN FORMACION"
                                     select i.AprendicesActivos).Sum();

            var DisponiblesAprendices = (from i in entity.Ficha
                                         where i.Estado == "EN FORMACION" &&
                                         !(from e in entity.ProgramacionInstructorFicha select e.IdFicha).Contains(i.IdFicha)
                                         orderby i.AprendicesActivos descending
                                         select new { i.IdFicha, i.AprendicesActivos }).ToList();

            var prueba1 = (from i in entity.Ficha
                           where i.Estado == "EN FORMACION" &&
                           !(from e in entity.ProgramacionInstructorFicha select e.IdFicha).Contains(i.IdFicha)
                           orderby i.AprendicesActivos descending
                           select new { i.IdFicha, i.AprendicesActivos }).ToList();

            var generalInstructores = (from i in entity.Instructor
                                       select i).ToList();

            var DisponiblesInstructores = (from i in entity.Instructor
                                           where !(from e in entity.ProgramacionInstructorFicha
                                                   select e.IdInstructor).Contains(i.IdInstructor)
                                           select i).ToList();

            var Aprendicestotales = generalAprendices / generalInstructores.Count();

            var TotalInstructores = DisponiblesInstructores.Count;



            //Asignar a instructores Disponibles
            for (var i = 0; i < TotalInstructores; i++)
            {


                if (DisponiblesAprendices.Count > 0)
                {
                    ProgramacionInstructorFicha oProgramacion = new ProgramacionInstructorFicha();
                    oProgramacion.IdInstructor = DisponiblesInstructores[0].IdInstructor;
                    oProgramacion.IdFicha = DisponiblesAprendices[i].IdFicha;
                    entity.ProgramacionInstructorFicha.Add(oProgramacion);
                    entity.SaveChanges();
                    DisponiblesInstructores.Remove(DisponiblesInstructores[0]);
                    prueba1.Remove(DisponiblesAprendices[i]);
                }
                else
                {
                    break;
                }

            }
            if (prueba1.Count > 0)
            {

                //Validar Instructores con programaciones
                if (generalInstructores.Count > DisponiblesInstructores.Count)
                {

                    var utilizados = (from p in entity.ProgramacionInstructorFicha
                                      join f in entity.Ficha on p.IdFicha equals f.IdFicha
                                      join i in entity.Instructor on p.IdInstructor equals i.IdInstructor
                                      group f by i.IdInstructor into instructoress
                                      select new { IdInstructor = instructoress.Key, AprendicesActivos = instructoress.Sum(x => x.AprendicesActivos) }).ToList();


                    for (var i = 0; i < utilizados.Count; i++)
                    {
                        instructores.Add(new Cuenta { id = utilizados[i].IdInstructor, faltante = Aprendicestotales - utilizados[i].AprendicesActivos });
                    }
                }

                var recorrido = prueba1.Count;

                var totalDisponibleAprendices = prueba1.Count;

                var totalDisponibleInstructores = instructores.Count;

                var ficha = 0;

                var instructor = 0;

                int[,] Faltante = new int[1, 2];

                int[,] Complemento = new int[1, 2];
                //Asignacion de sobrantes
                for (var i = 0; i < recorrido; i++)
                {
                    for (var e = 0; e < totalDisponibleInstructores; e++)
                    {
                        if (Faltante[0, 1] < instructores[e].faltante)
                        {
                            Faltante[0, 0] = instructores[e].id;
                            Faltante[0, 1] = int.Parse(instructores[e].faltante.ToString());
                            instructor = e;
                        }
                    }

                    for (var e = 0; e < totalDisponibleAprendices; e++)
                    {
                        if (Complemento[0, 1] < prueba1[e].AprendicesActivos)
                        {
                            Complemento[0, 0] = prueba1[e].IdFicha;
                            Complemento[0, 1] = int.Parse(prueba1[e].AprendicesActivos.ToString());
                            ficha = e;

                        }
                    }
                    ProgramacionInstructorFicha oProgramacion = new ProgramacionInstructorFicha();
                    oProgramacion.IdInstructor = Faltante[0, 0];
                    oProgramacion.IdFicha = Complemento[0, 0];
                    entity.ProgramacionInstructorFicha.Add(oProgramacion);
                    entity.SaveChanges();
                    instructores.Remove(instructores[instructor]);
                    prueba1.Remove(prueba1[ficha]);
                    totalDisponibleAprendices = prueba1.Count;
                    totalDisponibleInstructores = instructores.Count;
                    Faltante[0, 1] = 0;
                    Complemento[0, 1] = 0;
                }
            }

            return true;
        }


        public List<Ficha> ConsultarFichasActivas()
        {

            var Datos = (from i in entity.Ficha
                         where i.Estado == "EN FORMACION"
                         select i).ToList();
            foreach (var item in Datos)
            {
                var fecha = item.FechaLimite;

                if (DateTime.Now >= fecha)
                {
                    item.Estado = "TERMINADA POR FECHA";
                    entity.SaveChanges();
                }
            }
            var datosss = (from i in entity.Ficha
                           where i.EtapaFicha == "LECTIVA"
                           select i).ToList();

            foreach (var item in datosss)
            {

                if (DateTime.Now >= item.FechaInicioProductiva)
                {
                    item.EtapaFicha = "PRODUCTIVA";
                    entity.SaveChanges();
                }
            }
            var Datos1 = (from i in entity.Ficha
                          where i.Estado == "EN FORMACION"
                          select i).ToList();
            return Datos1;
        }

        //public List<Ficha> ConsultarFichas()
        //{

        //    var Datos = (from i in entity.Ficha
        //                 where i.Estado == "EN FORMACION"
        //                 select i).ToList();

        //    foreach (var item in Datos)
        //    {
        //        var fecha = item.FechaLimite;

        //        if (DateTime.Now >= fecha)
        //        {
        //            item.Estado = "TERMINADA POR FECHA";
        //            entity.SaveChanges();
        //        }
        //    }


        //    var datosss = (from i in entity.Ficha
        //                   where i.EtapaFicha == "LECTIVA"
        //                   select i).ToList();

        //    foreach (var item in datosss)
        //    {

        //        if (DateTime.Now >= item.FechaInicioProductiva)
        //        {
        //            item.EtapaFicha = "PRODUCTIVA";
        //            entity.SaveChanges();
        //        }
        //    }




        //    var Datos1 = (from i in entity.Ficha
        //                  where i.Estado == "EN FORMACION"
        //                  select i).ToList();

        //    return Datos1;
        //}

        public List<Coordinacion> ConsultarDetalle(int Id)
        {
            var Detalle = (from d in entity.DetalleCentrosAreasCoordinacion
                           join c in entity.Coordinacion on d.IdCoordinacion equals c.IdCoordinacion
                           where d.IdPrograma == Id
                           select c).ToList();


            return Detalle;
        }


        //public List<Ficha> ConsultarFichasxArea(int Idcoordinador)
        //{
        //    var coordinador = (from i in entity.Coordinacion
        //                       where i.IdCoordinacion == Idcoordinador
        //                       select i).FirstOrDefault();

        //    var Datos = (from i in entity.Ficha
        //                 where i.Estado == "EN FORMACION"
        //                 select i).ToList();


        //    foreach (var item in Datos)
        //    {
        //        var fecha = item.FechaLimite;

        //        if (DateTime.Now >= fecha)
        //        {
        //            item.Estado = "TERMINADA POR FECHA";
        //            entity.SaveChanges();
        //        }
        //    }


        //    var Datos1 = (from i in entity.Ficha
        //                  join p in entity.Programa on i.IdPrograma equals p.IdPrograma
        //                  join a in entity.Area on p.IdArea equals a.IdArea
        //                  join c in entity.Coordinacion on a.IdArea equals c.IdArea
        //                  where i.Estado == "EN FORMACION" && c.IdCoordinacion == coordinador.IdCoordinacion
        //                  orderby p.IdPrograma
        //                  select i).ToList();

        //    return Datos1;
        //}


        //public List<Ficha> ConsultarFichasxPrograma(int IdPrograma)
        //{
        //    var Datos = (from i in entity.Ficha
        //                 where i.Estado == "EN FORMACION"
        //                 select i).ToList();


        //    foreach (var item in Datos)
        //    {
        //        var fecha = item.FechaLimite;

        //        if (DateTime.Now >= fecha)
        //        {
        //            item.Estado = "TERMINADA POR FECHA";
        //            entity.SaveChanges();
        //        }
        //    }

        //    var Datos1 = (from i in entity.Ficha
        //                  join p in entity.Programa on i.IdPrograma equals p.IdPrograma
        //                  where i.Estado == "EN FORMACION" && p.IdPrograma == IdPrograma
        //                  select i).ToList();

        //    return Datos1;
        //}

        public Area ConsultarAreaxPrograma(int IdPrograma)
        {

            var Datos = (from a in entity.Area
                         join p in entity.Programa on a.IdArea equals p.IdArea
                         where p.IdPrograma == IdPrograma
                         select a).FirstOrDefault();

            return Datos;
        }

        public List<Ficha> ConsultarFichasInactivas()
        {

            var Datos = (from i in entity.Ficha
                         where i.Estado == "TERMINADA POR UNIFICACIÓN"
                         select i).ToList();
            return Datos;
        }


        public List<Ficha> ConsultarFichasInactivasFecha()
        {

            var Datos = (from i in entity.Ficha
                         where i.Estado == "TERMINADA POR FECHA"
                         select i).ToList();
            return Datos;
        }

        public void EliminarFicha(int IdFicha)
        {

            var Datos = (from i in entity.Ficha
                         where i.IdFicha == IdFicha
                         select i).FirstOrDefault();
            entity.Ficha.Remove(Datos);
            entity.SaveChanges();
        }

        public List<AprendizDTO> ConsultarAprendiz(int id)
        {
            var Datos = entity.Database.SqlQuery<AprendizDTO>("ConsultarAprendizFicha @id", new SqlParameter("@id", id)).ToList();

            return Datos;
        }

        public List<AprendizDTO> ConsultarAprendizCondicionado(int id)
        {
            var Datos = entity.Database.SqlQuery<AprendizDTO>("ConsultarAprendizCondicionado @id", new SqlParameter("@id", id)).ToList();

            return Datos;
        }




        public FichaDTO ConsultarFichaId(int IdFicha)
        {

            FichaDTO Dto = new FichaDTO();

            var Datos = (from i in entity.Ficha
                         where i.IdFicha == IdFicha
                         select i).FirstOrDefault();

            var Deta = (from i in entity.DetalleCentrosAreasCoordinacion
                        join p in entity.Programa on i.IdlDetalle equals Datos.IdDetalle
                        select p).FirstOrDefault();

            var Centro = (from i in entity.DetalleCentrosAreasCoordinacion
                          join c in entity.Centros on i.IdlDetalle equals Datos.IdDetalle
                          select c).FirstOrDefault();

            Dto.IdPrograma = Deta.IdPrograma;
            Dto.IdArea = Deta.IdArea;
            Dto.IdDetalle = Datos.IdDetalle;
            Dto.IdCentro = Centro.IdCentro;


            Dto.IdFicha = Datos.IdFicha;
            Dto.AprendicesActivos = Datos.IdDetalle;
            Dto.TotalAprendices = Datos.TotalAprendices;
            Dto.NumeroMesesEp = Datos.NumeroMesesEp;
            Dto.IdentificadorFicha = Datos.IdentificadorFicha;
            Dto.FechaFinFicha = Convert.ToDateTime(Datos.FechaFinFicha);
            Dto.FechaInicioFicha = Convert.ToDateTime(Datos.FechaInicioFicha);
            Dto.FechaInicioProductiva = Datos.FechaInicioProductiva;
            Dto.FechaLimite = Datos.FechaLimite;
            Dto.FechaMomentoDos = Datos.FechaMomentoDos;
            Dto.FechaMomentoTres = Datos.FechaMomentoTres;
            Dto.CapacitacionSGVA = Datos.CapacitacionSGVA;
            Dto.ModalidadFormacion = Datos.ModalidadFormacion;
            Dto.EtapaFicha = Datos.EtapaFicha;
            Dto.Jornada = Datos.Jornada;
            Dto.TipoRespuesta = Datos.TipoRespuesta;
            Dto.Estado = Datos.Estado;
            Dto.NumeroMesesEl = Datos.NumeroMesesEl;

            return Dto;
        }

        public void ActualizarRegistro(Ficha oFicha)
        {
            Model1 entity = new Model1();

            var Item = (from i in entity.Ficha
                        where i.IdFicha == oFicha.IdFicha
                        select i).First();

            DateTime FechaFinalEdicion = Convert.ToDateTime(oFicha.FechaFinFicha);
            DateTime FechaMomentoFinal;

            FechaMomentoFinal = FechaFinalEdicion.AddDays(-15);

            if (FechaMomentoFinal != Item.FechaMomentoTres)
            {
                Item.FechaMomentoTres = FechaMomentoFinal;
                entity.SaveChanges();
            }

            Item.Jornada = oFicha.Jornada;
            Item.TipoRespuesta = oFicha.TipoRespuesta;
            Item.FechaInicioFicha = oFicha.FechaInicioFicha;
            Item.FechaFinFicha = oFicha.FechaFinFicha;
            Item.ModalidadFormacion = oFicha.ModalidadFormacion;
            Item.TotalAprendices = oFicha.TotalAprendices;
            Item.AprendicesActivos = 0;
            Item.NumeroMesesEp = oFicha.NumeroMesesEp;
            Item.IdentificadorFicha = oFicha.IdentificadorFicha;
            Item.FechaInicioProductiva = oFicha.FechaInicioProductiva;
            Item.FechaMomentoDos = oFicha.FechaMomentoDos;
            Item.FechaMomentoTres = oFicha.FechaMomentoTres;
            Item.FechaLimite = oFicha.FechaLimite;
            Item.CapacitacionSGVA = oFicha.CapacitacionSGVA;
            Item.Estado = "EN FORMACION";
            Item.EtapaFicha = "LECTIVA";

            entity.SaveChanges();

        }

        public void GuardarMomento(MomentosDTO oFicha)
        {
            Model1 entity = new Model1();

            MomentosFicha momento = new MomentosFicha();

            momento.IdFicha = oFicha.IdFicha;

            if (oFicha.MomentoUno == "True")
            {
                momento.Momento = "Momento Uno";
                momento.Fecha = oFicha.FechaUno;
            }
            if (oFicha.MomentoDos == "True")
            {
                momento.Momento = "Momento Dos";
                momento.Fecha = oFicha.FechaDos;
            }


            entity.MomentosFicha.Add(momento);
            entity.SaveChanges();

        }

        public bool GuardarLlamadoLista(List<AprendizDTO> aprendices)
        {
            Model1 entity = new Model1();

            var numero = aprendices[0].IdentificadorFicha;


                var id = (from i in entity.Ficha
                          where i.IdentificadorFicha == numero
                          select i.IdFicha).FirstOrDefault();

                var aprendicesss = (from a in entity.Aprendiz
                                    join c in entity.DetalleEstadoAprendiz on a.IdAprendiz equals c.IdAprendiz
                                    where a.IdFicha == id && c.IdEstado == 3
                                    select a).ToList();

            for(var i = 0; i<aprendices.Count; i++)
            {
                for(var e = 0; e < aprendicesss.Count; e++)
                {
                    if(aprendicesss[e].NumeroDocumento == aprendices[i].NumeroDocumento)
                    {
                        aprendicesss.Remove(aprendicesss[e]);
                        break;
                    }
                }
            }
            
            foreach(var item in aprendicesss)
            {
                var estado = (from i in entity.DetalleEstadoAprendiz
                              where i.IdAprendiz == item.IdAprendiz
                              select i).FirstOrDefault();
                estado.IdEstado = 2;
                entity.SaveChanges();
            }

            var formacion = (from i in entity.Aprendiz
                             where (from e in entity.DetalleEstadoAprendiz
                                    where e.IdEstado == 3
                                    select e.IdAprendiz).Contains(i.IdAprendiz) && i.IdFicha == id
                             select i).ToList();


            return true;

        }



        public MomentosDTO ConsultarMomento(int Id)
        {

            var momento = (from i in entity.MomentosFicha
                           where i.IdFicha == Id
                           select i).ToList();

            MomentosDTO Mom = new MomentosDTO();

            if (momento.Count == 2)
            {
                Mom.MomentoUno = momento[0].Momento;
                Mom.FechaUno = Convert.ToDateTime(momento[0].Fecha);

                Mom.MomentoDos = momento[1].Momento;
                Mom.FechaDos = Convert.ToDateTime(momento[1].Fecha);
            }
            else
            {
                if (momento.Count == 1)
                {
                    Mom.MomentoUno = momento[0].Momento;
                    Mom.FechaUno = Convert.ToDateTime(momento[0].Fecha);
                }
            }

            return Mom;


        }

        public bool Condiciones(int Id, string condicion)
        {

            Estado estado = new Estado();
            DetalleEstadoAprendiz DetaEstado = new DetalleEstadoAprendiz();

            estado.Nombre = "Condicionado";

            var id = (from e in entity.Estado
                      where e.Nombre == estado.Nombre
                      select e.IdEstado).Single();

            var item = (from i in entity.Aprendiz
                        where i.IdAprendiz == Id
                        select i).FirstOrDefault();


            DetaEstado.IdAprendiz = Id;
            DetaEstado.IdEstado = id;
            DetaEstado.Descripcion = condicion;

            entity.DetalleEstadoAprendiz.Add(DetaEstado);
            entity.SaveChanges();

            return true;

        }

        public void UnificarFicha(List<Ficha> oFicha)
        {
            Model1 entity = new Model1();


            var FichaUno = oFicha[0].AprendicesActivos;
            var FichaDos = oFicha[1].AprendicesActivos;

            var ficha = oFicha[1].IdFicha;
            var Unoficha = oFicha[0].IdFicha;

            var FichaTerminada = 0;
            var FichaUnificada = 0;

            if (FichaDos > FichaUno)
            {
                var Item = (from i in entity.Ficha
                            where i.IdFicha == ficha
                            select i).FirstOrDefault();

                FichaTerminada = Unoficha;
                FichaUnificada = ficha;
                Item.AprendicesActivos = FichaDos + FichaUno;
                entity.SaveChanges();

            }
            else
            {
                var Item = (from i in entity.Ficha
                            where i.IdFicha == Unoficha
                            select i).First();

                FichaTerminada = ficha;
                FichaUnificada = Unoficha;
                Item.AprendicesActivos = FichaDos + FichaUno;
                entity.SaveChanges();

            }

            var Item1 = (from i in entity.Ficha
                         where i.IdFicha == FichaTerminada
                         select i).First();
            Item1.Estado = "TERMINADA POR UNIFICACIÓN";
            entity.SaveChanges();
            var Aprendices = (from a in entity.Aprendiz
                              where a.IdFicha == FichaTerminada
                              select a).ToList();

            for (int i = 0; i < Aprendices.Count; i++)
            {
                Aprendices[i].IdFicha = FichaUnificada;
            }
            entity.SaveChanges();


        }

        // Specify what you want to happen when the Elapsed event is raised.
        public static void OnTimedEvent(object source, ElapsedEventArgs e)
        {

            Model1 entity = new Model1();

            var fechaF = (from i in entity.Ficha
                          where i.FechaMomentoTres == DateTime.Now
                          select i).ToList();

            DateTime FechaMomentoTres = Convert.ToDateTime(fechaF);
            DateTime FechaFinal;
            FechaFinal = FechaMomentoTres.AddDays(15);

            foreach (var item in fechaF)
            {

                if (DateTime.Now == FechaFinal)
                {
                    item.EtapaFicha = "PRODUCTIVA";
                    entity.SaveChanges();
                }
            }

            var Datos = (from i in entity.Ficha
                         where i.Estado == "EN FORMACION"
                         select i).ToList();


            foreach (var item in Datos)
            {
                var fecha = item.FechaLimite;

                if (DateTime.Now == fecha)
                {
                    item.Estado = "CANCELADA";
                    entity.SaveChanges();
                }
            }
        }
    }
}
