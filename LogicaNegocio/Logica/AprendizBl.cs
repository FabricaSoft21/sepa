﻿using Datos.DTO;
using Datos.Modelo;
using LogicaNegocio.Enumeraciones;
using LogicaNegocio.Mail;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace LogicaNegocio.Logica
{
    public class AprendizBl
    {
        Model1 entitys = new Model1();

        public bool GuardarAprendizIndividual(Aprendiz oAprendiz)
        {
            var Dato = (from a in entitys.Aprendiz
                        where a.NumeroDocumento == oAprendiz.NumeroDocumento
                        select a.NumeroDocumento).FirstOrDefault();
            if (Dato == null)
            {
                Usuario oUsuario = new Usuario();
                oUsuario.NombreUsuario = oAprendiz.NumeroDocumento;
                var Encriptar = SecurityEncode.SecurityEncode.Encriptar(oAprendiz.NumeroDocumento);
                oUsuario.Password = Encriptar;
                oUsuario.TipoUsuario = (int)TipoUsuario.Aprendiz;
                entitys.Usuario.Add(oUsuario);
                entitys.SaveChanges();
                var user = (from i in entitys.Usuario
                            where i.NombreUsuario == oAprendiz.NumeroDocumento
                            select i.IdUsuario).Single();
                oAprendiz.IdUsuario = user;
                entitys.Aprendiz.Add(oAprendiz);
                entitys.SaveChanges();
                var id = (from i in entitys.Aprendiz
                          where i.NumeroDocumento == oAprendiz.NumeroDocumento
                          select i.IdAprendiz).Single();
                DetalleEstadoAprendiz oEstado = new DetalleEstadoAprendiz();
                oEstado.IdAprendiz = id;
                oEstado.IdEstado = 3;
                entitys.DetalleEstadoAprendiz.Add(oEstado);
                entitys.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

            
        }

        public List<RegistroError> GuardarAprendiz(List<Aprendiz> oAprendiz)
        {
            List<Ficha> Aprendices = new List<Ficha>();
            List<RegistroError> errores = new List<RegistroError>();
            List<string> prueba = new List<string>();
            var lista = (from e in entitys.Usuario
                         select e.NombreUsuario).ToList();
            var Activos = 0;

            entitys.Configuration.AutoDetectChangesEnabled = false;
            for (Int32 i = 0; i < oAprendiz.Count; i++)
            {
                if (!(lista.Contains(oAprendiz[i].NumeroDocumento)))
                {
                    Usuario oUsuario = new Usuario();
                    oUsuario.NombreUsuario = oAprendiz[i].NumeroDocumento;
                    oUsuario.Password = oAprendiz[i].NumeroDocumento;
                    oUsuario.TipoUsuario = (int)TipoUsuario.Aprendiz;
                    entitys.Usuario.Add(oUsuario);
                    lista.Add(oAprendiz[i].NumeroDocumento);
                }
            }
            int numero = (from e in entitys.Usuario
                          select e.IdUsuario).Count();
            entitys.SaveChanges();
            List<string> cedula = (from e in entitys.Aprendiz
                                   select e.NumeroDocumento).ToList();
            List<string> email = (from e in entitys.Aprendiz
                                  select e.Correo).ToList();

            List<Usuario> Usuarios = (from e in entitys.Usuario
                                      select e).ToList();

            List<Ficha> Fichas = (from e in entitys.Ficha
                                  select e).ToList();
            for (Int32 i = 0; i < oAprendiz.Count; i++)
            {
                var ficha = false;
                var user = 0;
                for (Int32 j = 0; j < Fichas.Count; j++)
                {
                    if (Fichas[j].IdentificadorFicha == oAprendiz[i].IdFicha.ToString())
                    {
                        oAprendiz[i].IdFicha = Fichas[j].IdFicha;
                        ficha = true;
                        break;
                    }
                }
                for (var j = 0; j < Usuarios.Count; j++)
                {
                    if (Usuarios[j].NombreUsuario == oAprendiz[i].NumeroDocumento)
                    {
                        user = Usuarios[j].IdUsuario;
                        Usuarios.Remove(Usuarios[j]);
                        break;
                    }
                }

                if (!(email.Contains(oAprendiz[i].Correo)))
                {
                    if (!(cedula.Contains(oAprendiz[i].NumeroDocumento)))
                    {
                        if (ficha)
                        {
                            oAprendiz[i].IdUsuario = user;
                            entitys.Aprendiz.Add(oAprendiz[i]);
                            cedula.Add(oAprendiz[i].NumeroDocumento);
                            email.Add(oAprendiz[i].Correo);
                            if (!(i == 0))
                            {
                                if (prueba.Contains(oAprendiz[i].IdFicha.ToString()))
                                {
                                    for (var a = 0; a < Aprendices.Count; a++)
                                    {
                                        if (Aprendices[a].IdFicha == oAprendiz[i].IdFicha)
                                        {
                                            Aprendices[a].AprendicesActivos++;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    Aprendices.Add(new Ficha() { AprendicesActivos = 1, IdFicha = oAprendiz[i].IdFicha });
                                    prueba.Add(oAprendiz[i].IdFicha.ToString());
                                }
                            }
                            else
                            {
                                Aprendices.Add(new Ficha() { AprendicesActivos = 1, IdFicha = oAprendiz[i].IdFicha });
                                prueba.Add(oAprendiz[i].IdFicha.ToString());
                            }
                        }
                        else
                        {
                            errores.Add(new RegistroError()
                            {
                                NumeroDocumento = oAprendiz[i].NumeroDocumento,
                                Nombres = oAprendiz[i].Nombres + " " + oAprendiz[i].PrimerApellido,
                                Descripcion = "La ficha a la cual esta asociado no existe"
                            });
                        }
                    }
                    else
                    {
                        errores.Add(new RegistroError()
                        {
                            NumeroDocumento = oAprendiz[i].NumeroDocumento,
                            Nombres = oAprendiz[i].Nombres + " " + oAprendiz[i].PrimerApellido,
                            Descripcion = "El Aprendiz ya existe"
                        });
                    }
                }
                else
                {
                    errores.Add(new RegistroError()
                    {
                        NumeroDocumento = oAprendiz[i].NumeroDocumento,
                        Nombres = oAprendiz[i].Nombres + " " + oAprendiz[i].PrimerApellido,
                        Descripcion = "Correo ya esta registrado"
                    });
                }
            }
            entitys.Configuration.AutoDetectChangesEnabled = true;
            entitys.SaveChanges();
            for (var f = 0; f < Aprendices.Count; f++)
            {
                var fichasss = Aprendices[f].IdFicha;
                var item = (from e in entitys.Ficha
                            where e.IdFicha == fichasss
                            select e).FirstOrDefault();
                item.AprendicesActivos = Aprendices[f].AprendicesActivos;
                entitys.SaveChanges();
            }
            var aprendiz = (from i in entitys.Aprendiz
                            select i.IdAprendiz).ToList();
            for(var i = 0; i< aprendiz.Count; i++)
            {
                DetalleEstadoAprendiz oEstado = new DetalleEstadoAprendiz();
                oEstado.IdAprendiz = aprendiz[i];
                oEstado.IdEstado = 3;
                entitys.DetalleEstadoAprendiz.Add(oEstado);
            }
            entitys.SaveChanges();

            return errores;
        }

        public void ActualizarRegistro(Aprendiz oAprendiz)
        {
            Model1 entity = new Model1();

            var Item = (from i in entity.Aprendiz
                        where i.IdAprendiz == oAprendiz.IdAprendiz
                        select i).First();

            Item.NumeroDocumento = oAprendiz.NumeroDocumento;
            Item.TipoDocumento = oAprendiz.TipoDocumento;
            Item.Nombres = oAprendiz.Nombres;
            Item.PrimerApellido = oAprendiz.PrimerApellido;
            Item.SegundoApellido = oAprendiz.SegundoApellido;
            //Item.Estado = oAprendiz.Estado;
            Item.Correo = oAprendiz.Correo;
            Item.Telefono = oAprendiz.Telefono;
            Item.PoblacionDPS = oAprendiz.PoblacionDPS;
            Item.IdFicha = oAprendiz.IdFicha;
            entity.SaveChanges();

        }

        public void ActualizarFicha(Aprendiz oAprendiz)
        {
            Model1 entity = new Model1();

            var Item = (from i in entity.Aprendiz
                        where i.NumeroDocumento == oAprendiz.NumeroDocumento
                        select i).First();

            Item.IdFicha = oAprendiz.IdFicha;
            entity.SaveChanges();

        }

        public List<Ficha> ConsultarFichaxAprendiz(int IdFicha)
        {
            var Datos1 = (from i in entitys.Ficha
                          where i.IdFicha == IdFicha
                          select i).ToList();

            return Datos1;
        }

        public List<string> ConsultarCorreos()
        {
            var Datos1 = (from i in entitys.Aprendiz
                          select i.Correo).ToList();

            return Datos1;
        }

        public List<string> ConsultarCedula()
        {
            var Datos1 = (from i in entitys.Usuario
                          select i.NombreUsuario).ToList();

            return Datos1;
        }

        public Aprendiz ConsultarAprendizId(int? IdAprendiz)
        {
            Model1 entity = new Model1();
            var Datos = (from i in entity.Aprendiz
                         where i.IdAprendiz == IdAprendiz
                         select i).FirstOrDefault();
            return Datos;
        }

        public List<AprendizDTO> ConsultarAprendiz()
        {
            var Datos = entitys.Database.SqlQuery<AprendizDTO>("ConsultarAprendiz").ToList();

            return Datos;
        }

        public bool EliminarAprendiz(int IdAprendiz)
        {

            var Aprendiz = (from i in entitys.Aprendiz
                         where i.IdAprendiz == IdAprendiz
                         select i).FirstOrDefault();
            entitys.Aprendiz.Remove(Aprendiz);
            entitys.SaveChanges();
            return true;


        }

        public String ConsultarAprendizDocumento(string Documento)
        {
            Model1 entity = new Model1();
            var Datos = (from i in entity.Aprendiz
                         where i.NumeroDocumento == Documento
                         select i.NumeroDocumento).FirstOrDefault();
            return Datos;
        }
    }

}
