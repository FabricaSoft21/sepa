﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.Modelo;
using Datos.DTO;
using System.Data.SqlClient;

namespace LogicaNegocio.Logica
{
   public class GestionBl
    {
        Model1 entry = new Model1();

        public List<AlternativaEP> ConsultarAlternativa()
        {


            var datos = (from i in entry.AlternativaEP
                         select i).ToList();


            return datos;
        }

        public List<Sede> ConsultarEmpresaSede()
        {

            var datos = (from i in entry.Sede
                         select i).ToList();


            return datos;

        }

        public List<Gestion> ConsultarAprendizId(int? IdAprendiz)
        {


            var tieneges = entry.GestionIndividual.Where(item => item.IdAprendiz == IdAprendiz).FirstOrDefault();

            List<Gestion> gestiont = new List<Gestion>();

            if (tieneges == null)
            {

                //var ficha = (from i in entry.Ficha
                //             join e in entry.Aprendiz on IdAprendiz equals e.IdAprendiz
                //             select i.IdentificadorFicha).FirstOrDefault();
                //Model1 entity = new Model1();
                //var Datos = (from i in entity.Aprendiz
                //             where i.IdAprendiz == IdAprendiz
                //             select i).FirstOrDefault();
                //Datos.IdFicha = Convert.ToInt32(ficha);


                var Datos = entry.Database.SqlQuery<Gestion>("PA_ConsultarAprendizGestion @id",
                    new SqlParameter("@id", IdAprendiz)).ToList();

                return Datos;

            }
            else
            {

                Gestion gestion = new Gestion();
                var Datos = entry.Database.SqlQuery<Gestion>("PA_ConsultarAprendizGestion @id",
                    new SqlParameter("@id", IdAprendiz)).ToList();


                var Datos1 = entry.Database.SqlQuery<Gestion>("PA_ConsultarAprendizGestionG @id",
                    new SqlParameter("@id", IdAprendiz)).ToList();

                gestion.IdAprendiz = Datos[0].IdAprendiz;
                gestion.NumeroDocumento = Datos[0].NumeroDocumento;
                gestion.TipoDocumento = Datos[0].TipoDocumento;
                gestion.Nombres = Datos[0].Nombres;
                gestion.PrimerApellido = Datos[0].PrimerApellido;
                gestion.SegundoApellido = Datos[0].SegundoApellido;
                gestion.Correo = Datos[0].Correo;
                gestion.Telefono = Datos[0].Telefono;
                gestion.PoblacionDPS = Datos[0].PoblacionDPS;



                var ficha = (from i in entry.Ficha
                             join e in entry.Aprendiz on IdAprendiz equals e.IdAprendiz
                             select i.IdentificadorFicha).FirstOrDefault();
                gestion.IdFicha = int.Parse(ficha);

                gestion.IdGestion = Datos1[0].IdGestion;
                gestion.Arl = Datos1[0].Arl;
                gestion.EmpresaSedido = Datos1[0].EmpresaSedido;
                gestion.DescripcionSedido = Datos1[0].DescripcionSedido;
                gestion.Idsede = Datos1[0].Idsede;
                gestion.FechaInicioProductiva = Datos1[0].FechaInicioProductiva;
                gestion.IdAlternativa = Datos1[0].IdAlternativa;

                gestiont.Add(gestion);

                return gestiont;
            }


           
        }



        public void ActualizarRegistro(Aprendiz oAprendiz)
        {
            Model1 entity = new Model1();



            var idficha = (from i in entry.Ficha
                           where i.IdentificadorFicha == oAprendiz.IdFicha.ToString()
                           select i.IdFicha).FirstOrDefault();
            var Item = (from i in entity.Aprendiz
                        where i.IdAprendiz == oAprendiz.IdAprendiz
                        select i).First();

            Item.NumeroDocumento = oAprendiz.NumeroDocumento;
            Item.TipoDocumento = oAprendiz.TipoDocumento;
            Item.Nombres = oAprendiz.Nombres;
            Item.PrimerApellido = oAprendiz.PrimerApellido;
            Item.SegundoApellido = oAprendiz.SegundoApellido;
            //Item.Estado = oAprendiz.Estado;
            Item.Correo = oAprendiz.Correo;
            Item.Telefono = oAprendiz.Telefono;
            Item.PoblacionDPS = oAprendiz.PoblacionDPS;
            Item.IdFicha = idficha;
            entity.SaveChanges();

        }

        public string selectfecah(int id)
        {


            var datos = (from i in entry.Ficha
                         join e in entry.Aprendiz on id equals e.IdAprendiz
                         select i.FechaInicioProductiva).FirstOrDefault();


            var Dato = Convert.ToDateTime(datos).ToShortDateString();
            return Dato.ToString();
        }




        public bool GuardarGestion(GestionIndividual gestion)
        {

            try


            {


                var esta = entry.GestionIndividual.Where(item => item.IdAprendiz == gestion.IdAprendiz).FirstOrDefault();

                if (esta == null)
                {
                    entry.GestionIndividual.Add(gestion);
                    entry.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

               
            }
            catch
            {
                return false;
            }

        }
    }
}
