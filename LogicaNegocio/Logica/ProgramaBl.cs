﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.Modelo;


namespace LogicaNegocio.Logica
{
    public class ProgramaBl
    {
        Model1 entity = new Model1();

        public void GuardarPrograma(Programa oPrograma)
        {
            entity.Programa.Add(oPrograma);
            entity.SaveChanges();
        }

        public List<Programa> ConsultarProgramas()
        {
            var Datos = (from i in entity.Programa
       
                         select i).ToList();
            return Datos;
        }


        public void ActualizarRegistro(Programa oPrograma)
        {
            Model1 entity = new Model1();

            var Item = (from i in entity.Programa
                        where i.IdPrograma == oPrograma.IdPrograma
                        select i).FirstOrDefault();

            Item.CodigoPrograma = oPrograma.CodigoPrograma;
            Item.Nivel = oPrograma.Nivel;
            Item.IdArea = oPrograma.IdArea;
            Item.NombrePrograma = oPrograma.NombrePrograma;
            entity.SaveChanges();

        }

        public bool EliminarPrograma(int IdProgrma)
        {

           
                var programas = (from i in entity.Programa
                             where i.IdPrograma == IdProgrma
                             select i).FirstOrDefault();
                entity.Programa.Remove(programas);
                entity.SaveChanges();
                return true;
          

        }

        public Programa ConsultarProgramaDetalle(int IdDetalle)
        {

            var Datos = (from i in entity.Programa
                         join d in entity.DetalleCentrosAreasCoordinacion on i.IdPrograma equals d.IdPrograma
                         where d.IdlDetalle == IdDetalle
                         select i).FirstOrDefault();
            return Datos;

        }

        public Programa ConsultarProgramaId(int IdDetalle)
        {

            var Datos = (from i in entity.Programa
                     
                         where i.IdPrograma == IdDetalle
                         select i).FirstOrDefault();
            return Datos;

        }

        public Programa ConsultarProgramaCodigo(string CodigoPrograma)
        {

            var Datos = (from i in entity.Programa
                         where i.CodigoPrograma == CodigoPrograma
                         select i).FirstOrDefault();
            return Datos;

        }

        //public List<Programa> ConsultarProgramaxCoordinacion(int IdCoordinador)
        //{
        //    var programa = (from p in entity.Programa
        //                    join a in entity.Area on p.IdArea equals a.IdArea
        //                    join c in entity.Coordinacion on a.IdArea equals c.IdArea
        //                    where c.IdCoordinacion == IdCoordinador && (p.NombrePrograma != "Transversal" || p.NombrePrograma != "TRANSVERSAL")
        //                    && p.Nivel != "MEDIA TÉCNICA" 
        //                    //&& p.Nivel != "CURSO ESPECIAL"
        //                    orderby p.NombrePrograma
        //                    select p).ToList();
        //    return programa;
        //}

        //public List<Programa> ProgramaTituladosxCoordinacion(int IdCoordinador)
        //{

        //    var coordinador = (from i in entity.Coordinacion
        //                       where i.IdCoordinacion == IdCoordinador
        //                       select i).FirstOrDefault();


        //    var area = (from i in entity.Area
        //                where i.IdArea == coordinador.IdArea
        //                select i).FirstOrDefault();

        //    var Datos = (from i in entity.Programa
        //                 where i.IdArea == area.IdArea
        //                 && i.Nivel != "MEDIA TÉCNICA" 
        //                 && i.Nivel != "CURSO ESPECIAL"
        //                  && i.Nivel != "CURSO VIRTUAL"
        //                 && i.Nivel != "ESPECIALIZACIÓN TECNOLÓGICA VIRTUAL"
        //                  && i.Nivel != "TECNÓLOGO VIRTUAL"
        //                 && (i.NombrePrograma != "Transversal" || i.NombrePrograma != "TRANSVERSAL")
        //                 orderby i.NombrePrograma
        //                 select i).ToList();

        //    return Datos;
        //}

        //public List<Programa> ProgramaVirtualesTitulados(int IdCoordinador)
        //{

        //    var coordinador = (from i in entity.Coordinacion
        //                       where i.IdCoordinacion == IdCoordinador
        //                       select i).FirstOrDefault();


        //    var area = (from i in entity.Area
        //                where i.IdArea == coordinador.IdArea
        //                select i).FirstOrDefault();

        //    var Datos = (from i in entity.Programa
        //                 where i.IdArea == area.IdArea
        //                 && (i.Nivel == "TECNÓLOGO VIRTUAL"
        //                 || i.Nivel == "ESPECIALIZACIÓN TECNOLÓGICA VIRTUAL" || i.Nivel == "CURSO VIRTUAL")
                        
        //                 orderby i.NombrePrograma
        //                 select i).ToList();

        //    return Datos;
        //}

        //public List<Programa> ProgramasComplementariosxCoordinacion(int IdCoordinador)
        //{

        //    var coordinador = (from i in entity.Coordinacion
        //                       where i.IdCoordinacion == IdCoordinador
        //                       select i).FirstOrDefault();


        //    var area = (from i in entity.Area
        //                where i.IdArea == coordinador.IdArea
        //                select i).FirstOrDefault();

        //    var Datos = (from i in entity.Programa
        //                 where i.IdArea == area.IdArea && i.Nivel == "CURSO ESPECIAL"
        //                 orderby i.NombrePrograma
        //                 select i).ToList();

        //    return Datos;
        //}


        public List<Programa> ConsultarProgramaxArea(int IdArea)
        {
            var programa = (from p in entity.Programa
                            where p.IdArea == IdArea 
                            orderby p.NombrePrograma
                            select p).ToList();
            return programa;
        }

        public List<Programa> ConsultarPragamaTransversal()
        {
            var programa = (from p in entity.Programa
                            where p.NombrePrograma == "Transversal" || p.NombrePrograma == "TRANSVERSAL"
                            select p).ToList();

            return programa;
        }


        //public List<Programa> ProgramasxEmpresa(int IdCoordinador)
        //{
        //    var coordinador = (from i in entity.Coordinacion
        //                       where i.IdCoordinacion == IdCoordinador
        //                       select i).FirstOrDefault();


        //    var area = (from i in entity.Area
        //                where i.IdArea == coordinador.IdArea
        //                select i).FirstOrDefault();

        //    var Datos = (from i in entity.Programa
        //                 where i.Nivel == "MEDIA TÉCNICA" && i.IdArea == area.IdArea
        //                 select i).ToList();

        //    return Datos;

        //}
        //public List<Programa> ProgramasVirtualesTitulados(int IdCoordinador)
        //{
        //    var coordinador = (from i in entity.Coordinacion
        //                       where i.IdCoordinacion == IdCoordinador
        //                       select i).FirstOrDefault();


        //    var area = (from i in entity.Area
        //                where i.IdArea == coordinador.IdArea
        //                select i).FirstOrDefault();

        //    var Datos = (from i in entity.Programa
        //                 where (i.Nivel == "TECNÓLOGO VIRTUAL" || i.Nivel == "ESPECIALIZACIÓN TECNOLÓGICA VIRTUAL") && i.IdArea == area.IdArea
        //                 select i).ToList();

        //    return Datos;

        //}


        //public List<Programa> ProgramasCursosVirtuales(int IdCoordinador)
        //{
        //    var coordinador = (from i in entity.Coordinacion
        //                       where i.IdCoordinacion == IdCoordinador
        //                       select i).FirstOrDefault();


        //    var area = (from i in entity.Area
        //                where i.IdArea == coordinador.IdArea
        //                select i).FirstOrDefault();

        //    var Datos = (from i in entity.Programa
        //                 where (i.Nivel == "TECNÓLOGO VIRTUAL" || i.Nivel == "ESPECIALIZACIÓN TECNOLÓGICA VIRTUAL") && i.IdArea == area.IdArea
        //                 select i).ToList();

        //    return Datos;

        //}
    }
}
