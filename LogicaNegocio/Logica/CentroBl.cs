﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.Modelo;

namespace LogicaNegocio.Logica
{
  public class CentroBl
    {
        Model1 entity = new Model1();

        public bool GuardarCentro (Centros oCentro)
        {
            var Datos = (from c in entity.Centros where c.IdCentro == oCentro.IdCentro
                         select c).FirstOrDefault();

            if (Datos == null)
            {
                entity.Centros.Add(oCentro);
                entity.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
  
        public List<Centros> ConsultarCentros()
        {
            var Datos = (from I in entity.Centros select I).ToList();


            return Datos;
        }

        public Centros IdCentro()
        {

            var codigo = (from i in entity.Centros
                          orderby i.IdCentro descending
                          select i).FirstOrDefault();

            return codigo;
        }

        public void ActualizarCentro(Centros oCentro)
        {
            var item = (from i in entity.Centros
                        where i.IdCentro == oCentro.IdCentro
                        select i).First();

            item.NombreCentro = oCentro.NombreCentro;
            item.Direccion = oCentro.Direccion;
            item.Codigo = oCentro.Codigo;
            entity.SaveChanges();
        }

        public Centros ConsultarCentroId(int IdCentro)
        {

            var Datos = (from i in entity.Centros
                         where i.IdCentro == IdCentro
                         select i).FirstOrDefault();
            return Datos;

        }

        public bool EliminarCentro( int id)
        {
            var Centro = (from i in entity.Centros
                          where i.IdCentro == id
                          select i).FirstOrDefault();

            entity.Centros.Remove(Centro);
            entity.SaveChanges();
            return true;
        }

        public string ConsultarNombreCentro(int id)
        {
            var Datos = (from i in entity.Centros
                         where i.IdCentro == id
                         select i.NombreCentro).FirstOrDefault();
            return Datos;
        }

        public List<Zona> ConsultarZona()
        {
            var zonas = (from i in entity.Zona
                        select i).ToList();

            return zonas;
        }

        public List<Regional> consultarRegionales(int IdZona)
        {
            var regional = (from i in entity.Regional
                                 where i.IdZona == IdZona
                            select i).ToList();

            return regional;

        }

        public Zona consultarZonaXReg (int IdRegional)
        {
            var regional = (from i in entity.Regional
                             where i.IdRegional == IdRegional
                             select i).FirstOrDefault();

            var zona = (from i in entity.Zona
                                 where i.IdZona == regional.IdZona
                                 select i).FirstOrDefault();

            return zona;

        }


        public Centros consultarCentroXReg(int IdRegional)
        {
            var regional = (from i in entity.Regional
                            where i.IdRegional == IdRegional
                            select i).FirstOrDefault();

            var centro = (from i in entity.Centros
                          where i.IdRegional == regional.IdRegional
                          select i).FirstOrDefault();

            return centro;

        }

        public Regional consultarRegionalxId(int IdRegional)
        {
            var regional = (from i in entity.Regional
                                 where i.IdRegional == IdRegional
                                 select i).FirstOrDefault();

            return regional;

        }

    }
}