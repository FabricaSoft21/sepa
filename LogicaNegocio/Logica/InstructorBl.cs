﻿using Datos.DTO;
using Datos.Modelo;
using LogicaNegocio.Enumeraciones;
using LogicaNegocio.Mail;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace LogicaNegocio.Logica
{
    public class InstructorBl
    {


        public bool GuardarInstructor(InstructorDTO oInstructorDTO)
        {
            Model1 entity = new Model1();
            var numero = int.Parse(oInstructorDTO.NumeroDocumento);
            var Datos = (from i in entity.Instructor
                         where i.NumeroDocumento == numero
                         select i).FirstOrDefault();
            if (Datos == null)
            {
                Usuario oUsuario = new Usuario();
                oUsuario.NombreUsuario = oInstructorDTO.NumeroDocumento.ToString();
                var Encriptar = SecurityEncode.SecurityEncode.Encriptar(oInstructorDTO.NumeroDocumento.ToString());
                oUsuario.Password = Encriptar;
                oUsuario.TipoUsuario = (int)TipoUsuario.Instructor;
                entity.Usuario.Add(oUsuario);
                entity.SaveChanges();
                Instructor oInstructor = new Instructor();
                oInstructor.TipoDocumento = oInstructorDTO.TipoDocumento;
                oInstructor.NumeroDocumento = Convert.ToInt32(oInstructorDTO.NumeroDocumento);
                oInstructor.Nombres = oInstructorDTO.Nombres;
                oInstructor.Apellidos = oInstructorDTO.Apellidos;
                oInstructor.Telefono = Convert.ToInt32(oInstructorDTO.Telefono);
                oInstructor.TipoDocumento = oInstructorDTO.TipoDocumento;
                oInstructor.Correo = oInstructorDTO.Correo;
                oInstructor.Estado = true;
                oInstructor.IdUsuario = oUsuario.IdUsuario;
                oInstructor.EnvioCorreo = false;
                oInstructor.PicoPlaca = (oInstructorDTO.PicoPlaca == "") ? "NO TIENE PICO Y PLACA" : oInstructorDTO.PicoPlaca;
                var id = (from i in entity.DetalleCentrosAreasCoordinacion
                          where i.IdCentro == oInstructorDTO.IdCentro && i.IdCoordinacion == oInstructorDTO.Idcordi && i.IdArea == oInstructorDTO.IdArea
                          select i.IdlDetalle).FirstOrDefault();
                oInstructor.IdDetalleArea = id;
                entity.Instructor.Add(oInstructor);
                entity.SaveChanges();
                var Asunto = "Nuevo Usuario";
                var Plantilla = "Usuario : " + oUsuario.NombreUsuario + "<br/> Contraseña: " + oUsuario.NombreUsuario;
                SendMail.SendMailMessage(Asunto, Plantilla, oInstructorDTO.Correo);
                return true;
            }
            else
            {
                return false;
            }

        }

        public List<Centros> ConsultarCentros()
        {
            Model1 entity = new Model1();
            var Datos = (from i in entity.Centros
                         select i).ToList();

            return Datos;
        }

        public List<Coordinacion> Consultarcoordinacion(int id)
        {
            Model1 entity = new Model1();
            var Datos = (from i in entity.DetalleCentrosAreasCoordinacion
                         where i.IdCentro == id
                         select i.IdCoordinacion).ToList();

            List<Coordinacion> coordi = new List<Coordinacion>();
            foreach(var item in Datos)
            {
                var coordinacion = (from i in entity.Coordinacion
                                    where i.IdCoordinacion == item
                                    select i).Single();
                coordi.Add(coordinacion);
            }

            return coordi;
        }

        public List<Area> ConsultarAreas(DetalleCentrosAreasCoordinacion oDetalleCentrosAreasCoordinacion)
        {
            Model1 entity = new Model1();
            var Datos = (from i in entity.DetalleCentrosAreasCoordinacion
                         where i.IdCentro == oDetalleCentrosAreasCoordinacion.IdCentro && i.IdCoordinacion == oDetalleCentrosAreasCoordinacion.IdCoordinacion
                         select i.IdArea).ToList();

            List<Area> areas = new List<Area>();
            foreach (var item in Datos)
            {
                var area = (from i in entity.Area
                                    where i.IdArea == item
                                    select i).Single();
                areas.Add(area);
            }

            return areas;
        }

        public List<Ficha> ConsultarFichas()
        {
            Model1 entity = new Model1();
            var Datos = (from i in entity.Ficha
                         where !(from o in entity.ProgramacionInstructorFicha select o.IdFicha).Contains(i.IdFicha) && i.Estado == "EN FORMACION"
                         select i).ToList();

            return Datos;
        }

        public List<ProgramacionInstructorFicha> ConsultarProgramaciones(int id)
        {
            Model1 entity = new Model1();
            var cedula = (from e in entity.Instructor
                          where e.NumeroDocumento == id || e.IdInstructor == id
                          select e.IdInstructor).Single();

            var Datos = (from i in entity.ProgramacionInstructorFicha
                         where i.IdInstructor == cedula
                         select i).ToList();

            return Datos;
        }

        public List<ProgramacionInstructorFicha> EliminarProgramaciones(int id)
        {
            Model1 entity = new Model1();
            var cedula = (from e in entity.ProgramacionInstructorFicha
                          where e.IdProgramacion == id
                          select e).Single();
            var instructor = cedula.IdInstructor;
            entity.ProgramacionInstructorFicha.Remove(cedula);
            entity.SaveChanges();
            var consulta = (from e in entity.ProgramacionInstructorFicha
                            where e.IdInstructor == instructor
                            select e).ToList();
            return consulta;
        }

        public void EnviarCorreoInstructor()
        {
            Model1 entity = new Model1();
            var Instructor = (from i in entity.Instructor
                              where i.EnvioCorreo == false
                              select i).FirstOrDefault();


            var Usuario = (from i in entity.Usuario
                           where i.IdUsuario == Instructor.IdUsuario
                           select i).FirstOrDefault();
            Instructor.EnvioCorreo = true;
            entity.SaveChanges();
            var Desencriptar = SecurityEncode.SecurityEncode.Desencriptar(Usuario.Password);


            var Asunto = "Nuevo Usuario";
            var Plantilla = "Usuario : " + Usuario.NombreUsuario + "<br/> Contraseña: " + Desencriptar;
            SendMail.SendMailMessage(Asunto, Plantilla, Instructor.Correo);
        }

        public void EnviarCorreoLista(List<Instructor> oInstrutor)
        {
            foreach (var item in oInstrutor)
            {
                var Asunto = "Nuevo Usuario";
                var Plantilla = "Usuario : " + item.NumeroDocumento + "<br/> Contraseña: " + item.NumeroDocumento;
                SendMail.SendMailMessage1(Asunto, Plantilla, item.Correo);
            }
        }

        public List<InstructorDTO> ConsultarInstructores()
        {
            Model1 entity = new Model1();
            var Datos = (from i in entity.Instructor
                         join e in entity.DetalleCentrosAreasCoordinacion on i.IdDetalleArea equals e.IdlDetalle
                         where i.Estado == true
                         orderby i.Nombres
                         select new {i,e}).ToList();

            List<InstructorDTO> ListaInstructor = new List<InstructorDTO>();
            foreach (var item in Datos)
            {
                InstructorDTO oInstructorDTO = new InstructorDTO();


                oInstructorDTO.IdInstructor = item.i.IdInstructor;
                oInstructorDTO.Nombres = item.i.Nombres;
                oInstructorDTO.Apellidos = item.i.Apellidos;
                oInstructorDTO.NumeroDocumento = item.i.NumeroDocumento.ToString();
                oInstructorDTO.TipoDocumento = item.i.TipoDocumento;
                oInstructorDTO.Telefono = item.i.Telefono.ToString();
                oInstructorDTO.Estado = item.i.Estado;
                oInstructorDTO.Correo = item.i.Correo;
                oInstructorDTO.IdUsuario = item.i.IdUsuario;
                oInstructorDTO.IdDetalle = item.i.IdDetalleArea;
                oInstructorDTO.PicoPlaca = item.i.PicoPlaca;
                oInstructorDTO.IdCentro = item.e.IdCentro;
                oInstructorDTO.IdArea = item.e.IdArea;
                oInstructorDTO.Idcordi = item.e.IdCoordinacion;
                ListaInstructor.Add(oInstructorDTO);
            }
            return ListaInstructor;
        }

        public List<Ficha> ConsultarIdentificadorFicha(int ficha)
        {
            Model1 entity = new Model1();
            var Datos = (from i in entity.Ficha
                         where i.IdFicha == ficha
                         select i).ToList();

            return Datos;
        }

        public List<InstructorDTO> ConsultarInhabilitados()
        {
            Model1 entity = new Model1();
            var Datos = (from i in entity.Instructor
                         where i.Estado == false
                         orderby i.Nombres
                         select i).ToList();

            List<InstructorDTO> ListaInstructor = new List<InstructorDTO>();
            foreach (var item in Datos)
            {
                InstructorDTO oInstructorDTO = new InstructorDTO();
                oInstructorDTO.IdInstructor = item.IdInstructor;
                oInstructorDTO.Nombres = item.Nombres;
                oInstructorDTO.Apellidos = item.Apellidos;
                oInstructorDTO.NumeroDocumento = item.NumeroDocumento.ToString();
                oInstructorDTO.TipoDocumento = item.TipoDocumento.ToString();
                oInstructorDTO.Correo = item.Correo;
                oInstructorDTO.Estado = item.Estado;
                oInstructorDTO.Telefono = item.Telefono.ToString();
                oInstructorDTO.Estado = item.Estado;
                oInstructorDTO.IdUsuario = item.IdUsuario;
                oInstructorDTO.PicoPlaca = item.PicoPlaca;
                ListaInstructor.Add(oInstructorDTO);
            }
            return ListaInstructor;
        }


        public void CambiarEstado(Instructor oInstructor)
        {
            Model1 entity = new Model1();
            var Item = (from i in entity.Instructor
                        where i.IdInstructor == oInstructor.IdInstructor
                        select i).First();
            if (Item.Estado == true)
            {
                Item.Estado = false;
                entity.SaveChanges();
            }
            else
            {
                Item.Estado = true;
                entity.SaveChanges();
            }
        }

        public String ConsultarInstructorCedula(string cedula)
        {
            Model1 entity = new Model1();
            var Datos = (from i in entity.Instructor
                         where i.NumeroDocumento == Convert.ToInt32(cedula)
                         select i.NumeroDocumento).FirstOrDefault().ToString();
            return Datos;
        }

        public InstructorDTO ConsultarInstructorCedula1(int Cedula)
        {
            Model1 entity = new Model1();
            var Datos = (from i in entity.Instructor
                         join e in entity.DetalleCentrosAreasCoordinacion on i.IdDetalleArea equals e.IdlDetalle
                         where i.NumeroDocumento == Cedula
                         select new { i, e }).FirstOrDefault();

                InstructorDTO oInstructorDTO = new InstructorDTO();
                oInstructorDTO.IdInstructor = Datos.i.IdInstructor;
                oInstructorDTO.Nombres = Datos.i.Nombres;
                oInstructorDTO.Apellidos = Datos.i.Apellidos;
                oInstructorDTO.NumeroDocumento = Datos.i.NumeroDocumento.ToString();
                oInstructorDTO.TipoDocumento = Datos.i.TipoDocumento;
                oInstructorDTO.Telefono = Datos.i.Telefono.ToString();
                oInstructorDTO.Estado = Datos.i.Estado;
                oInstructorDTO.Correo = Datos.i.Correo;
                oInstructorDTO.IdUsuario = Datos.i.IdUsuario;
                oInstructorDTO.IdDetalle = Datos.i.IdDetalleArea;
                oInstructorDTO.PicoPlaca = Datos.i.PicoPlaca;
                oInstructorDTO.IdCentro = Datos.e.IdCentro;
                oInstructorDTO.IdArea = Datos.e.IdArea;
                oInstructorDTO.Idcordi = Datos.e.IdCoordinacion;
            return oInstructorDTO;

        }

        public void ActualizarRegistro(InstructorDTO oInstructorDTO)
        {
            Model1 entity = new Model1();

            var Item = (from i in entity.Instructor
                        where i.IdInstructor == oInstructorDTO.IdInstructor
                        select i).First();

            Item.Nombres = oInstructorDTO.Nombres;
            Item.Apellidos = oInstructorDTO.Apellidos;
            Item.NumeroDocumento = Convert.ToInt32(oInstructorDTO.NumeroDocumento);
            Item.TipoDocumento = oInstructorDTO.TipoDocumento;
            Item.Correo = oInstructorDTO.Correo;
            Item.Estado = oInstructorDTO.Estado;
            Item.Telefono = Convert.ToInt32(oInstructorDTO.Telefono);
            var id = (from i in entity.DetalleCentrosAreasCoordinacion
                      where i.IdCentro == oInstructorDTO.IdCentro && i.IdCoordinacion == oInstructorDTO.Idcordi && i.IdArea == oInstructorDTO.IdArea
                      select i.IdlDetalle).FirstOrDefault();
            Item.IdDetalleArea = id;
            Item.Estado = true;
            Item.PicoPlaca = (oInstructorDTO.PicoPlaca == "") ? "NO TIENE PICO Y PLACA" : oInstructorDTO.PicoPlaca;
            entity.SaveChanges();

        }

        public bool RegistrarProgramacion(List<ProgramacionInstructorFicha> oProgramar)
        {
            foreach (var item in oProgramar) {
                Model1 entity = new Model1();
                var Datos = (from i in entity.ProgramacionInstructorFicha
                             where i.IdFicha == item.IdFicha && i.IdInstructor == item.IdInstructor
                             select i).FirstOrDefault();

                if (Datos == null)
                {
                    ProgramacionInstructorFicha oProgramacion = new ProgramacionInstructorFicha();
                    oProgramacion.IdFicha = item.IdFicha;
                    oProgramacion.IdInstructor = item.IdInstructor;
                    entity.ProgramacionInstructorFicha.Add(oProgramacion);
                    entity.SaveChanges();
                }
                
            }
            return true;
        }
    }
}
